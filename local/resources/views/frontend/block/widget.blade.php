<div class="col-lg-4">
    <div class="card border-0 shadow mb-4">
        <div class="card-header border-0 bg-success text-white py-3">
            <span class="font-weight-bold small"><i class="far fa-books"></i>Kiến thức</span>
        </div>

        @foreach ($listPostRandomTuVi as $post)
        <div class="list-group list-group-flush list-group-item">
            <a href="{{route('frontend-post', $post->url_post)}}" class="list-group-item-action py-3">
                <img class="lazy img-fluid" src="{{asset('upload/img/thumbnail/post/' . $post->thumbnail_post)}}" alt="{{$post->name_post}}">
                <div class="font-weight-bold mb-1">{{$post->name_post}}</div>
            </a>
            <div class="small mb-2">{{$post->present_post}}</div>
            <div class="small text-muted">{{$post->date_post}}</div>
        </div>
        @endforeach

    </div>
    <div class="card border-0 shadow mb-4">
        <div class="card-header border-0 bg-success text-white py-3">
            <span class="font-weight-bold small"><i class="far fa-books"></i>Người nổi tiếng</span>
        </div>

        @foreach ($listPostRandomFamous as $post)
        <div class="list-group list-group-flush list-group-item">
            <a href="{{route('frontend-post', $post->url_post)}}" class="list-group-item-action py-3">
                <img class="lazy img-fluid" src="{{asset('upload/img/thumbnail/post/' . $post->thumbnail_post)}}" alt="{{$post->name_post}}">
                <div class="font-weight-bold mb-1">{{$post->name_post}}</div>
            </a>
            <div class="small mb-2">{{$post->present_post}}</div>
            <div class="small text-muted">{{$post->date_post}}</div>
        </div>
        @endforeach
    </div>
</div>
