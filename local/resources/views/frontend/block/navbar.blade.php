<nav class="navbar navbar-expand-lg navbar-light bg-light sb-navbar">
    <div class="container">
        <a class="navbar-brand" href="{{ route('frontend-index') }}">
            <img src="{{ asset('frontend/assets/img/logo-tuvihaotam.png') }}" alt="HẢO TÂM">
            <span class="ml-1">ANH LIỄU</span>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto pt-3 pt-lg-0">
                <!-- <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="themesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Themes
                    </a>
                    <div class="dropdown-menu dropdown-themes border-0 shadow animate slideIn" aria-labelledby="themesDropdown">
                        <div class="d-lg-flex flex-row">
                            <div class="dropdown-themes-callout d-none d-lg-flex p-5 text-center text-white align-items-center"><div>
                                <h5>Bootstrap Themes</h5>
                                <p>Fully designed websites ready to modify and publish</p>
                                <a href="themes/index.html" class="btn btn-theme btn-xl">Browse All Themes <i class="fas fa-angle-right"></i></a>
                            </div>
                        </div>
                        <div class="py-lg-3">
                            <a class="dropdown-item font-weight-bold" href="themes/index.html"><i class="far fa-paint-brush-alt fa-fw"></i> Browse All Themes</a>
                            <a class="dropdown-item font-weight-bold" href="buy-bootstrap-themes/index.html"><i class="far fa-tags fa-fw"></i> Buy Bootstrap Themes</a>
                            <div class="dropdown-divider"></div>
                                <h6 class="dropdown-header">Theme Categories:</h6>
                                <a class="dropdown-item" href="themes/admin-dashboard/index.html">Admin &amp; Dashboard</a>
                                <a class="dropdown-item" href="themes/landing-pages/index.html">Landing Pages</a>
                                <a class="dropdown-item" href="themes/business-corporate/index.html">Business &amp; Corporate</a>
                                <a class="dropdown-item" href="themes/portfolio-resume/index.html">Portfolio &amp; Resume</a>
                                <a class="dropdown-item" href="themes/blog/index.html">Blog</a>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="snippets/index.html">Kiến thức</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="snippets/index.html">Người nổi tiếng</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Learn
                    </a>
                    <div class="dropdown-menu border-0 shadow animate slideIn" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="guides/index.html">Guides &amp; Tutorials</a>
                        <span class="d-block" tabindex="0" data-toggle="tooltip" data-placement="left" title="Under Development!" data-container=".sb-navbar">
                            <a class="dropdown-item disabled" href="#">Courses <span class="ml-2 badge badge-pill badge-secondary d-lg-none">Under Development!</span></a>
                        </span>
                        <div class="dropdown-divider"></div>
                        <span class="d-block" tabindex="0" data-toggle="tooltip" data-placement="left" title="Under Development!" data-container=".sb-navbar">
                        <a class="dropdown-item disabled" href="#">Books <span class="ml-2 badge badge-pill badge-secondary d-lg-none">Under Development!</span></a>
                        </span>
                    </div>
                </li> -->
            </ul>
            <ul class="navbar-nav pb-3 pb-lg-0">
                <!-- <li class="nav-item">
                    <a class="nav-link" href="https://shop.startbootstrap.com/">
                        Shop
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="blog/index.html">Blog</a>
                </li> -->
                <li class="nav-item">
                    <a href="{{FACEBOOK_URL}}" class="nav-link text-twitter" target="_blank" style="font-size: 20px;">
                        <i class="fa-brands fa-facebook"></i>
                    </a>
                </li>
                <!-- <li class="nav-item d-none d-lg-inline-block">
                    <a href="https://github.com/StartBootstrap" class="nav-link text-github">
                    <i class="fab fa-github"></i>
                    </a>
                </li>
                <li class="nav-item d-none d-lg-inline-block">
                    <a href="https://startbootstrap-slack.herokuapp.com/" class="nav-link">
                    <i class="fab fa-slack"></i>
                    </a>
                </li> -->
            </ul>
        </div>
    </div>
</nav>
