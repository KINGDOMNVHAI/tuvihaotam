
<div class="container">
    <div class="text-center">
        <h4 class="mt-0 mb-1">Các bài viết khác</h4>
        <!-- <p class="mb-4 font-weight-light">Looking for something more? Try one of Start Bootstrap's premium themes!</p> -->
    </div>
    <div class="row justify-content-center">

        @foreach ($listPostRecent as $post)
        <div class="col-md-6 col-lg-3 mb-4">
            <div class="item-preview">
                <a class="item-preview-img box-shadow-lg d-block" href="{{route('frontend-post', $post->url_post)}}"
                    onclick="if (!window.__cfRLUnblockHandlers) return false; ga('send','event','Premium Link','click','SB UI Kit Pro')"
                    rel="nofollow" data-cf-modified-6bd821c3320aa44a83fa14e1-="">
                    <img class="lazy img-fluid" data-src="{{asset('upload/img/thumbnail/post/' . $post->thumbnail_post)}}" alt="{{$post->name_post}}">
                </a>
                <h6 class="mt-3 mb-0">{{$post->name_post}}</h6>
                <!-- <div class="small">A premium Bootstrap 4 UI Kit</div> -->
            </div>
        </div>
        @endforeach

    </div>
</div>

<footer>
    <div class="footer-main bg-dark py-5 small">
        <div class="container">
            Tử Vi Anh Liễu là trang web được tạo bởi <a href="{{DOMAIN_KAWAIICODE}}">KAWAII CODE</a>.
            <br>
            <!-- <a href="#">Số ĐT</a> -->
            <br>
        </div>
    </div>
</footer>

<script id="dsq-count-scr" src="{{ asset('frontend/js/count.js') }}" async type="6bd821c3320aa44a83fa14e1-text/javascript"></script>
<script src="{{ asset('frontend/js/jquery-3-3-1.min.js') }}" type="text/javascript"></script>
{{-- <script src="{{ asset('frontend/js/jquery-3.3.1.min.js') }}" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous" type="6bd821c3320aa44a83fa14e1-text/javascript"></script> --}}
<script src="{{ asset('frontend/js/bootstrap.bundle.min.js') }}" integrity="sha384-zDnhMsjVZfS3hiP7oCBRmfjkQC4fzxVxFhBx8Hkz2aZX8gEvA/jsP3eXRCvzTofP" crossorigin="anonymous" type="6bd821c3320aa44a83fa14e1-text/javascript"></script>
<script type="6bd821c3320aa44a83fa14e1-text/javascript" src="{{ asset('frontend/js/jquery.lazy.min.js') }}"></script>
<script type="6bd821c3320aa44a83fa14e1-text/javascript" src="{{ asset('frontend/js/jquery.lazy.plugins.min.js') }}"></script>
<script src="{{ asset('frontend/js/scripts.js') }}" type="6bd821c3320aa44a83fa14e1-text/javascript"></script>
<script src="{{ asset('frontend/js/rocket-loader.min.js') }}" data-cf-settings="6bd821c3320aa44a83fa14e1-|49" defer=""></script>
<script id="dsq-count-scr" src="{{ asset('frontend/js/bootstrap.min.js') }}" async type="6bd821c3320aa44a83fa14e1-text/javascript"></script>
<script src="{{ asset('frontend/fonts/fontawesome-free/js/all.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('frontend/js-nvhai.js') }}" type="text/javascript"></script>
