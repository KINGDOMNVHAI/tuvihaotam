<script type="6bd821c3320aa44a83fa14e1-text/javascript">
    var host = "startbootstrap.com";
    if ((host == window.location.host) && (window.location.protocol != "index.html"))
    window.location.protocol = "https";
</script>
<meta charset="utf-8">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="author" content="Start Bootstrap">
<meta name="google-site-verification" content="37Tru9bxB3NrqXCt6JT5Vx8wz2AJQ0G4TkC-j8WL3kw">

<title>{{$title}} || Tử Vi Hảo Tâm</title>

<meta name="description" content="tử vi hảo tâm">

<link rel="canonical" href="index.html">

<link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

<link rel="stylesheet" href="{{ asset('frontend/fonts/fontawesome-free/css/all.min.css') }}" integrity="sha384-ekOryaXPbeCpWQNxMwSWVvQ0+1VrStoPJq54shlYhR8HzQgig1v5fas6YgOqLoKz" crossorigin="anonymous">

<link rel="stylesheet" href="{{ asset('frontend/assets/style-default.css') }}">

<link type="application/atom+xml" rel="alternate" href="feed.xml" title="startbootstrap" />
<script type="6bd821c3320aa44a83fa14e1-text/javascript">
    // Google Analytics Tracking Script
    (function(i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function() {
        (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
        m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '../www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-38417733-17', 'startbootstrap.com');
    ga('send', 'pageview');
</script>
<meta name='ir-site-verification-token' value='-28223945'>

<meta property="og:title" content="Free Bootstrap Themes, Templates, Snippets, and Guides">
<meta property="og:site_name" content="Start Bootstrap">
<meta property="og:type" content="website">
<meta property="og:description" content="Start Bootstrap develops free to download, open source Bootstrap 4 themes, templates, and snippets and creates guides and tutorials to help you learn more about designing and developing with Bootstrap.">
<meta property="og:image" content="https://startbootstrap.com/assets/img/branding/og-start-bootstrap.png">
<meta property="og:url" content="https://startbootstrap.com/">
<meta property="og:image:alt" content="Free Bootstrap Themes, Templates, Snippets, and Guides">

<link rel="apple-touch-icon" sizes="180x180" href="{{asset('apple-touch-icon.png')}}">
<link rel="icon" type="image/png" sizes="32x32" href="{{asset('favicon-32x32.png')}}">
<link rel="icon" type="image/png" sizes="16x16" href="{{asset('favicon-16x16.png')}}">
{{-- <link rel="manifest" href="assets/img/icons/site.webmanifest">
<link rel="mask-icon" href="assets/img/icons/safari-pinned-tab.svg" color="#dd3d31"> --}}
<meta name="msapplication-TileColor" content="#dd3d31">
<meta name="theme-color" content="#ffffff">
<script type="6bd821c3320aa44a83fa14e1-text/javascript" src="{{ asset('frontend/m.servedby-buysellads.com/monetization.js') }}"></script>

<script type="6bd821c3320aa44a83fa14e1-text/javascript">
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    '../connect.facebook.net/en_US/fbevents.js');
    fbq('init', '581173015940795');
    fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=581173015940795&amp;ev=PageView&amp;noscript=1"/></noscript>
