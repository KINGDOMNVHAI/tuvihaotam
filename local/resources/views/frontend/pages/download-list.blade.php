@extends('frontend.master-frontend')

@section('content')

<div class="container" style="margin-top:10px;">

    <div class="container" style="margin-top:10px;">
        <h3>Download</h3>
    </div>
    <!-- Page Content -->
    <div class="container" style="margin-top:10px;">
        <div class="row">
            <div class="col-8">
                <div class="row">

                    @foreach ($listDownload as $download)
                    <div class="col-md-4">
                        <div class="item-preview mb-5">
                            <a class="item-preview-img box-shadow-lg d-block mb-3" href="{{route('frontend-download', $download->url_download)}}">
                            <img class="lazy img-fluid" src="{{asset('upload/img/thumbnail/document/' . $download->thumbnail_download)}}" alt="{{$download->name_download}}">
                            </a>
                            <div class="item-preview-title">{{$download->name_download}}</div>
                        </div>
                    </div>
                    @endforeach

                </div>
            </div>

            @include('frontend.block.widget')
        </div>
    </div>
    <div class="container" style="margin-top:10px;">
        <div class="row">

        </div>
    </div>
</div>

@endsection
