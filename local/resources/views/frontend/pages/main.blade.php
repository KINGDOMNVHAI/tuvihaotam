@extends('frontend.master-frontend')

@section('content')

<section class="py-5 bg-light">
    <div class="mb-5 sb-nav-tabs-wrapper">
        <div class="container">
            <ul class="nav nav-tabs sb-nav-tabs border-0" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link nav-link-theme active" id="theme-tab" data-toggle="tab" href="#theme" role="tab" aria-controls="home" aria-selected="true">Kiến thức</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link nav-link-template" id="template-tab" data-toggle="tab" href="#template" role="tab" aria-controls="profile" aria-selected="false">Người nổi tiếng</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="tab-content mb-5 mb-lg-0" id="myTabContent">
                    <div class="tab-pane fade show active" id="theme" role="tabpanel" aria-labelledby="theme-tab">
                        <div class="row">
                            @foreach ($listPostTuVi as $post)
                            <div class="col-md-4">
                                <div class="item-preview mb-5">
                                    <a class="item-preview-img box-shadow-lg d-block mb-3" href="{{route('frontend-post', $post->url_post)}}">
                                        <img class="lazy img-fluid" src="{{asset('upload/img/thumbnail/post/' . $post->thumbnail_post)}}" alt="{{$post->name_post}}">
                                    </a>
                                    <div class="item-preview-title">{{$post->name_post}}</div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>

                    <div class="tab-pane fade" id="template" role="tabpanel" aria-labelledby="snippet-tab">
                        <div class="row">

                            @foreach ($listPostFamous as $famous)
                            <div class="col-md-4">
                                <div class="item-preview mb-5">
                                    <a class="item-preview-img box-shadow-lg d-block mb-3" href="{{route('frontend-post', $famous->url_post)}}">
                                        <img class="lazy img-fluid" src="{{asset('upload/img/thumbnail/post/' . $famous->thumbnail_post)}}" alt="{{$famous->name_post}}">
                                    </a>
                                    <div class="item-preview-title">{{$famous->name_post}}</div>
                                </div>
                            </div>
                            @endforeach

                            {{-- <div class="col-md-12 text-center">
                                <a href="snippets/index.html" class="btn btn-xl btn-snippet text-white shadow">Browse All Snippets</a>
                            </div> --}}
                        </div>
                    </div>
                </div>
            </div>

            @include('frontend.block.widget')
        </div>
    </div>
</section>

@endsection
