@extends('frontend.master-frontend')

@section('content')

<div class="container" style="margin-top:10px;">

    <div class="container" style="margin-top:10px;">
        <h3>Giới thiệu và hướng dẫn</h3>
    </div>
    <!-- Page Content -->
    <div class="container" style="margin-top:10px;">
        <div class="row">
            <div class="col-8">
                <p>Tử Vi Anh Liễu là website về Tử Vi, Phong Thủy. Nguyện vọng của chúng tôi là mong muốn giúp đỡ mọi người giải đáp vấn đề của mình thông qua bộ môn Tử Vi.</p>

                <h2>Hướng dẫn gửi yêu cầu đến Tử Vi Anh Liễu:</h2>

                <p>Gửi tin nhắn đến Fanpage Tử Vi Anh Liễu. Ghi rõ ngày tháng năm Âm hoặc Dương và giờ sinh.</p>
                <p>Mô tả về:</p>
                <ul>
                    <li>Bản mệnh: đương số tính cách thế nào, làm nghề gì?
                    <li>Hoàn cảnh gia đình: ba mẹ ly thân, làm ăn xa. Nhà bao nhiêu anh chị em?
                    <li>Bạn bè ít hay nhiều?
                    <li>Có thể gửi hình chân dung nếu cần. Không cần gửi chỉ tay.
                </ul>

                <p>Tử Vi Anh Liễu sẽ trả lời xác nhận bạn đã gửi tin nhắn. Và khi nào Tử Vi Anh Liễu có bài viết sẽ gửi link bài viết đến cho bạn.</p>

                <h3>Giá cả:</h3>
                <ul>
                    <li>Luận bằng văn bản: 150.000 VND
                    <li>Gọi video Google Meet: 300.000 VND
                </ul>

                <p>Tuy nhiên, có rất nhiều trường hợp gửi sai lá số.<br>
                Vì vậy khi nào chắc chắn đúng lá số thì khách hàng mới gửi tiền. Ai không chắc về lá số có thể hỏi, Tử Vi Anh Liễu sẽ tìm giúp.<br>
                Nếu không, bài luận sẽ nói sơ lược cuộc đời. Ví dụ: lá số này Quan Lộc vô chính diệu là làm nhiều nghề, nghề không cố định.</p>

                <h3>Chuyển khoản:</h3>
                <p>Ngân hàng TMCP Tiên Phong. NGUYỄN VIỆT HẢI 0184 1733 201</p>
                <p>Nội dung chuyển khoản: ngày/tháng/năm âm/dương lịch giờ sinh</p>
                <p>Ví dụ: 1/6/2020 Dương Giờ Thìn.</p>

                <img src="{{ asset('frontend/assets/img/tu-vi-anh-lieu-tai-khoan.jpg') }}" alt="HẢO TÂM" width="100%"><br><br>

                <p>Trên tinh thần giúp người tích đức, mong mọi người hãy bình luận, hỏi đáp một cách thân thiện và học hỏi. Nếu có gì sai sót, Tử Vi Anh Liễu sẽ lắng nghe và tiếp thu.</p>
                <p>Xin cảm ơn.</p>
            </div>

            @include('frontend.block.widget')
        </div>
    </div>
    <div class="container" style="margin-top:10px;">
        <div class="row">

        </div>
    </div>
</div>

@endsection
