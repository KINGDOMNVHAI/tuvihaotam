@extends('frontend.master-frontend')

@section('content')

<div class="container" style="margin-top:10px;">

    <div class="container" style="margin-top:10px;">
        <h3>{{$post->name_post}}</h3>
    </div>
    <!-- Page Content -->
    <div class="container" style="margin-top:10px;">
        <div class="row">
            <div class="col-8">
                {!! $post->content_post !!}
            </div>

            @include('frontend.block.widget')
        </div>
    </div>
    <div class="container" style="margin-top:10px;">
        <div class="row">

        </div>
    </div>
</div>

@endsection
