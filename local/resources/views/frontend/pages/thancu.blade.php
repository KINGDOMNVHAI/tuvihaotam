@extends('frontend.master-frontend')

@section('content')

<div class="container" style="margin-top:10px;">
    <div class="accordion" id="accordionExample">
        <?php $i = 1 ?>
        @foreach($viewCungThanCu as $cungthancu)
        <div class="card">
            <div class="card-header" id="headingOne">
                <h2 class="mb-0">
                    <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapse{{$i}}" aria-expanded="false" aria-controls="collapse{{$i}}">
                    {{ $cungthancu->name_cung }}
                    </button>
                </h2>
            </div>
            <div id="collapse{{$i}}" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                <div class="card-body">
                    {!! $cungthancu->luanlaso_thancu !!}
                </div>
            </div>
        </div>
        <?php $i++ ?>
        @endforeach
    </div>
</div>

@endsection
