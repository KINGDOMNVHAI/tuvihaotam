@extends('frontend.master-frontend')

@section('content')

<div class="container" style="margin-top:10px;">

    <div class="container" style="margin-top:10px;">
        <h3>{{$download->name_download}}</h3>
    </div>
    <!-- Page Content -->
    <div class="container" style="margin-top:10px;">
        <div class="row">
            <div class="col-8">
                {!! $download->description_download !!}

                <div class="row">
                    <a class="btn btn-secondary" href="{{$download->link_download}}" target="_blank" title="{{$download->name_download}}">Link Download</a>
                </div>
            </div>

            @include('frontend.block.widget')
        </div>
    </div>
    <div class="container" style="margin-top:10px;">
        <div class="row">

        </div>
    </div>
</div>

@endsection
