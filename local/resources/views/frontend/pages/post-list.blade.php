@extends('frontend.master-frontend')

@section('content')

<div class="container" style="margin-top:10px;">

    <div class="container" style="margin-top:10px;">
        <h3>{{$title}}</h3>
    </div>
    <!-- Page Content -->
    <div class="container" style="margin-top:10px;">
        <div class="row">
            <div class="col-8">
                <div class="row">

                    <blockquote>
                        <p>{{$description}}</p>
                    </blockquote>

                    @foreach ($listPost as $post)
                    <div class="col-md-4">
                        <div class="item-preview mb-5">
                            <a class="item-preview-img box-shadow-lg d-block mb-3" href="{{route('frontend-post', $post->url_post)}}">
                            <img class="lazy img-fluid" src="{{asset('upload/img/thumbnail/post/' . $post->thumbnail_post)}}" alt="{{$post->name_post}}">
                            </a>
                            <div class="item-preview-title">{{$post->name_post}}</div>
                        </div>
                    </div>
                    @endforeach

                </div>
            </div>

            @include('frontend.block.widget')
        </div>
    </div>
    <div class="container" style="margin-top:10px;">
        <div class="row">

        </div>
    </div>
</div>

@endsection
