<!DOCTYPE html>
<html lang="en">
<head>
    @include('frontend.block.meta')
</head>
<body>
    @include('frontend.block.navbar')

<main>
    @include('frontend.block.header')

    @yield('content')
</main>

<hr class="mb-5 custom">

@include('frontend.block.footer')

</body>
</html>
