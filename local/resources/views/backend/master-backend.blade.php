<!DOCTYPE html>
<html>
<head>
    <title>SB Admin 2 - Dashboard</title>
    @include('backend.block.meta')
</head>
<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">
        @include('backend.block.sidebar')

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">
                @include('backend.block.topbar')

                @yield('content')
            </div>
            <!-- End of Main Content -->

            @include('backend.block.footer')

        </div>
        <!-- End of Content Wrapper -->
    </div>
</body>
</html>
