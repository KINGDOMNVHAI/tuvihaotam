@extends('backend.master-backend')

@section('content')

<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-4 text-gray-800">Tạo bài viết mới</h1>

<div class="row">
    <div class="col-lg-12">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">New Post</h6>
            </div>
            <div class="card-body">
                <form action="" method="post">
                    <input type="hidden" class="form-control bg-light border-0 small" name="postID" value="1">
                    <input type="hidden" class="form-control bg-light border-0 small" name="postDate" value="1">

                    <p>Tiêu đề</p>
                    <input type="text" class="form-control bg-light border-0 small" placeholder="Title"><br>

                    <p>Giới thiệu</p>
                    <textarea type="text" class="form-control bg-light border-0 small" name="postPresent" rows="2" cols="20"></textarea><br>

                    <p>Nội dung</p>
                    <textarea type="text" class="form-control bg-light border-0 small" name="postDesc" rows="6" cols="50"></textarea><br>

                    <p>Thumbnail</p>
                    <input type="file" id="myFile" class="form-control bg-light border-0 small" name="postThumbnail"><br>

                    <button href="#" class="btn btn-primary btn-icon-split">
                        <span class="text">Đăng bài</span>
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Xem trước bài viết</h6>
            </div>
            <div class="card-body">

            </div>
        </div>
    </div>
</div>
</div>
<!-- /.container-fluid -->

@endsection
