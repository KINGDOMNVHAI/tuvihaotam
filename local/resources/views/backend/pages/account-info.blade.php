@extends('backend.master-backend')

@section('content')

<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Thông tin tài khoản</h1>

    <div class="row">
        <div class="col-lg-6">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Account Information</h6>
                </div>
                <div class="card-body">
                    <form action="" method="post">
                        <input type="hidden" class="form-control bg-light border-0 small" name="accountID"
                            value="1">
                        <input type="hidden" class="form-control bg-light border-0 small" name="accountDate"
                            value="1">

                        <p>Tên đăng nhập</p>
                        <input type="text" class="form-control bg-light border-0 small" name="accountUsername"
                            placeholder="Username"><br>

                        <p>Giới thiệu bản thân</p>
                        <textarea type="text" class="form-control bg-light border-0 small"
                            name="accountPresent" rows="2" cols="20"></textarea><br>

                        <p>Phone</p>
                        <input type="text" class="form-control bg-light border-0 small" name="accountPhone"
                            placeholder="012345678"><br>

                        <p>Facebook</p>
                        <input type="text" class="form-control bg-light border-0 small" name="accountFacebook"
                            placeholder="Facebook"><br>

                        <p>Avatar</p>
                        <input type="file" id="accountAvatar" class="form-control bg-light border-0 small"
                            name="accountAvatar"><br>

                        <button type="submit" class="btn btn-primary btn-icon-split">
                            <span class="text">Đăng bài</span>
                        </button>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Thay đổi mật khẩu</h6>
                </div>
                <div class="card-body">
                    <form action="" method="post">
                        <p>Mật khẩu hiện tại</p>
                        <input type="text" class="form-control bg-light border-0 small" name="accountPassword" placeholder="Password"><br>

                        <p>Mật khẩu mới</p>
                        <input type="text" class="form-control bg-light border-0 small" name="accountPassword" placeholder="Password"><br>

                        <p>Xác nhận mật khẩu</p>
                        <input type="text" class="form-control bg-light border-0 small" name="accountPassword" placeholder="Password"><br>

                        <button type="submit" class="btn btn-primary btn-icon-split">
                            <span class="text">Đổi mật khẩu</span>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid -->

@endsection
