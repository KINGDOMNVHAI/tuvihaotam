<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class categorydownload extends Model
{
    //Khai báo tên table
    protected $table = 'categorydownload';

    // Khai báo primary key
    // Trong Laravel có categorydownload::find() nghĩa là tìm theo primary key
    protected $primaryKey = 'id_cat_download';

    // Bỏ updated_at
    public $timestamps = false;

    protected $fillable = [
        'id_cat_download', 'name_vi_cat_download', 'url_cat_download', 'enable_cat_download'
    ];
}
