<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class download extends Model
{
    //Khai báo tên table
    protected $table = 'download';

    // Khai báo primary key
    // Trong Laravel có download::find() nghĩa là tìm theo primary key
    protected $primaryKey = 'id_download';

    // Bỏ updated_at
    public $timestamps = false;

    protected $fillable = [
        'id_download', 'title_vi_download', 'url_download', 'description_vi_download',
        'thumbnail_download', 'link_download', 'id_cat_download', 'enable_download', 'alert'
    ];
}
