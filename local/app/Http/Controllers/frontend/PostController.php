<?php
namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use App\Services\CategoryService;
use App\Services\PostService;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    public function __construct()
    {
        // Get title from parent class
        $this->title = "Trang chủ";
    }

    public function list($urlCat)
    {
        $title = $this->title;
        $description = '';
        $categoryService = new CategoryService();
        $listIDTuVi = $categoryService->findChildCategoryByURLCat(TUVI_CAT);
        $listIDFamous = $categoryService->findChildCategoryByURLCat(FAMOUS_PEOPLE_CAT);

        $arrIDTuVi = [];
        $arrIDFamous = [];
        foreach ($listIDTuVi as $id) {
            array_push($arrIDTuVi, $id->id_cat_post);
        }
        foreach ($listIDFamous as $id) {
            array_push($arrIDFamous, $id->id_cat_post);
        }

        $requestTuVi = [
            'keyword' => '',
            'date' => '',
            'category' => TUVI_CAT,
            'categoryIDs' => $arrIDTuVi,
        ];

        $requestFamous = [
            'keyword' => '',
            'date' => '',
            'category' => FAMOUS_PEOPLE_CAT,
            'categoryIDs' => $arrIDFamous,
        ];

        $postService = new PostService();

        if ($urlCat == TUVI_CAT) {
            $title = 'Tử vi';
            $listPost = $postService->list($requestTuVi, false, true, ITEM_PER_PAGE_12);
        } elseif ($urlCat == FAMOUS_PEOPLE_CAT) {
            $title = 'Người nổi tiếng';
            $description = 'Chuyên mục luận lá số của những người nổi tiếng trong và ngoài nước, mọi lĩnh vực, ngành nghề.';
            $listPost = $postService->list($requestFamous, false, true, ITEM_PER_PAGE_12);
        } else {
            $request = [
                'keyword' => '',
                'date' => '',
                'category' => 'all',
                'categoryIDs' => [],
            ];
            $listPost = $postService->list($request, false, true, ITEM_PER_PAGE_12);
        }

        $postService = new PostService();
        $listPostTuVi = $postService->list($requestTuVi, false, false, ITEM_PER_PAGE_12);
        $listPostRandomTuVi = $postService->list($requestTuVi, false, true, ITEM_PER_PAGE_12);
        $listPostRandomFamous = $postService->list($requestFamous, false, true, ITEM_PER_PAGE_12);
        $listPostRecent = $postService->getListRandomPost(ITEM_PER_PAGE_4);

        return view('frontend.pages.post-list', [
            'title' => $title,
            'description' => $description,
            'listPostTuVi' => $listPostTuVi,
            'listPost' => $listPost,
            'listPostRandomTuVi' => $listPostRandomTuVi,
            'listPostRandomFamous' => $listPostRandomFamous,
            'listPostRecent' => $listPostRecent,
        ]);
    }

    public function detail($urlPost)
    {
        $title = "";
        $categoryService = new CategoryService();
        $listIDTuVi = $categoryService->findChildCategoryByURLCat(TUVI_CAT);
        $listIDFamous = $categoryService->findChildCategoryByURLCat(FAMOUS_PEOPLE_CAT);

        $arrIDTuVi = [];
        $arrIDFamous = [];
        foreach ($listIDTuVi as $id) {
            array_push($arrIDTuVi, $id->id_cat_post);
        }
        foreach ($listIDFamous as $id) {
            array_push($arrIDFamous, $id->id_cat_post);
        }

        $requestTuVi = [
            'keyword' => '',
            'date' => 'desc',
            'category' => TUVI_CAT,
            'categoryIDs' => $arrIDTuVi,
        ];

        $requestFamous = [
            'keyword' => '',
            'date' => 'desc',
            'category' => FAMOUS_PEOPLE_CAT,
            'categoryIDs' => $arrIDFamous,
        ];

        $postService = new PostService();
        $post = $postService->detail($urlPost);
        $listPostRandomTuVi = $postService->list($requestTuVi, false, true, ITEM_PER_PAGE_12);
        $listPostRandomFamous = $postService->list($requestFamous, false, true, ITEM_PER_PAGE_12);
        $listPostRecent = $postService->getListRandomPost(ITEM_PER_PAGE_4);

        if ($post == null) {
            return view('frontend.pages.404', [
                'title' => $title,
                'post' => $post,
                'listPostRandomTuVi' => $listPostRandomTuVi,
                'listPostRandomFamous' => $listPostRandomFamous,
                'listPostRecent' => $listPostRecent,
            ]);
        }

        $title = $post->name_post;

        return view('frontend.pages.post', [
            'title' => $title,
            'post' => $post,
            'listPostRandomTuVi' => $listPostRandomTuVi,
            'listPostRandomFamous' => $listPostRandomFamous,
            'listPostRecent' => $listPostRecent,
        ]);
    }
}
