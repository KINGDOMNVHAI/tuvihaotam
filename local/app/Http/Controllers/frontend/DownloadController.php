<?php
namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use App\Services\CategoryService;
use App\Services\DownloadService;
use App\Services\PostService;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DownloadController extends Controller
{
    public function __construct()
    {
        // Get title from parent class
        $this->title = "Download";
    }

    public function list($urlCat)
    {
        $categoryService = new CategoryService();
        $listIDTuVi = $categoryService->findChildCategoryByURLCat(TUVI_CAT);
        $listIDFamous = $categoryService->findChildCategoryByURLCat(FAMOUS_PEOPLE_CAT);

        $arrIDTuVi = [];
        $arrIDFamous = [];
        foreach ($listIDTuVi as $id) {
            array_push($arrIDTuVi, $id->id_cat_post);
        }
        foreach ($listIDFamous as $id) {
            array_push($arrIDFamous, $id->id_cat_post);
        }

        $requestTuVi = [
            'keyword' => '',
            'date' => 'desc',
            'category' => TUVI_CAT,
            'categoryIDs' => $arrIDTuVi,
        ];

        $requestFamous = [
            'keyword' => '',
            'date' => 'desc',
            'category' => FAMOUS_PEOPLE_CAT,
            'categoryIDs' => $arrIDFamous,
        ];

        $downloadService = new DownloadService();
        $listDownload = $downloadService->list($requestTuVi, false);

        $postService = new PostService();
        $listPostRandomTuVi = $postService->list($requestTuVi, false, true, ITEM_PER_PAGE_4);
        $listPostRandomFamous = $postService->list($requestFamous, false, true, ITEM_PER_PAGE_4);
        $listPostRecent = $postService->getListRandomPost(ITEM_PER_PAGE_4);

        return view('frontend.pages.download-list', [
            'title' => $this->title,
            'listDownload' => $listDownload,
            'listPostRandomTuVi' => $listPostRandomTuVi,
            'listPostRandomFamous' => $listPostRandomFamous,
            'listPostRecent' => $listPostRecent,
        ]);
    }

    public function detail($urlDownload)
    {
        $categoryService = new CategoryService();
        $listIDTuVi = $categoryService->findChildCategoryByURLCat(TUVI_CAT);
        $listIDFamous = $categoryService->findChildCategoryByURLCat(FAMOUS_PEOPLE_CAT);

        $arrIDTuVi = [];
        $arrIDFamous = [];
        foreach ($listIDTuVi as $id) {
            array_push($arrIDTuVi, $id->id_cat_post);
        }
        foreach ($listIDFamous as $id) {
            array_push($arrIDFamous, $id->id_cat_post);
        }

        $requestTuVi = [
            'keyword' => '',
            'date' => '',
            'category' => 'all',
            'categoryIDs' => $arrIDTuVi,
        ];

        $requestFamous = [
            'keyword' => '',
            'date' => '',
            'category' => FAMOUS_PEOPLE_CAT,
            'categoryIDs' => $arrIDFamous,
        ];

        $downloadService = new DownloadService();
        $download = $downloadService->detail($urlDownload);

        $postService = new PostService();
        $listPostRandomTuVi = $postService->list($requestTuVi, false, true, ITEM_PER_PAGE_12);
        $listPostRandomFamous = $postService->list($requestFamous, false, true, ITEM_PER_PAGE_12);
        $listPostRecent = $postService->getListRandomPost(ITEM_PER_PAGE_4);

        return view('frontend.pages.download', [
            'title' => $this->title,
            'download' => $download,
            'listPostRandomTuVi' => $listPostRandomTuVi,
            'listPostRandomFamous' => $listPostRandomFamous,
            'listPostRecent' => $listPostRecent,
        ]);
    }
}
