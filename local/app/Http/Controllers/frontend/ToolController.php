<?php
namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
// use App\Model\productdetail;
use App\Services\ToolService;
use Cart;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ToolController extends Controller
{
    public function __construct()
    {
        // Get title from parent class
        $this->title = "Tool luận lá số" . $this->title;

        $tooldata = new ToolService;
        $this->viewCung = $tooldata->cung();
        $this->viewChinhTinh = $tooldata->chinhtinh();
        $this->viewPhuTinh = $tooldata->phutinh();
        $this->viewVongThaiTue = $tooldata->vongthaitue();
    }




    public function index()
    {
        // Phát triển tool từ từ
        // Học cách xét Lai Nhân Cung, Thân Cư

    }












    public function thancu()
    {
        $content = "";

        $tooldata = new ToolService;
        $cungthancu = $tooldata->thancu(null);

        return view('frontend.pages.thancu', [
            'title' => $this->title,
            'content' => $content,

            'viewCungThanCu' => $cungthancu
        ]);
    }

    public function bosao(Request $request)
    {
        $content = "";

        return view('frontend.pages.bosao', [
            'title' => $this->title,
            'content' => $content,

            'viewChinhTinh' => $this->viewChinhTinh
        ]);
    }

    public function bosaosearch(Request $request)
    {
        $content = "";

        $chinhtinh = $request->input['chinhtinh'];

        dd($chinhtinh);



        return view('frontend.pages.bosao', [
            'title' => $this->title,
            'content' => $content,

            'viewChinhTinh' => $this->viewChinhTinh
        ]);
    }







    public function search(Request $request)
    {
        $content = "";

        // Bước 1: kiểm tra có cung VCD không.
        // Chính tinh
        $thaiDuong = $request["thai-duong-cung"];
        $thaiAm = $request["thai-am-cung"];
        $thienCo = $request["thien-co-cung"];
        $thienTuong = $request["thien-tuong-cung"];
        $thienLuong = $request["thien-luong-cung"];
        $tuVi = $request["tu-vi-cung"];
        $cuMon = $request["cu-mon-cung"];
        $thienDong = $request["thien-dong-cung"];
        $vuKhuc = $request["vu-khuc-cung"];
        $thienPhu = $request["thien-phu-cung"];
        $liemTrinh = $request["liem-trinh-cung"];
        $thamLang = $request["tham-lang-cung"];
        $phaQuan = $request["pha-quan-cung"];
        $thatSat = $request["that-sat-cung"];

        // $thaiDuong = PHU_THE_URL;
        // $thaiAm = BAN_MENH_URL;
        // $thienCo = BAN_MENH_URL;
        // $thienTuong = DIEN_TRACH_URL;
        // $thienLuong = QUAN_LOC_URL;
        // $tuVi = PHU_MAU_URL;
        // $cuMon = PHUC_DUC_URL;
        // $thienDong = TAI_BACH_URL;
        // $vuKhuc = TU_TUC_URL;
        // $thienPhu = HUYNH_DE_URL;
        // $liemTrinh = NO_BOC_URL;
        // $thamLang = PHU_MAU_URL;
        // $phaQuan = TU_TUC_URL;
        // $thatSat = NO_BOC_URL;
        // $cungvcd = $request["cungvcd"];

        $chinhTinh = array(
            $thaiDuong, $thaiAm,
            $thienCo, $thienTuong,
            $thienLuong, $tuVi,
            $cuMon, $thienDong,
            $vuKhuc, $thienPhu,
            $liemTrinh, $thamLang,
            $phaQuan, $thatSat,
        );
        $thancu = $request["thancu"];

        $tooldata = new ToolService;
        // Bước 2: tìm các cung Vô Chính Diệu và Thân Cư
        $cungthancu = $tooldata->thancu($thancu);
        $content = $content . "<h4>Thân Cư " . $cungthancu[0]->name_cung . "</h4>" . $cungthancu[0]->luanlaso_thancu. "<br>";

        $vcd = $tooldata->findVCD($chinhTinh);
        $content = $content . "<h3>Các cung Vô Chính Diệu</h3>";

        if (count($vcd) > 0)
        {
            foreach ($vcd as $vochinhdieu)
            {
                $content = $content . "<h4>" . $vochinhdieu->name_cung . " VCD</h4>" . $vochinhdieu->luanlaso_cungvcd. "<br>";
            }

            // Bước 3: từ chính tinh suy ra bộ sao.
            // Đầu tiên là Mệnh. Nếu là Mệnh VCD thì kiểm tra Quan Lộc. Nếu Quan Lộc VCD thì kiểm tra Tài Bạch

            // $countVCD = 0; // Bộ sao chỉ tối đa 2 cung VCD
            // for ($i = 1; $i <= $arrlength; $i++)
            // {
            //     if ($vcd[i]['url_cung'] == 'ban-menh')
            //     {
            //         var_dump($vcd[i]['url_cung']);
            //         $countVCD++;
            //     }
            //     else if ($vcd[i]['url_cung'] == 'quan-loc')
            //     {
            //         var_dump($vcd[i]['url_cung']);
            //         $countVCD++;
            //     }
            //     else if ($vcd[i]['url_cung'] == 'tai-bach')
            //     {
            //         var_dump($vcd[i]['url_cung']);
            //         $countVCD++;
            //     }

            //     if ($countVCD == 2)
            //     {
            //         break;
            //     }
            // }

        }
        // Bước 4: kiểm tra phụ tinh quan trọng, từ đó suy ra vòng Thái Tuế.


        return view('frontend.master', [
            'title' => $this->title,
            'content' => $content,

            'viewCung' => $this->viewCung,
            'viewChinhTinh' => $this->viewChinhTinh,
            'viewPhuTinh' => $this->viewPhuTinh,
            'viewVongThaiTue' => $this->viewVongThaiTue,
        ]);
    }

    private function chinhTinhMenh()
    {
        // Thái Dương
        // Bộ sao: Thái Dương Cự Môn, Thái Dương Thái Âm, Thái Dương Thiên Lương
        if ($thaiDuong == "ban-menh")
        {
            if ($thaiAm == "ban-menh")
            {
                var_dump("Thái Dương Thái Âm");die;
            }
            else if ($cuMon == "ban-menh")
            {
                var_dump("Thái Dương Cự Môn");die;
            }
            else if ($thienLuong == "ban-menh")
            {
                var_dump("Thái Dương Thiên Lương");die;
            }
            else
            {
                $motChinhTinh = $tooldata->motChinhTinh(THAI_DUONG_URL);
                var_dump($motChinhTinh["luanlaso_sao"]);die;
            }
        }
        // Thái Âm
        // Bộ sao: Thái Dương Thái Âm, Thiên Cơ Thái Âm
        else if ($thaiAm == "ban-menh")
        {
            if ($thienCo == "ban-menh")
            {
                var_dump("Cơ Nguyệt");die;
            }
            else
            {
                $motChinhTinh = $tooldata->motChinhTinh(THAI_AM_URL);
                var_dump($motChinhTinh["luanlaso_sao"]);die;
            }
        }
        // Thiên Cơ
        // Bộ sao: Thiên Cơ Thái Âm, Thiên Cơ Cự Môn, Thiên Cơ Thiên Lương
        else if ($thienCo == "ban-menh")
        {
            if ($thienLuong == "ban-menh")
            {
                var_dump("Cơ Lương");die;
            }
            else if ($cuMon == "ban-menh")
            {
                var_dump("Cơ Cự");die;
            }
            else
            {
                $motChinhTinh = $tooldata->motChinhTinh(THIEN_CO_URL);
                var_dump($motChinhTinh["luanlaso_sao"]);die;
            }
        }
        // Thiên Tướng
        // Bộ sao: Tử Vi Thiên Tướng, Liêm Trinh Thiên Tướng, Vũ Khúc Thiên Tướng
        else if ($thienTuong == "ban-menh")
        {
            if ($tuVi == "ban-menh")
            {
                var_dump("Tử Vi Thiên Tướng");die;
            }
            else if ($liemTrinh == "ban-menh")
            {
                var_dump("Liêm Trinh Thiên Tướng");die;
            }
            else if ($vuKhuc == "ban-menh")
            {
                var_dump("Vũ Khúc Thiên Tướng");die;
            }
            else
            {
                $motChinhTinh = $tooldata->motChinhTinh(THIEN_TUONG_URL);
                var_dump($motChinhTinh["luanlaso_sao"]);die;
            }
        }
        // Thiên Lương
        // Bộ sao: Thái Dương Thiên Lương, Thiên Cơ Thiên Lương, Thiên Đồng Thiên Lương
        else if ($thienLuong == "ban-menh")
        {
            if ($thienDong == "ban-menh")
            {
                var_dump("Thiên Đồng Thiên Lương");die;
            }
            else
            {
                $motChinhTinh = $tooldata->motChinhTinh(THIEN_LUONG_URL);
                var_dump($motChinhTinh["luanlaso_sao"]);die;
            }
        }
        // Tử Vi
        // Bộ sao: Tử Vi Thiên Tướng, Tử Vi Tham Lang, Tử Vi Thiên Phủ
        else if ($tuVi == "ban-menh")
        {
            if ($thamLang == "ban-menh")
            {
                var_dump("Tử Vi Tham Lang");die;
            }
            else if ($thienPhu == "ban-menh")
            {
                var_dump("Tử Vi Thiên Phủ");die;
            }
            else
            {
                $motChinhTinh = $tooldata->motChinhTinh(TU_VI_URL);
                var_dump($motChinhTinh["luanlaso_sao"]);die;
            }
        }
        else if ($cuMon == "ban-menh")
        {
            $motChinhTinh = $tooldata->motChinhTinh(CU_MON_URL);
            var_dump($motChinhTinh["luanlaso_sao"]);die;
        }
        else if ($thienDong == "ban-menh")
        {
            $motChinhTinh = $tooldata->motChinhTinh(THIEN_DONG_URL);
            var_dump($motChinhTinh["luanlaso_sao"]);die;
        }
        else if ($vuKhuc == "ban-menh")
        {
            if ($thienPhu == "ban-menh")
            {
                var_dump("Vũ Khúc Thiên Phủ");die;
            }
            else if ($thamLang == "ban-menh")
            {
                var_dump("Vũ Khúc Tham Lang");die;
            }
            else
            {
                $motChinhTinh = $tooldata->motChinhTinh(VU_KHUC_URL);
                var_dump($motChinhTinh["luanlaso_sao"]);die;
            }
        }
        else if ($thienPhu == "ban-menh")
        {
            $motChinhTinh = $tooldata->motChinhTinh(THIEN_PHU_URL);
            var_dump($motChinhTinh["luanlaso_sao"]);die;
        }
        else if ($liemTrinh == "ban-menh")
        {
            if ($phaQuan == "ban-menh")
            {
                var_dump("Liêm Trinh Phá Quân");die;
            }
            else if ($thatSat == "ban-menh")
            {
                var_dump("Liêm Trinh Thất Sát");die;
            }
            else if ($thamLang == "ban-menh")
            {
                var_dump("Liêm Trinh Tham Lang");die;
            }
            else
            {
                $motChinhTinh = $tooldata->motChinhTinh(LIEM_TRINH_URL);
                var_dump($motChinhTinh["luanlaso_sao"]);die;
            }
        }
        else if ($thamLang == "ban-menh")
        {
            $motChinhTinh = $tooldata->motChinhTinh(THAM_LANG_URL);
            var_dump($motChinhTinh["luanlaso_sao"]);die;
        }
        else if ($phaQuan == "ban-menh")
        {
            if ($thatSat == "ban-menh")
            {
                var_dump("Phá Quân Thất Sát");die;
            }
            else
            {
                $motChinhTinh = $tooldata->motChinhTinh(PHA_QUAN_URL);
                var_dump($motChinhTinh["luanlaso_sao"]);die;
            }
        }
        else if ($thatSat == "ban-menh")
        {
            $motChinhTinh = $tooldata->motChinhTinh(THAT_SAT_URL);
            var_dump($motChinhTinh["luanlaso_sao"]);die;
        }
        else
        {
            var_dump("Mệnh VCD");die;
        }
    }
}
