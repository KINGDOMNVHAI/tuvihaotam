<?php
namespace App\Services;

use Illuminate\Support\ServiceProvider;
use App\Models\posts;
use App\Models\categorypost;
use App\Ultis\StringUltis;
use DB;

class PostService extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Get List User
     *
     * @return array
     */
    public function list($request, $pageable, $random, $limit)
    {
        $result = [];
        // $stringUltis = new StringUltis();
        // $keyword = $stringUltis->removeSpecialCharacter($request->keyword);

        // Create query
        // ilike only for Postgre, not support for phpmyadmin
        // $query = posts::where('name_vi_post', 'like', $request['keyword']);

        $query = DB::table('posts')->select(
            'posts.id_post',
            'posts.name_vi_post AS name_post',
            'posts.url_post',
            'posts.present_vi_post AS present_post',
            'posts.date_post',
            'posts.thumbnail_post',
            'posts.id_cat_post',
            'categorypost.*'
        )->join('categorypost', 'categorypost.id_cat_post', '=', 'posts.id_cat_post')
        ->where('enable_post', true);

        if ($request['categoryIDs'] != null) {
            $query = $query->whereIn('posts.id_cat_post', $request['categoryIDs']);
        }

        if ($random) {
            $query = $query->orderBy(DB::raw('RAND()'));
        }

        if ($request['date'] == 'asc') {
            $query = $query->orderBy('date_post', 'asc');
        } else if ($request['date'] == 'desc') {
            $query = $query->orderBy('date_post', 'desc');
        }

        // Get result
        if ($pageable == true) {
            $result = $query->paginate($limit);
        } else {
            $result = $query->limit($limit)->get();
        }

        return $result;
    }

    /**
     * Detail post
     */
    public function detail($urlPost)
    {
        return DB::table('posts')->select(
            'posts.id_post',
            'posts.name_vi_post AS name_post',
            'posts.url_post',
            'posts.present_vi_post AS present_post',
            'posts.content_vi_post AS content_post',
            'posts.date_post',
            'posts.thumbnail_post',
            'categorypost.*'
        )->join('categorypost', 'categorypost.id_cat_post', '=', 'posts.id_cat_post')
        ->where('url_post', 'like', $urlPost)
        ->where('enable_post', true)
        ->first();
    }

    public function insert($request)
    {
        $enable = $popular = $update = 0;

        // File name mặc định không có tên
        $fileName = '';

        //Kiểm tra file
        if (Input::hasFile('thumbnail'))
        {
            // Thư mục upload
            $uploadPath = public_path('upload/images/thumbnail');
            $file = Input::file('thumbnail');

            // File name được gắn tên
            $fileName = $file->getClientOriginalName();

            // Đưa file vào thư mục
            $file->move($uploadPath, $fileName);
        }

        if ($request->enable != null)
        {
            $enable = $request->enable;
        }
        if ($request->popular != null)
        {
            $popular = $request->popular;
        }
        if ($request->update != null)
        {
            $update = $request->update;
        }

        $query = posts::insert([
            'name_detailpost'    => $request->name_detailpost,
            'url_detailpost'     => $request->url_detailpost,
            'content_detailpost' => $request->content_detailpost,
            'present_detailpost' => $request->present_detailpost,
            'date_detailpost'    => $request->date,
            'img_detailpost'     => $fileName,  // Lấy tên file
            'idCat'              => $request->id_cat_detailpost,
            'signature'          => Auth::user()->signature,
            'author'             => Auth::user()->username,
            'enable'             => $enable,
            'popular'            => $popular,
            'update'             => $update,
            'views'              => 0,
        ]);

        return $query;
    }

    public function update($datas, $request)
    {
        // File name mặc định không có tên
        $fileName = '';
        $uploadPath = 'upload/images/thumbnail';

        if ($request->hasFile('thumbnail'))
        {
            // Thư mục upload
            // $uploadPath = public_path('upload/images/thumbnail');
            $file = $request->file('thumbnail');

            // File name được gắn tên
            $fileName = $file->getClientOriginalName();

            // Đưa file vào thư mục
            $file->move($uploadPath, $fileName);
        } else {
            $fileName = $request->img;
        }

        // idUpdate là id sẽ nhập vào data
        $idUpdate = $datas['id_detailpost'];

        // Case 1: trường hợp Insert, id null
        // Case 2: trường hợp Update, id có giá trị
        if ($idUpdate == null)
        {
            // $getFinalID =  DB::table('detailpost')
            //     ->select(DB::raw('max(id_detailpost) as id_detailpost'))
            //     ->first();

            // $idUpdate = $getFinalID->id_detailpost;

            $query = posts::create([
                'name_vi_detailpost'    => $datas['name_vi_detailpost'],
                'name_en_detailpost'    => $datas['name_en_detailpost'],
                'url_detailpost'        => $datas['url_detailpost'],
                'content_vi_detailpost' => $datas['content_vi_detailpost'],
                'content_en_detailpost' => $datas['content_en_detailpost'],
                'date_detailpost'       => date('Y-m-d'),
                'present_vi_detailpost' => $datas['present_vi_detailpost'],
                'present_en_detailpost' => $datas['present_en_detailpost'],
                'img_detailpost'        => $fileName, // Lấy tên file
                'id_cat_detailpost'     => $datas['id_cat_detailpost'],
                'signature'             => Auth::user()->signature,
                'author'                => Auth::user()->username,
                'enable'                => $datas['enable'],
                'popular'               => $datas['popular'],
                'update'                => $datas['update'],
                'views'                 => random_int(10,100),
            ]);
        }
        else
        {
            $query = posts::where('id_detailpost', $idUpdate)
            ->update([
                'name_vi_detailpost'    => $datas['name_vi_detailpost'],
                'name_en_detailpost'    => $datas['name_en_detailpost'],
                'url_detailpost'        => $datas['url_detailpost'],
                'content_vi_detailpost' => $datas['content_vi_detailpost'],
                'content_en_detailpost' => $datas['content_en_detailpost'],
                'date_detailpost'       => $datas['date_detailpost'],
                'present_vi_detailpost' => $datas['present_vi_detailpost'],
                'present_en_detailpost' => $datas['present_en_detailpost'],
                'img_detailpost'        => $fileName,  // Lấy tên file
                'id_cat_detailpost'     => $datas['id_cat_detailpost'],
                'popular'               => $datas['popular'],
                'update'                => $datas['update'],
                'enable'                => $datas['enable'],
            ]);
        }

        return $query;
    }

    /**
     * Delete post
     */
    public function delete($idPost)
    {
        $query = posts::where('id_post', $idPost)->update(['enable' => 0]);
    }

    /**
     * Get newest post
     */
    public function getListNewestPost($limit,$idCat,$convertToArray = false,$language)
    {
        switch ($language)
        {
            case "en":
                $new = posts::join('categorypost', 'categorypost.id_cat_post', '=', 'posts.id_cat_post')
                ->select(
                    'posts.id_post',
                    'posts.id_cat_post',
                    'posts.name_en_post as name_post',
                    'posts.present_en_post as present_post',
                    'posts.url_post',
                    DB::raw("DATE_FORMAT(posts.date_post,'%d-%m-%Y') as date_post"),
                    'posts.thumbnail_post',

                    'categorypost.name_en_cat_post AS name_cat_post'
                )
                ->where('posts.enable', ENABLE);
            break;

            default:
                $new = posts::join('categorypost', 'categorypost.id_cat_post', '=', 'posts.id_cat_post')
                ->select(
                    'posts.id_post',
                    'posts.id_cat_post',
                    'posts.name_vi_post as name_post',
                    'posts.present_vi_post as present_post',
                    'posts.url_post',
                    DB::raw("DATE_FORMAT(posts.date_post,'%d-%m-%Y') as date_post"),
                    'posts.thumbnail_post',

                    'categorypost.name_vi_cat_post AS name_cat_post'
                )
                ->where('posts.enable', ENABLE);
        }

        if ($idCat != null)
        {
            $new = $new->where('posts.id_cat_post', '=', $idCat);
        }

        $new = $new->latest('posts.date_post')->take($limit)->get();

        // Convert object to array
        if ($convertToArray === true)
        {
            $arrNewest = [];

            $i = 0;
            foreach ($new as $viewNewest)
            {
                $arrNewest[$i]['id_post'] = $viewNewest->id_post;
                $arrNewest[$i]['id_cat_post'] = $viewNewest->id_cat_post;
                $arrNewest[$i]['name_post'] = $viewNewest->name_post;
                $arrNewest[$i]['present_post'] = $viewNewest->present_post;
                $arrNewest[$i]['url_post'] = $viewNewest->url_post;
                $arrNewest[$i]['date_post'] = $viewNewest->date_post;
                $arrNewest[$i]['thumbnail_post'] = $viewNewest->thumbnail_post;
                $arrNewest[$i]['name_cat_post'] = $viewNewest->name_cat_post;
                $i++;
            }
            return $arrNewest;
        }
        else
        {
            return $new;
        }
    }

    /**
     * Get updated post
     */
    public function getListUpdatedPost($limit,$idCat,$language)
    {
        switch ($language)
        {
            case "en":
                $query = posts::join('categorypost', 'categorypost.id_cat_post', '=', 'posts.id_cat_post')
                ->select(
                    'posts.name_en_post as name_post',
                    'posts.url_post',
                    'posts.present_en_post as present_post',
                    'posts.thumbnail_post',
                    'posts.date_post',
                    'posts.id_cat_post',
                    'posts.update',

                    'categorypost.name_en_cat_post AS name_cat_post'
                )
                ->where('posts.enable', ENABLE);
            break;

            default:
                $query = posts::join('categorypost', 'categorypost.id_cat_post', '=', 'posts.id_cat_post')
                ->select(
                    'posts.name_vi_post as name_post',
                    'posts.url_post',
                    'posts.present_vi_post as present_post',
                    'posts.thumbnail_post',
                    'posts.date_post',
                    'posts.id_cat_post',
                    'posts.update',

                    'categorypost.name_vi_cat_post AS name_cat_post'
                )
                ->where('posts.enable', ENABLE);
        }

        if ($idCat != null)
        {
            $query = $query->where('posts.id_cat_post', '=', $idCat);
        }

        $query = $query->where('posts.update', '=', UPDATE_POST)
            ->orderBy('posts.updated_at', 'desc')
            ->take($limit)
            ->get();

        return $query;
    }

    /**
     * Get recent post
     */
    public function getListRandomPost($limit)
    {
        return posts::where('enable_post', true)
                ->select(
                    'name_vi_post as name_post',
                    'url_post',
                    'present_vi_post as present_post',
                    'thumbnail_post',
                    'date_post',
                    'id_cat_post'
                )->orderBy(DB::raw('RAND()'))
                ->latest('date_post')
                ->take($limit)
                ->get();
    }

    /**
     * Get most view post
     */
    public function getListMostViewPost($limit,$idCat)
    {
        $query = posts::where('enable', ENABLE);

        if ($idCat != null)
        {
            $query = $query->where('id_cat_post', '=', $idCat);
        }

        $query = $query->orderBy('views', 'desc')
            ->latest('date_post')
            ->take($limit)
            ->get();

        return $query;
    }

    public function getListPostJoinCategoryPaginate($params)
    {
        $namePost = $params['name_post'];
        $sort = $params['sort'];

        // $query = DB::table('posts')->join('categorypost', 'posts.id_cat_post', '=', 'categorypost.id_cat_post');

        // // Nếu yêu cầu search cột cụ thể
        // if ($fields == null)
        // {
        //     $query = $query->select('posts.*', 'categorypost.*')
        //     ->where('posts.name_vi_post', 'LIKE', "%{$params['name_vi_post']}%");
        // }
        // else
        // {
        //     $query = $query->select($fields)
        //     ->where('posts.name_vi_post', 'LIKE', "%{$params['name_vi_post']}%");
        // }

        // // Nếu search yêu cầu id_cat_post
        // if ($params['id_cat_post'] && is_numeric($params['id_cat_post']))
        // {
        //     $query = $query->where('categorypost.id_cat_post', '=', "{$params['id_cat_post']}");
        // }

        // // Nếu search yêu cầu số năm
        // if ($params['year'] && is_numeric($params['year']))
        // {
        //     $query = $query->whereRaw('YEAR(posts.date_post) = ?', $params['year']);
        // }

        // // Nếu search yêu cầu số tháng
        // if ($params['year'] && is_numeric($params['month']))
        // {
        //     $query = $query->whereRaw('MONTH(posts.date_post) = ?', $params['month']);
        // }

        // Mặc định sắp xếp id giảm dần
        // $query = $query->orderBy('posts.id_post', $sort)->paginate(PAGINATE_POST_INDEX);

        $query = DB::table('posts')->join('categorypost', 'posts.id_cat_post', '=', 'categorypost.id_cat_post')
            ->select('posts.id_post'
            ,'posts.name_vi_post'
            ,'posts.url_post'
            ,'posts.present_vi_post'
            ,'posts.thumbnail_post'
            ,'posts.date_post'
            // ,'posts.update'

            , 'categorypost.id_cat_post'
            , 'categorypost.name_vi_cat_post AS name_cat_post'
            , 'categorypost.enable_cat_post'
        );

        if ($namePost != null && $namePost != '')
        {
            $query = $query->where('posts.name_vi_post', 'LIKE', "%{$params['name_post']}%")
                ->orWhere('posts.name_en_post', 'LIKE', "%{$params['name_post']}%");
        }
        $sortDefault = 'DESC';
        if ($sort != null && $sort == 'ASC')
        {
            $sortDefault = 'ASC';
        }

        $query = $query->orderBy('posts.date_post', $sortDefault)->paginate(PAGINATE_POST_INDEX);
        return $query;
    }
}
