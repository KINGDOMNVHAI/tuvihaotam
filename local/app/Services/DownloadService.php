<?php
namespace App\Services;

use Illuminate\Support\ServiceProvider;
use App\Models\download;
use App\Models\categorypost;
use App\Ultis\StringUltis;
use DB;

class DownloadService extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Get List User
     *
     * @return array
     */
    public function list($request, $pageable)
    {
        $result = [];
        // $stringUltis = new StringUltis();
        // $keyword = $stringUltis->removeSpecialCharacter($request->keyword);

        // Create query
        // ilike only for Postgre, not support for phpmyadmin
        // $query = posts::where('name_vi_post', 'like', $request['keyword']);

        $query = DB::table('download')->select(
            'download.id_download',
            'download.name_vi_download AS name_download',
            'download.url_download',
            'download.thumbnail_download',
            'categorydownload.*'
        )->join('categorydownload', 'categorydownload.id_cat_download', '=', 'download.id_cat_download')
        ->where('enable_download', true);

        // if ($request['category'] != 'all') {
        //     $query = $query->where('id_cat', $request['category']);
        // }

        // if ($request['date'] == 'asc') {
        //     $query = $query->orderBy('date_post', 'asc');
        // } else if ($request['date'] == 'desc') {
        //     $query = $query->orderBy('date_post', 'desc');
        // }

        // Get result
        if ($pageable == true) {
            $result = $query->paginate(ITEM_PER_PAGE_12);
        } else {
            $result = $query->limit(ITEM_PER_PAGE_12)->get();
        }

        return $result;
    }

    /**
     * Detail post
     */
    public function detail($urlDownload)
    {
        return DB::table('download')->select(
            'download.id_download',
            'download.name_vi_download AS name_download',
            'download.url_download',
            'download.description_vi_download AS description_download',
            'download.thumbnail_download',
            'download.link_download',
            'categorydownload.*'
        )->join('categorydownload', 'categorydownload.id_cat_download', '=', 'download.id_download')
        ->where('url_download', 'like', $urlDownload)
        ->where('enable_download', true)
        ->first();
    }
}
