<?php
namespace App\Services;

use App\Models\categorypost;
use DB;
use Illuminate\Support\ServiceProvider;

class CategoryService extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     *
     *
     * @return void
     */
    public function list($language)
    {
        $query = DB::table('categorypost')
            ->select('id_cat_post'
            , 'name_vi_cat_post AS name_cat_post'
            , 'enable_cat_post')
        ->where('enable_cat_post', ENABLE)->get();
        return $query;
    }

    public function listPaginate()
    {
        $query = categorypost::paginate(PAGINATE_POST_INDEX);

        return $query;
    }

    public function listEnable()
    {
        $query = categorypost::where('enable', ENABLE);

        return $query;
    }

    public function detail($urlCat)
    {
        $query = categorypost::where('enable_cat_post', ENABLE)
            ->where('url_cat_post', '=', $urlCat)
            ->first();

        return $query;
    }

    public function findParentCategoryByURLCat($urlCat)
    {
        $query = DB::table('categorypost')
            ->select('id_cat_post', 'parent_cat')
            ->where('enable_cat_post', ENABLE)
            ->where('url_cat_post', '=', $urlCat)
            ->first();

        return $query;
    }

    public function findChildCategoryByURLCat($urlCat)
    {
        $id = $this->detail($urlCat);

        $query = DB::table('categorypost')
            ->select('id_cat_post', 'parent_cat')
            ->where('enable_cat_post', ENABLE)
            ->where('parent_cat', '=', $id->id_cat_post)
            ->orWhere('id_cat_post', '=', $id->id_cat_post)
            ->get();

        return $query;
    }
}
