<?php
namespace App\Seeder;

use App\Models\posts;
use DB;
use Illuminate\Support\ServiceProvider;

class MenhSeeder
{
    public function index()
    {
        // --------------------------- Kim ---------------------------
        posts::create([
            'name_vi_post'    => 'Kiếm Phong Kim - Vàng trên mũi kiếm',
            'url_post'        => 'kiem-phong-kim-vang-tren-mui-kiem',
            'present_vi_post' => 'Vàng trên mũi kiếm có ý nghĩa gì?',
            'content_vi_post' => '<p>Năm sinh: 1992 Nhâm Thân, 1993 Quý Dậu.</p>
<p>Ngũ Hành: Kim</p>

<center><img src="../upload/img/post/kiem-phong-kim-vang-tren-mui-kiem-1.jpg" width="100%" alt="Kiếm Phong Kim Vàng trên mũi kiếm" /><br><br></center>

<p><b>Tính cách:</b></p>
<ul>
<li>Mạnh mẽ, cương trực, cứng rắn như kiếm.
<li>Ngông cuồng
<li>Thích cạnh tranh. Kiếm là thứ dùng trong chiến trận nên thích đấu đá, cạnh tranh.
<li>Thông minh, nhanh nhẹnh.
<li>Cả thèm chóng chán.
<li>Bảo thủ
</ul>

<p><b>Hợp với các mệnh:</b></p>
<ul>
<li>
<li>Lộ Bàng Thổ (1990, 1991)
<li>
<li>
<li>
</ul>

<center><img src="../upload/img/post/kiem-phong-kim-vang-tren-mui-kiem-2.jpg" width="100%" alt="Kiếm Phong Kim Vàng trên mũi kiếm" /><br><br></center>

<p><b>Công việc phù hợp:</b></p>
<ul>
<li>Tự do, không dựa dẫm.
<li>Làm về binh nghiệp, chính trị.
<li>Làm về những ngành năng động, sáng tạo như marketing, đồ họa...
<li>Đặc biệt là những nghề cần được trui rèn kỹ năng, giống như rèn kiếm.
</ul>

<p><b>Màu sắc tốt:</b></p>
<ul>
<li>Màu thuộc Thổ: xám, nâu, vàng đất.
<li>Màu thuộc Thủy: xanh dương.
</ul>

<p><b>Màu sắc xấu:</b></p>
<ul>
<li>Màu thuộc Hỏa: đỏ, hồng, tím.
<li>Màu thuộc Mộc: xanh lá cây.
</ul>

<center><img src="../upload/img/post/kiem-phong-kim-vang-tren-mui-kiem-3.jpg" width="100%" alt="Kiếm Phong Kim Vàng trên mũi kiếm" /><br><br></center>
',
            'date_post'         => '2023-12-01',
            'thumbnail_post'    => 'kiem-phong-kim-vang-tren-mui-kiem-thumbnail.jpg',
            'id_cat_post'       => TUVI_POST_ID_CAT,
            'views'             => random_int(50,500),
            'enable_post'       => ENABLE,
        ]);// https://tuvidongtay.com/kiem-phong-kim-la-gi.html

        // --------------------------- Mộc ---------------------------

        posts::create([
            'name_vi_post'    => 'Đại Lâm Mộc - Cổ thụ trong rừng',
            'url_post'        => 'dai-lam-moc-co-thu-trong-rung',
            'present_vi_post' => 'Cây cổ thụ có ý nghĩa gì?',
            'content_vi_post' => '<p>Năm sinh: </p>
<p>Ngũ Hành: Mộc</p>

<center><img src="../upload/img/post/dai-lam-moc-co-thu-trong-rung-1.jpg" width="100%" alt="Đại Lâm Mộc Cổ thụ trong rừng" /><br><br></center>

<p><b>Tính cách:</b></p>
<ul>
<li>
</ul>

<p><b>Hợp với các mệnh:</b></p>
<ul>
<li>
<li>
<li>
<li>
<li>
</ul>

<center><img src="../upload/img/post/dai-lam-moc-co-thu-trong-rung-2.jpg" width="100%" alt="Đại Lâm Mộc Cổ thụ trong rừng" /><br><br></center>

<p><b>Công việc phù hợp:</b></p>
<ul>
<li>
</ul>

<p><b>Màu sắc tốt:</b></p>
<ul>
<li>Màu thuộc Mộc: màu xanh rêu (xanh lá cây)
<li>Màu thuộc:
<li>Màu thuộc:
</ul>

<p><b>Màu sắc xấu:</b></p>
<ul>
<li>Màu thuộc:
</ul>
',
            'date_post'         => '2023-12-01',
            'thumbnail_post'    => 'dai-lam-moc-co-thu-trong-rung-thumbnail.jpg',
            'id_cat_post'       => TUVI_POST_ID_CAT,
            'views'             => random_int(50,500),
            'enable_post'       => UNENABLE,
        ]);



        // --------------------------- Thủy ---------------------------



        // --------------------------- Hỏa ---------------------------

        posts::create([
            'name_vi_post'    => 'Sơn Đầu Hỏa - núi Hỏa Diệm Sơn',
            'url_post'        => 'son-dau-hoa-nui-hoa-diem-son',
            'present_vi_post' => 'Lửa từ núi lửa có ý nghĩa gì?',
            'content_vi_post' => '<p>Năm sinh: 1994 Giáp Tuất, 1995 Ất Hợi.</p>
<p>Ngũ Hành: Hỏa (kết hợp với Thổ)</p>

<p>Sơn Đầu Hỏa chính là lửa của những ngọn núi lửa, hay còn gọi là dung nham. Vì là lửa trong đất nên ngũ hành là Hỏa kết hợp với Thổ. Khác với Lư Trung Hỏa là lửa trong lò, là Hỏa kết hợp với Mộc.</p>

<center><img src="../upload/img/post/son-dau-hoa-nui-hoa-diem-son-1.jpg" width="100%" alt="Sơn Đầu Hỏa Hỏa Diệm Sơn" /><br><br></center>

<p><b>Tính cách:</b></p>
<ul>
<li>Độc lập, không dựa dẫm.
<li>Nội tại mạnh mẽ ẩn bên trong cơ thể, giống như dung nham trong núi chờ phun ra.
<li>Cô độc, một ngọn núi lửa nằm một mình.
<li>Chắc chắn, nói được làm được.
<li>Coi trọng gia đình.
<li>Hay lo xa, khá bất an, giống dung nham chảy trong cơ thể.
</ul>

<p><b>Hợp với các mệnh:</b></p>
<ul>
<li>
<li>Lộ Bàng Thổ (1990, 1991): Lộ Bàng Thổ hành Thổ, kết hợp với Mộc. Vì là lửa trong đất nên Sơn Đầu Hỏa hợp với Lộ Bàng Thổ, có cây để cháy càng thêm hợp.
<li>
<li>
<li>
</ul>

<center><img src="../upload/img/post/son-dau-hoa-nui-hoa-diem-son-2.jpg" width="100%" alt="Sơn Đầu Hỏa Hỏa Diệm Sơn" /><br><br></center>

<p><b>Công việc phù hợp:</b></p>
<li>Những nghề sáng tạo, nhiều ý tưởng.
<li>Không giỏi tính toán.
<li>Làm việc độc lập
</ul>

<p><b>Màu sắc tốt:</b></p>
<ul>
<li>Màu thuộc Thổ: xám, nâu, vàng đất.
<li>Màu thuộc Mộc: xanh lá cây.
</ul>

<p><b>Màu sắc xấu:</b></p>
<ul>
<li>Màu thuộc Thủy: xanh dương.
</ul>

<center><img src="../upload/img/post/son-dau-hoa-nui-hoa-diem-son-3.jpg" width="100%" alt="Sơn Đầu Hỏa Hỏa Diệm Sơn" /><br><br>

<iframe width="100%" height="350" src="https://www.youtube.com/embed/k6Cg-9nMLfU" title="Sơn Đầu Hỏa Hỏa Diệm Sơn" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe></center>
',
            'date_post'         => '2023-12-02',
            'thumbnail_post'    => 'son-dau-hoa-nui-hoa-diem-son-thumbnail.jpg',
            'id_cat_post'       => TUVI_POST_ID_CAT,
            'views'             => random_int(50,500),
            'enable_post'       => UNENABLE,
        ]);

        posts::create([
            'name_vi_post'    => 'Thiên Thượng Hỏa - Mặt Trời',
            'url_post'        => 'thien-thuong-hoa-mat-troi',
            'present_vi_post' => 'Lửa trên trời có ý nghĩa gì?',
            'content_vi_post' => '<p>Năm sinh: </p>
<p>Ngũ Hành: Hỏa (kết hợp với )</p>

<p>Thiên Thượng Hỏa chính là lửa Mặt Trời. Thiên là trời. Thiên Thượng là trên trời. Và trên trời có lửa thì chỉ có thể là Mặt Trời.</p>

<center><img src="../upload/img/post/thien-thuong-hoa-mat-troi-1.jpg" width="100%" alt="Thiên Thượng Hỏa Mặt Trời" /><br><br></center>

<p>Đặc biệt, Thiên Thượng Hỏa và <a href="/post/tich-lich-hoa-lua-sam-set" title="Tích Lịch Hỏa lửa sấm sét" target="_blank">Tích Lịch Hỏa</a> là 2 mệnh Hỏa không bị Thủy dập tắt. Lửa Mặt Trời không dập được. Tích Lịch Hỏa là lửa sấm sét, lửa từ điện, đổ nước vào là hỏng.</p>

<center><img src="../upload/img/post/thor-1.jpg" width="100%" alt="Tích Lịch Hỏa Lửa sấm sét" /><br><br></center>

<p><b>Tính cách:</b></p>
<ul>
<li>
</ul>

<p><b>Hợp với các mệnh:</b></p>
<ul>
<li>
<li>
<li>
<li>
<li>
</ul>

<center><img src="../upload/img/post/thien-thuong-hoa-mat-troi-2.jpg" width="100%" alt="Thiên Thượng Hỏa Mặt Trời" /><br><br></center>

<p><b>Công việc phù hợp:</b></p>
<li>Những nghề sáng tạo, nhiều ý tưởng.
<li>Không giỏi tính toán.
<li>Làm việc độc lập
</ul>

<p><b>Màu sắc tốt:</b></p>
<ul>
<li>Màu thuộc Thổ: xám, nâu, vàng đất.
<li>Màu thuộc Mộc: xanh lá cây.
</ul>

<p><b>Màu sắc xấu:</b></p>
<ul>
<li>Màu thuộc Thủy: xanh dương.
</ul>
',
            'date_post'         => '2023-12-02',
            'thumbnail_post'    => 'thien-thuong-hoa-mat-troi-thumbnail.jpg',
            'id_cat_post'       => TUVI_POST_ID_CAT,
            'views'             => random_int(50,500),
            'enable_post'       => UNENABLE,
        ]);

        posts::create([
            'name_vi_post'    => 'Tích Lịch Hỏa - Lửa sấm sét',
            'url_post'        => 'tich-lich-hoa-lua-sam-set',
            'present_vi_post' => 'Sấm sét có ý nghĩa gì?',
            'content_vi_post' => '<p>Năm sinh: </p>
<p>Ngũ Hành: Hỏa (kết hợp với )</p>

<p>Tích Lịch Hỏa là lửa từ điện, lửa từ sấm sét.</p>

<center><img src="../upload/img/post/thor-1.jpg" width="100%" alt="Tích Lịch Hỏa Lửa sấm sét" /><br><br></center>

<p>Đặc biệt, <a href="/post/thien-thuong-hoa-mat-troi" title="Thiên Thượng Hỏa lửa mặt trời" target="_blank">Thiên Thượng Hỏa</a> và Tích Lịch Hỏa là 2 mệnh Hỏa không bị Thủy dập tắt. Lửa Mặt Trời không dập được. Tích Lịch Hỏa là lửa sấm sét, lửa từ điện, đổ nước vào là hỏng.
Vì vậy, Tích Lịch Hỏa đặc biệt là gặp Thủy, không những không bị dập tắt mà còn phát mạnh hơn.</p>

<center><img src="../upload/img/post/thien-thuong-hoa-mat-troi-1.jpg" width="100%" alt="Thiên Thượng Hỏa Mặt Trời" /><br><br></center>

<p>Ngược lại, sấm sét không ảnh hưởng đến đất. Điện cũng không thể truyền xuống đất. Điều này trái với Ngũ Hành là Hỏa sinh Thổ. Vậy nên trường hợp này, Hỏa không tác động được vào Thổ.</p>

<p>Mệnh Tích Lịch Hỏa có thể được mệnh <a href="/post/lo-bang-tho-dat-ven-duong" title="Lộ Bàng Thổ đất ven đường" target="_blank">Lộ Bàng Thổ</a> dung hòa, thậm chí là không thể tác động đến Lộ Bàng Thổ. Lộ Bàng Thổ nghĩa là phần đất bằng phẳng ven đường. Sấm sét không thể đánh vào những khu vực bằng phẳng.</p>

<p><b>Tính cách:</b></p>
<ul>
<li>
</ul>

<p><b>Hợp với các mệnh:</b></p>
<ul>
<li>
<li>
<li>
<li>
<li>
</ul>

<center><img src="../upload/img/post/zeus-1.jpg" width="100%" alt="Tích Lịch Hỏa Lửa sấm sét" /><br><br></center>

<p><b>Công việc phù hợp:</b></p>
<li>Những nghề sáng tạo, nhiều ý tưởng.
<li>Không giỏi tính toán.
<li>Làm việc độc lập
</ul>

<p><b>Màu sắc tốt:</b></p>
<ul>
<li>Màu thuộc Thổ: xám, nâu, vàng đất.
<li>Màu thuộc Mộc: xanh lá cây.
</ul>

<p><b>Màu sắc xấu:</b></p>
<ul>
<li>Màu thuộc Thủy: xanh dương.
</ul>
',
            'date_post'         => '2023-12-02',
            'thumbnail_post'    => 'thien-thuong-hoa-mat-troi-thumbnail.jpg',
            'id_cat_post'       => TUVI_POST_ID_CAT,
            'views'             => random_int(50,500),
            'enable_post'       => UNENABLE,
        ]);

        // --------------------------- Thổ ---------------------------



        // gian-ha-thuy-mach-nuoc-ngam
        // bich-thuong-tho-doi-quan-dat-nung
        // lu-trung-hoa-lua-trong-lo-nung
        // bach-lap-kim-kim-loai-nong-chay

        // Đại Khê Thủy dai-khe-thuy-thac-nuoc (1974 - 1975)
        // Đại Hải Thủy
        // Phú Đăng Hỏa phu-dang-hoa-lua-den-dau
        // Sơn Hạ Hỏa
        // Lộ Bàng Thổ lo-bang-tho-dat-ven-duong
        // Kim Bạch Kim
        // Bình Địa Mộc
    }
}
