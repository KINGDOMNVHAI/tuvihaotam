<?php
namespace App\Seeder;

use App\Models\posts;
use DB;
use Illuminate\Support\ServiceProvider;

class FamousSeeder
{
    public function index()
    {
        posts::create([
            'name_vi_post'    => 'Lá số nghệ sĩ Giang Còi',
            'url_post'        => 'la-so-nghe-si-giang-coi',
            'present_vi_post' => 'Sinh ngày 31/10/1962, mất ngày 04/08/2021, hưởng thọ 59 tuổi',
            'content_vi_post' => '<p>Cố nghệ sĩ Lê Hồng Giang được công chúng biết tới với nghệ danh Giang Còi, sinh ngày 31/10/1962, mất ngày 04/08/2021, hưởng thọ 59 tuổi. Lá số giờ Mão.</p>

<h3>Tiểu sử</h3>

<p>Cố Nghệ sĩ Giang Còi tên thật là Lê Hồng Giang, sinh năm 1962 ở Hà Nội.</p>

<p>Ông tốt nghiệp Trường đại học Sân khấu - Điện ảnh Hà Nội, cùng lớp với nghệ sĩ Chiều Xuân, đạo diễn Bùi Thạc Chuyên.
Nam nghệ sĩ được khán giả yêu mến qua các vai hài trong chương trình Gặp nhau cuối tuần, các vai diễn nông dân trên màn ảnh nhỏ,
đặc biệt là khi kết hợp cùng các nghệ sĩ Quang Tèo, Văn Hiệp.</p>

<p>Cố Nghệ sĩ Giang Còi trải qua hai cuộc hôn nhân đổ vỡ. Ông sống một mình nuôi con, không đi thêm bước nữa.</p>

<p>Đầu năm 2021, cố nghệ sĩ Giang Còi phát hiện bị bệnh ung thư vùng hạ họng giai đoạn 3. Theo nhiều bạn bè đồng nghiệp và người thân trong gia đình,
dù bị bệnh nặng nhưng ông vẫn sống lạc quan. Ông mất ngày 4/8/2021.
Sáng 5/8, tang lễ của ông được tổ chức gọn nhẹ. Gia đình đưa thi hài cố nghệ sĩ tới Đài hóa thân hoàn vũ và gửi hài cốt nương nhờ cửa Phật.</p>

<p>Cố Nghệ sĩ Giang Còi được biết đến nhiều trong các chương trình của VTV, đăc biệt là chương trình Gặp Nhau Cuối Tuần bắt đầu phát sóng năm 2000. Đến năm 2007, chương trình Gặp Nhau Cuối Tuần kết thúc.</p>

<center><img src="../upload/img/post/nghe-si-giang-coi.jpg" width="100%" alt="nghệ sĩ Giang Còi" /><br><br>

<p>Cố nghệ sĩ Giang Còi</p>

<img src="../upload/img/post/la-so-nghe-si-giang-coi-31-10-1962-Mao.jpg" width="100%" alt="nghệ sĩ Giang Còi" /><br><br>

<p>Lá số của cố nghệ sĩ Giang Còi</p>

<iframe width="100%" height="350" src="https://www.youtube.com/embed/07Icm_IlCzk" title="Gặp Nhau Cuối Tuần" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<p>Chương trình Gặp Nhau Cuối Tuần</p></center>

<p>Lá số mệnh Thiên Đồng Thiên Lương tại cung Thân (Mậu Thân)</p>

<h3>Mệnh Tài Quan</h3>

<p>Lá số này, bên ngoài thì vui tươi, có danh phận nhưng bên trong đầy phiền muộn, lo toan.</p>

<p>Mã ngộ Trường Sinh, thanh vân đắc lộ: mọi việc hanh thông, thuận lợi, làm việc gì cũng được may mắn và có quý nhân hỗ trợ. Lại thêm Thiên Đồng, thân cư Thiên Di, chắc chắn cuộc đời đi xa quê lập nghiệp. Tiếng tăm rồi sẽ lẫy lừng nhờ cung Nô Bộc tốt, có Giải Thần Phượng Các.</p>

<p>Tuy nhiên, có tiếng nhưng miếng thì không, do gặp Không Kiếp, Song Hao, Khốc Hư. Cuộc đời bên ngoài thì vui vẻ, nổi danh, được nhiều người yêu mến. Nhưng bên trong thì nhiều phiền muộn về tiền bạc, những tâm tư khó nói.</p>

<h3>Phụ Mẫu, Phúc Đức, Huynh Đệ</h3>

<p>Để tránh làm phật ý người nhà, người thân cố nghệ sị, Tử Vi Anh Liễu xin tránh việc đề cập đến người thân.</p>

<h3>Thiên Di, Nô Bộc, Điền Trạch</h3>

<p>Điền Trạch Vô Chính Diệu, dễ thay đổi nơi ở, nơi làm việc. Cố nghệ sĩ luôn phải thay đổi nơi diễn, quay phim.</p>

<p>Thiên Di Vô Chính Diệu, đi lại nhiều nhưng nơi đến lại phụ thuộc vào người khác. Địa điểm quay phim, diễn ở đâu đều do người khác quyết định.</p>

<p>Nô Bộc Tử Vi Phá Quân, bạn bè nhiều người làm nghệ sĩ, luôn tìm cách phá cách, đổi mới, sáng tạo. Ông cũng quen nhiều người có địa vị xã hội nhờ Tử Vi, Hóa Quyền.
Tả Phù Hữu Bật cung Nô, nhiều người giúp đỡ ông trên con đường sự nghiệp lẫn hoạn nạn.</p>

<h3>Phu Thê, Tử Tức</h3>

<p>Cung Phu Thê gặp nhiều sát tinh như Thiên Hình, Quan Phù, Quan Phủ, Địa Kiếp, báo hiệu đường tình duyên đầy trắc trở.</p>

<p>Tử Tức nhị hợp với Mệnh, con giống ba, sống cùng với ba. Cô Thần chiếu vào Mệnh, đương số càng dễ sống trong cảnh cô độc.</p>

<h3>Tật Ách</h3>

<p>Tật Ách có Thiên Phủ, chủ về bệnh dạ dày, cước khí (tê phù chân), phù thũng, tê liệt do thấp khí, cổ trướng. Thiên Phủ là tài tinh, nếu bệnh sẽ có tiền cứu chữa.</p>

<p>Năm 2021, Lưu Tang Môn vào cung Tật Ách, sức khỏe đi xuống, lo lắng vì bệnh tật. Ông mất ngày 4/8/2021 Dương lịch, tức ngày 26/6/2021 Âm lịch. Tháng 6 Âm rơi vào cung Thiên Di có Địa Kiếp. Tháng 7 Âm rơi vào cung Tật Ách.</p>

<h3>Tổng kết</h3>

<p>Trong 5 điều: sức khỏe, tiền bạc, đất đai, mối quan hệ trong gia đình và mối quan hệ ngoài xã hội. Lá số này có hưởng và nghiệp như sau:</p>

<ul>
<li>Phúc được hưởng nhất: mối quan hệ ngoài xã hội, luôn có bạn bè giúp đỡ, sự nghiệp phát đạt.
<li>Phúc được hưởng nhì: đất đai
<li>Bình thường: tiền tài, không giàu cũng không nghèo, bệnh có tiền chữa, có bạn bè giúp đỡ.
<li>Nghiệp nhì: mối quan hệ trong gia đình, 2 lần ly hôn, một mình nuôi con. Vì đi lại nhiều nên cũng không được ở gần con.
<li>Nghiệp nhất: sức khỏe, đi lại nhiều dẫn đến sức khỏe đi xuống. Cố nghệ sĩ hưởng thọ 59 tuổi, không thấp cũng không cao.
</ul>

<p>Cung Nô Bộc là cung tốt nhất, được rất nhiều bạn bè ngoài xã hội giúp đỡ. Sự nghiệp hanh thông, đất đai, tiền tài đều đầy đủ, danh tiếng lẫy lừng.</p>

<p>Nhưng vì đi quá nhiều nên mối quan hệ trong gia đình khó có thể chu toàn, ảnh hưởng đến mối quan hệ trong gia đình. Và tất nhiên sức khỏe sẽ giảm sút nhanh.</p>
',
            'date_post'         => '2023-01-02',
            'thumbnail_post'    => 'la-so-nghe-si-giang-coi-thumbnail.jpg',
            'id_cat_post'       => FAMOUS_PEOPLE_ID_CAT,
            'views'             => random_int(50,500),
            'enable_post'       => ENABLE,
        ]);

        posts::create([
            'name_vi_post'    => 'Lá số Ngô Bảo Châu',
            'url_post'        => 'la-so-ngo-bao-chau',
            'present_vi_post' => 'Sinh ngày 28/6/1972 Dương lịch',
            'content_vi_post' => '<p>Giáo sư Ngô Bảo Châu được công chúng biết đến nhờ đạt Huy chương Fields năm 2010. Ông được đưa sang Pháp làm việc.
Tuy nhiên, ông cũng có những ý kiến gây tranh cãi trên mạng xã hội nên nhiều lần ông bị Việt Nam phản đối, chỉ trích.</p>

<h3>Tiểu sử</h3>

<p>Nguồn: <a href="https://vi.wikipedia.org/wiki/Ng%C3%B4_B%E1%BA%A3o_Ch%C3%A2u" title="Ngô Bảo Châu" rel="nofollow">Wikipedia</a></p>

<p>Ngô Bảo Châu sinh ngày 28/6/1972 Dương lịch tại Hà Nội, giáo sư tại Khoa Toán, Đại học Chicago.</p>

<p>Ngô Bảo Châu sinh ra trong một gia đình trí thức truyền thống. Ông là con trai của Tiến sĩ khoa học ngành cơ học chất lỏng Ngô Huy Cẩn, hiện đang làm việc tại Viện Cơ học Việt Nam.
Mẹ của ông là Phó Giáo sư, Tiến sĩ dược Trần Lưu Vân Hiền, công tác tại Bệnh viện Y học cổ truyền Trung ương, Việt Nam.
Ông là cháu họ của Ngô Thúc Lanh, một Giáo sư toán viết cuốn sách Đại số đầu tiên.</p>

<center><img src="../upload/img/post/ngo-bao-chau.jpg" width="100%" alt="Ngô Bảo Châu" /><br><br>

<p>Giáo sư Ngô Bảo Châu</p>

<img src="../upload/img/post/la-so-ngo-bao-chau-28-6-1972-Mui.jpg" width="100%" alt="Ngô Bảo Châu" /><br><br>

<p>Lá số của Ngô Bảo Châu</p></center>

<p></p>

<h3>Mệnh Tài Quan</h3>

<p></p>

<p></p>

<p></p>

<h3>Phụ Mẫu, Phúc Đức, Huynh Đệ</h3>

<p></p>

<h3>Thiên Di, Nô Bộc, Điền Trạch</h3>

<p></p>

<p></p>

<p></p>

<h3>Phu Thê, Tử Tức</h3>

<p></p>

<p></p>

<p></p>

<p></p>

<h3>Tật Ách</h3>

<p></p>

<p></p>

<p></p>

<h3>Tổng kết</h3>

<p>Trong 5 điều: sức khỏe, tiền bạc, đất đai, mối quan hệ trong gia đình và mối quan hệ ngoài xã hội. Lá số này có hưởng và nghiệp như sau:</p>

<ul>
<li>Phúc được hưởng nhất:
<li>Phúc được hưởng nhì:
<li>Bình thường:
<li>Nghiệp nhì:
<li>Nghiệp nhất: mối quan hệ ngoài xã hội. Chính vì những ý kiến gây tranh cãi trên mạng xã hội nên nhiều lần ông bị phản đối, chỉ trích.
</ul>
',
            'date_post'         => '2023-01-03',
            'thumbnail_post'    => 'la-so-ngo-bao-chau-thumbnail.jpg',
            'id_cat_post'       => FAMOUS_PEOPLE_ID_CAT,
            'views'             => random_int(50,500),
            'enable_post'       => ENABLE,
        ]);

        posts::create([
            'name_vi_post'    => 'Lá số Alina Li',
            'url_post'        => 'la-so-alina-li',
            'present_vi_post' => 'Sinh ngày 8-9-1993 Dương lịch',
            'content_vi_post' => '<p>Alina Li sinh ngày 8/9/1993 Dương lịch. Lá số giờ Sửu.</p>

<h3>Tiểu sử</h3>

<p>Nguồn: <a href="https://www.famedwiki.com/alina-li/" title="Tam Ám Diêu Đà Kỵ" rel="nofollow">Famedwiki</a></p>

<p>Alina Li sinh ngày 8/9/1993 Dương lịch tại Thượng Hải. Ba mẹ cô ly hôn khi cô mới 3 tuổi.
Sau khi ba mẹ ly hôn, Alina chuyển đến định cư ở nhiều địa điểm khác nhau ở Trung Quốc trước khi định cư ở Burlington, Vermont.</p>

<p>Do phải di chuyển liên tục, mẹ của Alina đã gửi cô đến trường nội trú trong 4 năm. Cô ấy đã tiết lộ rằng cô ấy cảm thấy như địa ngục khi ở trường nội trú.</p>

<p>Alina bắt đầu học trung học ở Vermont tuy nhiên cuộc sống của cô đã thay đổi sau khi tốt nghiệp. Cô gia nhập thế giới phim người lớn vào năm 2013, sau 2 tháng tốt nghiệp trung học.</p>

<p>Năm 2014, cô được gặp thần tượng của mình là Asa Akira. Cô cũng đã nghỉ đóng phim người lớn vào năm 2014. Sự nghiệp của cô chỉ kéo dài 1 năm.</p>

<center><img src="../upload/img/post/alina-li-1.jpg" width="100%" alt="Alina Li" /><br><br>

<p>Alina Li</p>

<img src="../upload/img/post/la-so-alina-li-8-9-1993-Suu.jpg" width="100%" alt="Alina Li" /><br><br>

<p>Lá số của Alina Li</p></center>

<h3>Lý do chọn lá số</h3>

<p>Nhìn vào lá số, có thể dễ dàng nhận ra <a href="/post/cac-sao-am-tinh" title="Tam Ám Diêu Đà Kỵ">bộ Tam Ám Diêu Đà Kỵ</a> tại Mệnh. Đây là bộ sao đa tình, hoang dâm.
Cộng với tính cách mạnh mẽ, phá cách, đổi mới của Phá Quân, luôn muốn làm theo ý mình, làm điều gì đó mới mẻ, lá số này sẵn sàng vượt qua dư luận để làm theo ý mình.</p>

<p>Ba mẹ cô ly hôn khi cô mới 3 tuổi. Phụ Mẫu vô chính diệu không được ở gần ba mẹ, không được nhờ cậy ba mẹ.</p>

<p>2013, cô bắt đầu sự nghiệp. Lưu Thái Tuê vào cung Phu Thê, có sự kiện lớn trong chuyện tình cảm, sinh lý. Lưu Tang Môn vào Bản Mệnh có Thiên Diêu.</p>

<p>2014, cô được gặp thần tượng. Lưu Thái Tuê vào cung Huynh Đệ. Lưu Tang Môn vào Phụ Mẫu. Asa Akira có thể xem như vừa là chị em, vừa là tiền bối đi trước.</p>

<h3>Mệnh Tài Quan</h3>

<p>Mệnh có Tử Vi Phá Quân: Phá Quân tính cách thích đổi mới, vượt khuôn khổ thông thường. Tử Vi đế tinh, cái tôi cá nhân cao, mạnh mẽ.</p>

<p></p>

<p></p>

<h3>Phụ Mẫu, Phúc Đức, Huynh Đệ</h3>

<p>Phụ Mẫu vô chính diệu không được ở gần ba mẹ, không được nhờ cậy ba mẹ. Ba mẹ cô ly hôn khi cô mới 3 tuổi.</p>

<p>Thân Cư Phúc Đức, </p>

<p>Huynh Đệ Thiên Cơ, anh chị em, bạn thân đều là những người thông minh.</p>

<h3>Thiên Di, Nô Bộc, Điền Trạch</h3>

<p></p>

<p></p>

<p>Nô Bộc Cự Môn, Địa Kiếp, những sao sinh ra mâu thuẫn, nói xấu, hợp với nghề đầy thị phi của cô. Nô Bộc Cự Môn cũng có ý nghĩa là tài ăn nói, ngoại ngữ tốt.
Cô sống ở nước ngoài, bạn bè, diễn viên đều giỏi ngoại ngữ, nói chuyện có duyên.</p>

<h3>Phu Thê, Tử Tức</h3>

<p>Phu Thê Vô Chính Diệu, chậm chồng, không nhờ cậy được chồng. Quan Lộc có Đà La, Hóa Kỵ, Cô Thần chiếu qua cung Phu Thê, càng khiến đường tình duyên khó khăn hơn.</p>

<p>Ân Quang: chồng là người giúp đỡ mình mà tạo nên tình duyên.</p>

<p></p>

<h3>Tật Ách</h3>

<p>Thiên Đồng Thiên Lương: bộ sao cứu giải nên hóa giải bệnh tật, sức khỏe tốt.</p>

<p>Thiên Lương ở hai cung Dần hoặc Thân, chủ về đau bụng kinh. Thiên Đồng chủ về các bệnh ở thận, bàng quang, niệu đạo, ống dẫn tinh hoặc buồng trứng.</p>

<p>Kiếp Sát: dao kéo, mổ xẻ, phẫu thuật thẩm mỹ.</p>

<p>Lưu Hà: tai nạn sống nước, máu loãng. Gặp nữ giới thì nguy hiểm khi sinh con.</p>

<center><img src="../upload/img/post/alina-li-2.jpg" width="100%" alt="Alina Li" /><br><br></center>

<h3>Tổng kết</h3>

<p>Trong 5 điều: sức khỏe, tiền bạc, đất đai, mối quan hệ trong gia đình và mối quan hệ ngoài xã hội. Lá số này có hưởng và nghiệp như sau:</p>

<ul>
<li>Phúc được hưởng nhất: mối quan hệ ngoài xã hội. Được nhiều người ủng hộ trên con đường làm công việc đầy thị phi.
<li>Phúc được hưởng nhì: sức khỏe. Phải có sức khỏe tốt mới làm được công việc cần thể chất dồi dào như vậy.
<li>Trung bình: đất đai. Dù phải di chuyển chỗ ở, nhưng nơi làm việc cũng khá đẹp, trong các khách sạn xa hoa, chứng tỏ Điền Trạch tốt.
<li>Nghiệp nặng nhì: tiền tài. Không sợ nghèo khó, nhưng khó giàu.
<li>Nghiệp nặng nhất: mối quan hệ trong gia đình. Ba mẹ ly dị. Tình trường nhiều nhưng sẽ bị tai tiếng, khó đi đến hôn nhân. Nếu có hôn nhân thì khó có con, nguy hiểm khi sinh.
</ul>
',
            'date_post'         => '2023-01-04',
            'thumbnail_post'    => 'la-so-alina-li-thumbnail.jpg',
            'id_cat_post'       => FAMOUS_PEOPLE_ID_CAT,
            'views'             => random_int(50,500),
            'enable_post'       => ENABLE,
        ]);

        posts::create([
            'name_vi_post'    => 'Lá số nghệ sĩ Chí Tài',
            'url_post'        => 'la-so-nghe-si-chi-tai',
            'present_vi_post' => 'Sinh ngày 15/08/1958, mất ngày 09/12/2020, hưởng thọ 62 tuổi',
            'content_vi_post' => '<p>Cố nghệ sĩ Chí Tài, tên thật là Nguyễn Chí Tài được công chúng biết tới với nghệ danh Giang Còi, sinh ngày 15/08/1958, mất ngày 09/12/2020, hưởng thọ 62 tuổi. Lá số giờ Sửu.</p>

<h3>Tiểu sử</h3>




<center><img src="../upload/img/post/nghe-si-chi-tai-1.jpg" width="100%" alt="cố nghệ sĩ Chí Tài" /><br><br>

<p>Cố nghệ sĩ Chí Tài</p>

<img src="../upload/img/post/la-so-nghe-si-chi-tai-15-08-1958-Suu.jpg" width="100%" alt="cố nghệ sĩ Chí Tài" /><br><br>

<p>Lá số của cố nghệ sĩ Chí Tài</p></center>


<h3>Lý do chọn lá số</h3>



<h3>Tật Ách</h3>

<p>Năm 2020, Lưu Tang Môn và Lưu Thiên Mã vào cung Tật Ách. Tang Mã là...</p>

<h3>Tổng kết</h3>

<p>Trong 5 điều: sức khỏe, tiền bạc, đất đai, mối quan hệ trong gia đình và mối quan hệ ngoài xã hội. Lá số này có hưởng và nghiệp như sau:</p>

<ul>
<li>Phúc được hưởng nhất:
<li>Phúc được hưởng nhì:
<li>Trung bình:
<li>Nghiệp nặng nhì:
<li>Nghiệp nặng nhất:
</ul>
',
            'date_post'         => '2023-01-05',
            'thumbnail_post'    => 'la-so-nghe-si-chi-tai-thumbnail.jpg',
            'id_cat_post'       => FAMOUS_PEOPLE_ID_CAT,
            'views'             => random_int(50,500),
            'enable_post'       => ENABLE,
        ]);

        posts::create([
            'name_vi_post'    => 'Lá số nghệ sĩ Nemoto Nagi',
            'url_post'        => 'la-so-nemoto-nagi',
            'present_vi_post' => 'Sinh ngày 15/03/1999',
            'content_vi_post' => '<p>Nemoto Nagi, sinh ngày 15/03/1999. Lá số giờ Thìn.</p>

<h3>Tiểu sử</h3>

<p>Nemoto Nagi (根本凪)</p>



<p>11/01/2022, cô đã tốt nghiệp nhóm Niji no Conquistador và hiện tại đang làm VTuber, live stream với hình ảnh đại diện là một nhân vật phong cách anime.</p>

<center><img src="../upload/img/post/nemoto-nagi-1.jpg" width="100%" alt="Nemoto Nagi" /><br><br>

<blockquote class="twitter-tweet"><p lang="ja" dir="ltr">💫本日21時から配信です💫<br><br>【実写動画あり】オフラインイベントの様子ってどんな感じ？【根本凪】<a href="https://twitter.com/hashtag/vtuber?src=hash&amp;ref_src=twsrc%5Etfw">#vtuber</a> <br><br>☕️待機場所→<a href="https://t.co/XamNP53l91">https://t.co/XamNP53l91</a> <a href="https://twitter.com/YouTube?ref_src=twsrc%5Etfw">@YouTube</a>より <a href="https://t.co/qVKhcUkcZe">pic.twitter.com/qVKhcUkcZe</a></p>&mdash; 根本凪☕️💫 (@nemoto_nagi) <a href="https://twitter.com/nemoto_nagi/status/1625754032622141440?ref_src=twsrc%5Etfw">February 15, 2023</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

<p>Nemoto Nagi</p>

<img src="../upload/img/post/la-so-nemoto-nagi-15-03-1999-Thin.jpg" width="100%" alt="Nemoto Nagi" /><br><br>

<p>Lá số của Nemoto Nagi</p></center>

<h3>Lý do chọn lá số</h3>

<p>Mệnh Tham Lang vượng địa, chắc chắn đây là một người con gái cực kỳ xinh đẹp. Nemoto Nagi là nghệ sĩ nữ được yêu thích nhất nhóm nhạc Niji no Conquistador với số đo 3 vòng 90-57-85</p>

<p>Cung Tài Bạch có Văn Xương, cô kiếm tiền dựa vào nghệ thuật. Mệnh có Hóa Quyền, Tài có Phá Quân, cô là người sáng lập ra nhóm Niji no Conquistador.</p>




<h3>Tật Ách</h3>

<p></p>

<h3>Tổng kết</h3>

<p>Trong 5 điều: sức khỏe, tiền bạc, đất đai, mối quan hệ trong gia đình và mối quan hệ ngoài xã hội. Lá số này có hưởng và nghiệp như sau:</p>

<ul>
<li>Phúc được hưởng nhất:
<li>Phúc được hưởng nhì:
<li>Trung bình:
<li>Nghiệp nặng nhì:
<li>Nghiệp nặng nhất:
</ul>
',
            'date_post'         => '2023-01-06',
            'thumbnail_post'    => 'la-so-nemoto-nagi-thumbnail.jpg',
            'id_cat_post'       => FAMOUS_PEOPLE_ID_CAT,
            'views'             => random_int(50,500),
            'enable_post'       => ENABLE,
        ]);





        posts::create([
            'name_vi_post'    => 'Lá số Bill Gates',
            'url_post'        => 'la-so-bill-gates',
            'present_vi_post' => 'Sinh ngày dương lịch 28/10/1955',
            'content_vi_post' => '<p></p>

<h3>Tiểu sử</h3>

<p></p>

<p></p>

<p>Nhưng đến ngày 3/5/2021, tỷ phú Bill Gates bất ngờ thông báo trên Twitter rằng sẽ ly hôn sau 27 năm chung sống với vợ.
Ông cũng dính vào bê bối tình ái với nhân viên từ, khiến hình tượng của ông sụp đổ trong lòng nhiều người.</p>

<center><img src="../upload/img/post/nghe-si-giang-coi.jpg" width="100%" alt="Bill Gates" /><br><br>

<p>Bill Gates</p>

<img src="../upload/img/post/la-so-nghe-si-giang-coi-31-10-1962-Mao.jpg" width="100%" alt="Bill Gates" /><br><br>

<p>Lá số của Bill Gates</p></center>

<p>Lá số mệnh Thiên Đồng Thiên Lương tại cung Thân (Mậu Thân)</p>

<h3>Mệnh Tài Quan</h3>

<p></p>

<p></p>

<p></p>

<h3>Phụ Mẫu, Phúc Đức, Huynh Đệ</h3>

<p></p>

<h3>Thiên Di, Nô Bộc, Điền Trạch</h3>

<p></p>

<p></p>

<p></p>

<h3>Phu Thê, Tử Tức</h3>

<p></p>

<h3>Tật Ách</h3>

<p></p>

<p></p>

<p></p>

<h3>Tổng kết</h3>

<p>Trong 5 điều: sức khỏe, tiền bạc, đất đai, mối quan hệ trong gia đình và mối quan hệ ngoài xã hội. Lá số này có hưởng và nghiệp như sau:</p>

<ul>
</ul>

',
            'date_post'         => '2023-01-02',
            'thumbnail_post'    => 'la-so-bill-gates-thumbnail.jpg',
            'id_cat_post'       => FAMOUS_PEOPLE_ID_CAT,
            'views'             => random_int(50,500),
            'enable_post'       => UNENABLE,
        ]);

        posts::create([
            'name_vi_post'    => 'Lá số Joanne Rowling',
            'url_post'        => 'la-so-joanne-rowling',
            'present_vi_post' => 'Sinh ngày 31/7/1965 Dương lịch',
            'content_vi_post' => '<p></p>

<h3>Tiểu sử</h3>

<p>Nguồn: <a href="https://cafef.vn/cuoc-doi-that-bai-the-tham-cua-tac-gia-harry-potter-va-bai-hoc-quy-cho-ca-the-gioi-20201222154548061.chn" title="Rowling Harry Potter" rel="nofollow">Cafef</a></p>

<p>Joanne Rowling sinh ngày 31/7/1965 Dương lịch tại Yate, Vương Quốc Anh. Mẹ bà là Anne, một kỹ thuật gia khoa học, còn cha bà là Peter James Rowling, một kỹ sư máy bay của Rolls-Royce.
Ông nội của mẹ bà, Louis Volant, là người Pháp, được trao tặng giải thưởng Croix de Guerre vì lòng can đảm cho việc bảo vệ ngôi làng Courcelles-le-Comte trong Thế chiến thứ nhất.</p>

<p>Khả năng văn chương của Rowling đã bộc lộ từ nhỏ. Thầy cô thường hay phàn nàn về khả năng tập trung của Rowling. Bà thường mất tập trung, không chịu nghe giảng,
chỉ nghĩ và sáng tác những mẩu truyện ngắn về một thế giới không có thật. Bà miêu tả: "Thời niên thiếu của tôi không lấy gì làm vui vẻ. Mẹ tôi bệnh nặng. Tôi bất hòa đến độ không thể nói chuyện với bố".</p>

<p>Năm 1982, bà tốt nghiệp trung học và mạnh dạn nộp đơn và Đại học Oxford với hi vọng sẽ được thỏa sức theo đuổi đam mê sáng tác văn học nhưng lại bị từ chối.
Sau khi tốt nghiệp Đại học Exeter, ý tưởng về Harry Potter đến với Rowling một cách khá bất ngờ khi bà đang ngồi chờ tàu để đi từ Manchester đến London vào năm 1990.</p>

<p>Tháng 12/1990, mẹ của Rowling - bà Anne Rowling qua đời sau 10 năm bệnh tật.
Bà sống trong cảnh túng quẫn, cần trợ cấp xã hội cho đến khi tiểu thuyết đầu tay là Harry Potter và Hòn đá Phù thủy ra mắt vào năm 1997.
Kể từ đó, cuộc đời bà chuyển thành cuộc sống giàu sang. Bà từng lọt vào top 10 người giàu nhất thế giới, sánh ngang với Bill Gates đứng top 1 thời điểm đó.</p>

<p>Hiện tại, bà là người cấp tiến, ủng hộ phong trào nữ quyền mạnh mẽ. Bà muốn đấu tranh giành quyền phụ nữ, nhưng không chấp nhận những người chuyển giới thành nữ là phụ nữ.
Bà bị cộng đồng LGBT, bao gồm cả Daniel Radcliffe, diễn viên đóng vai Harry Potter phản đối.</p>

<center><img src="../upload/img/post/joanne-rowling-1.jpg" width="100%" alt="Joanne Rowling J.K Rowling" /><br><br>

<p>J.K Rowling, tác giả của Harry Potter</p>

<img src="../upload/img/post/la-so-joanne-rowling.jpg" width="100%" alt="Joanne Rowling J.K Rowling" /><br><br>

<p>Lá số của J.K Rowling</p></center>

<h3>Lý do chọn lá số</h3>

<p>J.K Rowling là nhà văn. Nhắc tới văn học, chắc chắn phải nhắc tới Thái Âm, ngôi sao với tài văn chương thiên phú.</p>

<p>Tháng 12/1990, mẹ của Rowling - bà Anne Rowling qua đời sau 10 năm bệnh tật. Lúc đó bà 25 tuổi, trong Đại Vận cung .......</p>

<h3>Mệnh Tài Quan</h3>

<p></p>

<p></p>

<p></p>

<h3>Phụ Mẫu, Phúc Đức, Huynh Đệ</h3>

<p></p>

<h3>Thiên Di, Nô Bộc, Điền Trạch</h3>

<p></p>

<p></p>

<p></p>

<h3>Phu Thê, Tử Tức</h3>

<p></p>

<h3>Tật Ách</h3>

<p></p>

<p></p>

<p></p>

<h3>Tổng kết</h3>

<p>Trong 5 điều: sức khỏe, tiền bạc, đất đai, mối quan hệ trong gia đình và mối quan hệ ngoài xã hội. Lá số này có hưởng và nghiệp như sau:</p>

<ul>
</ul>

',
            'date_post'         => '2023-01-02',
            'thumbnail_post'    => 'la-so-joanne-rowling-thumbnail.jpg',
            'id_cat_post'       => FAMOUS_PEOPLE_ID_CAT,
            'views'             => random_int(50,500),
            'enable_post'       => UNENABLE,
        ]);
    }
}



// Kimmie 14-6-1995 https://nguoinoitieng.tv/nghe-nghiep/youtuber/kimmie/bj41


// Annie Lee annie-lee-1 giờ Sửu
// https://pornstargold.com/best-thai-pornstars/

// Trương Lệ https://globalzonetoday.com/evelyn-lin/


// https://soyouthinkyoucandance.vn/bronwin-aurora-la-ai/#ftoc-heading-2
// Facebook: https://www.facebook.com/groups/735666618143633/user/100089469793725/


// Tôi gặp chú lần đầu và cũng là lần duy nhất. Một buổi chiều tà tháng 4 khi tôi đón con bé lớn đi học. Bé nhà tôi năm đó lớp 1. Chắc khoảng 2014. Tầm đó. Quán nước chè cạnh cổng trường tiểu học Nguyễn Trãi phố Khương Trung. Cũng vô tình. Chú là diễn viên. Tuổi thơ ai chả biết chú. Nhà tôi ngõ 211 Khương Trung rẽ vào. Chén trà đặc,nhâm nhi điếu thuốc. Lúc ấy quanh khu nhà tôi cũng nhiều người biết tôi xem được lá số Tử Vi. Chú chủ động làm quen. Bộ ria quai nón chẳng lẫn vào đâu được: "Anh xem cho chú cuộc đời thế nào". Tôi giật mình. Thì phải chào chú trước. Hình như con nhà chú cũng học trường Nguyễn Trãi cùng bé nhà tôi. Giọng oang oang,vừa nói vừa cười. "Tôi nghe bu nhà tôi kể tôi đẻ lúc sáng gì đó,tầm 6 giờ. Ngày 31 tháng 10 năm 1962. Sáu Hai Nhâm Dần đấy ". Bấm nhanh lá số. Tôi chẳng nói vội. Hỏi chú quê đâu. Chú nhẹ nhàng: quê tôi có Chợ Sắt . Hoá ra chú cháu cùng quê.
// Lập lá số mệnh Thiên Đồng-Thiên Lương tại cung Thân(Mậu Thân) mệnh tự Hoá Lộc lại có Mã ngộ Tràng Sinh. Đẻ giờ Mão thân cư Thiên Di tại Dần. Thanh Vân Đắc Lộ. Tiếng tăm quả thực có nhiều. Mệnh ôm vòng Tuế Phá nên cả đời lênh đênh phiêu bạt. Tôi đoán chú sau này khi Thác mới về đất tổ quê cha. Mệnh ôm Mã sinh long đong bôn tẩu. Chất nghệ sỹ của Thiên Đồng thì phiêu. Hoá Lộc thực khéo lại tài văn chương. Thê thiếp 1 đời đứt gãy. Do an Thân nằm ở Thái Tuế nên đến 50 tuổi sẽ chọn cuộc sống thanh nhàn đạm bạc. "thái Tuế tại Thân,chung thân cô độc". Qua 60 ắt thọ dài lâu. Mệnh lập tại cung Kim,tuổi Nhâm Dần Kim gặp Địa Không tắc phát như lôi. Tiếc rằng "Kiếp không đóng ở mệnh cung-cho dù đắc địa thọ vòng trung niên". Chú cười bảo: sống ý nghĩa là được chứ sống lâu mà vô nghĩa thì phí cái đời anh nghệ sỹ. Chú lại cười. Đứng dậy trả tiền trà đá và chén nước cho tôi. Hôm nay nghe tin chú về với tổ tiên. Chợt buồn....nhưng biết sao được. Số mệnh đã an bài sắp đặt. Mong chú nơi xa ấy yên nghỉ cõi vĩnh hằng. Luôn yêu mến chú. Nghệ Sỹ Giang Còi
// Tiểu hạn năm 2021 chú 60 tuổi tại Mão. Thiên Không tang hổ mã khốc hư. 2 tầng Hoá Kỵ xung chính Tiểu Hạn.

// Daniel Jacob Radcliffe 23 tháng 7, 1989 (33 tuổi), Queen Charlotte's and Chelsea Hospital, London, Vương Quốc Anh

// Joanne Rowling J.K Rowling 31 tháng 7, 1965 (57 tuổi), Yate, Vương Quốc Anh
// Bà sống trong cảnh túng quẫn cho đến khi tiểu thuyết đầu tay là Harry Potter và Hòn đá Phù thủy ra mắt vào năm 1997.



// Ca sĩ Vy Oanh
// Luận giải lá số người nổi tiếng. Cs Vy O
// Trước tiên xét gia đạo hưng suy. Song kỵ phá phụ mẫu lại kỵ xuất. Cô là người sinh ra trong gđ nghèo, hoàn cảnh gđ bố mẹ ko được thuận lợi (gđ có 5 anh em, sau đó cô nhận người khác làm con nuôi). Niên kỵ tại huynh chuyển kỵ nhập phụ, bản thân ko phải là con cả nhưng đóng vai trò như con cả, về sau lo cho anh em (cô từng tâm sự bản thân vừa làm vừa học, từng kiếm tiền mua xe đạp cho anh trai đi học).
// Thực ra thì xét hạn tiểu nhi còn nhiều cách nữa nhưng chỉ cần xem thế này cũng đã là ứng số.
// Về tính cách. Bản thân là người có duyên nhưng nóng nảy, ưa thể hiện bản thân, tính cách hơi so đo nhiều khi vì lên tiếng sai lầm mà tự chuốc lấy những thị phi ko đáng có. Mệnh phi kỵ nhập di, tật phi quyền nhập phụ, phúc phi kỵ nhập phụ tự xuất...
// Về hôn nhân. Mệnh phu kỵ nhập di, cô lấy ck xa, hôn nhân duyên bạc... Nhưng phu có vũ phũ khoa tuế nên ck cô là doanh nhân thành đạt, có số má trên thương trường...
