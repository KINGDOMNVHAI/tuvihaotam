<?php
namespace App\Seeder;

use App\Models\posts;
use DB;
use Illuminate\Support\ServiceProvider;

class EmotionSeeder
{
    public function index()
    {
        posts::create([
            'name_vi_post'    => 'Phụ nữ và đàn ông cần tài nguyên của nhau',
            'url_post'        => 'phu-nu-va-dan-ong-can-tai-nguyen-cua-nhau',
            'present_vi_post' => 'Tài nguyên đó bao gồm những gì?',
            'content_vi_post' => '<p></p>

<h3>Trong tự nhiên và xã hội phong kiến</h3>

<p><b>Trong tự nhiên</b></p>

<p>Loài nhện hay bọ ngựa, sau khi giao phối, con đực sẽ bị con cái ăn thịt. Lý do là vì con cái cần có chất dinh dưỡng để nuôi con của mình. Trong trường hợp này, tài nguyên chính là cơ thể, chất dinh dưỡng của con đực.</p>

<p><b>Xã hội phong kiến</b></p>

<p>Trong thời phong kiến, đàn ông cung cấp tài nguyên là lao động tay chân, sự bảo vệ, đi lính... Và đàn ông được tạo điều kiện để có thể phát triển tài nguyên của mình như học hành, luyện tập võ nghệ. Từ trí tuệ hay sức mạnh, đàn ông sẽ thu về nhiều của cải, tài nguyên.</p>

<p>Phụ nữ cần tài nguyên của đàn ông để bảo vệ cho những đứa con của mình. Đổi lại, họ sẽ cung cấp các "dịch vụ" như làm việc nhà, vào bếp hay trên giường. Đó là lý do họ sẽ tìm đến những người đàn ông nhiều tài nguyên.</p>

<p>Tuy nhiên, phụ nữ sẽ phải đề phòng những người phụ nữ khác bén mảng đến "kho tài nguyên" của mình, lấy mất người đàn ông cung cấp tài nguyên, lấy tài nguyên hoặc buộc mình phải chia sẻ cả 2 thứ đó. Vì vậy, họ quyết bảo vệ tài nguyên khỏi kẻ xâm phạm kia.</p>

<p></p>

<p></p>

<h3>Phụ nữ muốn tài nguyên gì?</h3>

<p></p>

<p></p>

<p></p>

<center>

<p></p></center>

<h3>Đàn ông muốn tài nguyên gì?</h3>

<p></p>

<p></p>

<p></p>

<p>Gái đẹp và đại gia, cặp đôi hoàn hảo từ ngàn đời. Thuở xa xưa, đàn ông được phép cưới nhiều vợ. Nhưng chỉ có các phú ông, đại gia, vua chúa mới có năm thê bảy thiếp chứ chẳng bao giờ thấy anh nông dân nghèo có thê thiếp.</p>

<p>Nguyên nhân rất đơn giản: đôi bên đều có tài nguyên họ cần. Đàn ông có tiền bạc, nhà cửa, danh vọng và quan trọng nhất là sự bảo vệ. Đàn bà có các "dịch vụ", đúng đổ tuổi sinh nở.</p>

https://www.youtube.com/watch?v=WmfCJI9Om2U
',
            'date_post'         => '2023-01-07',
            'thumbnail_post'    => 'phu-nu-va-dan-ong-can-tai-nguyen-cua-nhau-thumbnail.jpg',
            'id_cat_post'       => TAM_LY_CAT,
            'views'             => random_int(100,1000),
            'enable_post'       => ENABLE,
        ]);
    }
}
