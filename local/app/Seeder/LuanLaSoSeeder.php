<?php
namespace App\Seeder;

use App\Models\posts;
use DB;
use Illuminate\Support\ServiceProvider;

class FamousSeeder
{
    public function index()
    {
        posts::create([
            'name_vi_post'    => 'Lá số làm gái bán hoa',
            'url_post'        => 'la-so-lam-gai-ban-hoa',
            'present_vi_post' => 'Sinh ngày 6/6/1993 giờ Sửu',
            'content_vi_post' => '<p></p>

<h3>Tiểu sử (nguồn Facebook)</h3>

<blockquote>
<p>Lá số từng một thời gian ngắn làm tay vịn quán hát và làm gái bán hoa, cũng từng suýt làm vợ lẽ.</p>

<p>Bổ sung 1 số hạn nổi bật khác:</p>
<ul>
<li>2018 bị lừa online mất tầm 100tr, sau đó báo công an và kẻ lừa đảo đi tù.
<li>Cuối 2017, đầu 2018 cho đến 2020: Yêu xa 1 người sinh năm 1986. Người này có vợ con nhưng nói dối là độc thân. Sau đó LS có định làm đơn kiện nhưng bên đó xin hòa giải, giờ đã hoàn toàn kết thúc.
</ul>

<p>Từ hôm qua đến giờ có rất nhiều người quan tâm bài đăng, em rất cảm ơn ạ. Em có 1 số điều muốn nói như sau:</p>
<ul>
<li>Công việc này LS làm một thời gian ngắn và làm thêm, chứ ko phải công việc toàn thời gian. Thời đấy tâm lý LS sụp đổ và chán chường, bất cần đời nên mới đưa ra quyết định như vậy. Sau đó thì đã bỏ hẳn, học hành và làm ăn chân chính.
<li>Tiền kiếm được thời gian đó hầu hết là để chữa bệnh sùi mào gà (sau nhiễm bệnh thì ko đi làm nữa). Đây là 1 căn bệnh chữa trị đau đớn, tốn kém và nhục nhã. Nhưng cũng may là chưa HIV gì ạ 😞 Mỗi lần đi chữa là 2tr-5tr đi toi. Chưa kể tiền mua thuốc, tiêm vắc xin HPV.
<li>Môi trường làm việc quán hát có kiếm được tiền đi nữa thì rất hại người do tính chất làm việc ban đêm, khói thuốc, rượu bia. Đêm làm thì ngày sẽ lờ đờ như zombie.
<li>Em ko dám khuyên ai cái gì. Nhưng mong rằng mọi người sẽ chú ý đến con gái, chị em gái của mình hơn, ko để ai phải suy sụp cô đơn khi có biến cố gì mà nghĩ quẩn. Nhất là các bé gái sinh trưởng trong gia đình ko hạnh phúc cũng dễ tự ti, suy nghĩ lệch lạc về tình yêu. Em cũng mong ko có thêm người mắc các bệnh xã hội.
<li>Và thêm nữa, học hành và lại động là con đường đi lên chân chính nhất ạ. Quá khứ khép lại, em đã học xong và đi làm với mức thu nhập và vốn sống kha khá.
<li>Ở bài viết này, có người chửi em, có người động viên. Em rất cảm ơn và điều em mong là lá số cũng như câu chuyện của em có ý nghĩa dù ít dù nhiều với mọi người. Em xin cảm ơn!
</ul>
</blockquote>

<blockquote>
<p>Dạ mình làm trước khi đi du học, và thời gian đầu sau du học.</p>
<p>Hồi trước du học thì mình làm gái ở VN... phần nhiều là do tâm lý sa sút do đổ vỡ tình cảm, chán chường, bất cần đời, và muốn kiếm tiền để thoát ly. Vì mình du học thế thôi nhưng du học tự túc và nhà cũng nghèo. Thời gian này ngắn thôi.</p>
<p>Sau du học thì mình làm quán hát ở nước ngoài, lý do muốn kiếm tiền nhanh vì mình bị lừa tiền, 1 số tiền lớn khoảng 100tr (tiền này đi vay). Hồi ấy 100tr lớn với mình lắm vì mình tay trắng. Mặc dù đi làm quán hát nhưng mình vẫn quyết tâm ko đi tăng 2 với khách ạ. Thời gian này cũng ngắn và mình làm cuối tuần thôi.</p>
</blockquote>

<center><img src="../upload/img/post/6-6-1993-suu-nu-2023.jpg" width="100%" alt="Lá số làm gái bán hoa" /><br><br></center>

<h3>Một số bình luận</h3>

<blockquote>
<p>Cảm ơn bạn đã chia sẻ thông tin.</p>
<ul>
<li>Mình mạn phép đoán đương số đã cưới hoặc đã ăn ở với người khác và muốn có con
<li>Chồng của đương số sinh năm Bính Dần 86 hay Kỷ Tỵ 89 phải hông bạn?
<li>Đường con cái có chút vấn đề. Năm 2022 bạn này rất mong con, nhưng thả mã không có
</ul>
</blockquote>

<blockquote>
<p>Cả thân mệnh toàn dâm tinh. Mệnh tham, diêu, kỵ. Thân cư phúc có bộ đào - hồng - thai. Cũng ý nói họ nay sinh nhiều người con gái ong bướm giống như chủ nhân lá số này.
Nhưng có tham hoả số giàu đấy. Đủ binh hinh tướng ấn chắc kiêm thêm chức điều gái làng chơi nữa mà giàu chăng?
Cũng có thể vì xinh đẹp mà buôn bán mỹ phẩm, quần áo nữ giới... mà phát phú</p>
</blockquote>

<blockquote>
<p>Tham lang ở mộ có hóa kỵ riêu y đồng cung. Tài bạch có kiếp hình. Phá quân ko ưa song lộc cho lắm vì nó mang tính gò bó. Sát phá tham cần vài hung sát tinh thêm hóa quyền vào vòng tang tuế điếu của vòng thái tuế là đủ. Lá này mình có vài ý sau đây.</p>
<p>Thứ nhất là nên học những ngành như làm đẹp hoặc làm nghề rồi đi lên. Cung thân này ko đến nỗi tệ nên thành công muộn. Thường thế vũ tham ở mộ có hỏa tinh thường là sau 35 mới bắt đầu vô vận.</p>
<p>Thứ hai dụng tuyến phúc phối di người khác giới vào môi trường tập thể tận dụng khả năng ngoại giao xã giao của mình.</p>
<p>Thứ 3 tháng 1.3.5.7.11. Cẩn trọng tai nạn, tiền bạc và sức khỏe nhá.</p>
</blockquote>

<blockquote>
<p>Tham lang hoá kị</p>
<p>Diêu y ở mệnh nhu cầu rất cao nếu xa ngã thì rất dễ làm nghề đó còn bt ck chiều chắc cũng k làm thoả mãn đc hết.</p>
</blockquote>

<blockquote>
<p>Đào ko kiếp kị về phúc lm nghề là bt nhưng vì cơ kị triệt nên 1 thời gian ngắn mà nghỉ dễ vì lương tâm mà ko tiếp tục</p>
</blockquote>

<blockquote>
<p>Bạn này chak làm gái do hoàn cảnh chứ hok fai bản chất. Chak hận đời hAy dòng đời bi đát quá mới thế chứ mệnh có Long Đức Tướng Quân là ng có Tâm tốt sao làm gái đc.</p>
</blockquote>

<blockquote>
<p>Ls nhìn tham đắc hoả linh , tiếc là bị phá . Chỉ cần sau này giữ đc thì chồng khá</p>
</blockquote>

<blockquote>
<p>Tham + hỏa với mệnh kim thì thất bại đầu đời và thân cu Phúc ở đất tuyệt địa rất khó , cuộc đời lao tâm khổ tứ bề</p>
</blockquote>

<h3>Tử Vi Anh Liễu luận</h3>

<p></p>

<p></p>

<p></p>

<p></p>

<p></p>
',
            'date_post'         => '2023-02-17',
            'thumbnail_post'    => 'la-so-lam-gai-ban-hoa-thumbnail.jpg',
            'id_cat_post'       => FAMOUS_PEOPLE_ID_CAT,
            'views'             => random_int(50,500),
            'enable_post'       => ENABLE,
        ]);

    }
}

// Lá số từng một thời gian ngắn làm tay vịn quán hát và làm gái bán hoa, cũng từng suýt làm vợ lẽ.
// Bổ sung 1 số hạn nổi bật khác:
// - 2018 bị lừa online mất tầm 100tr, sau đó báo công an và kẻ lừa đảo đi tù.
// - Cuối 2017, đầu 2018 cho đến 2020: Yêu xa 1 người sinh năm 1986. Người này có vợ con nhưng nói dối là độc thân. Sau đó LS có định làm đơn kiện nhưng bên đó xin hòa giải, giờ đã hoàn toàn kết thúc.
// Mời ACE nghiệm lý nhân tiện cho em hỏi bao giờ LS này có em bé ạ?
// [UPDATE]
// Từ hôm qua đến giờ có rất nhiều người quan tâm bài đăng, em rất cảm ơn ạ. Em có 1 số điều muốn nói như sau:
// - Công việc này LS làm một thời gian ngắn và làm thêm, chứ ko phải công việc toàn thời gian. Thời đấy tâm lý LS sụp đổ và chán chường, bất cần đời nên mới đưa ra quyết định như vậy. Sau đó thì đã bỏ hẳn, học hành và làm ăn chân chính.
// - Tiền kiếm được thời gian đó hầu hết là để chữa bệnh sùi mào gà (sau nhiễm bệnh thì ko đi làm nữa). Đây là 1 căn bệnh chữa trị đau đớn, tốn kém và nhục nhã. Nhưng cũng may là chưa HIV gì ạ 😞 Mỗi lần đi chữa là 2tr-5tr đi toi. Chưa kể tiền mua thuốc, tiêm vắc xin HPV.
// - Môi trường làm việc quán hát có kiếm được tiền đi nữa thì rất hại người do tính chất làm việc ban đêm, khói thuốc, rượu bia. Đêm làm thì ngày sẽ lờ đờ như zombie.
// - Em ko dám khuyên ai cái gì. Nhưng mong rằng mọi người sẽ chú ý đến con gái, chị em gái của mình hơn, ko để ai phải suy sụp cô đơn khi có biến cố gì mà nghĩ quẩn. Nhất là các bé gái sinh trưởng trong gia đình ko hạnh phúc cũng dễ tự ti, suy nghĩ lệch lạc về tình yêu. Em cũng mong ko có thêm người mắc các bệnh xã hội.
// - Và thêm nữa, học hành và lại động là con đường đi lên chân chính nhất ạ. Quá khứ khép lại, em đã học xong và đi làm với mức thu nhập và vốn sống kha khá.
// - Ở bài viết này, có người chửi em, có người động viên. Em rất cảm ơn và điều em mong là lá số cũng như câu chuyện của em có ý nghĩa dù ít dù nhiều với mọi người. Em xin cảm ơn!

// 6/6/1993 giờ Sửu
// Tên My
// Mệnh Tham Lang vượng tại Thìn
// https://www.facebook.com/photo/?fbid=719581209633483&set=gm.3501735470041851&idorvanity=1592102927671791
