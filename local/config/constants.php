<?php
// Rule: function/variable_website_page

// ========== ONE FOR ALL ==========

define('TITLE_TU_VI_HAO_TAM', ' | TỬ VI HẢO TÂM');
define('DOMAIN_TU_VI_HAO_TAM', '..');
define('DOMAIN_KAWAIICODE', '..');

define('FACEBOOK_URL', 'https://www.facebook.com/T%E1%BB%AD-vi-h%E1%BA%A3o-t%C3%A2m-106421171096202');

// === Enable ===

define('ENABLE', true);
define('UNENABLE', false);

// === Page ===

define('ITEM_PER_PAGE_4', 4);
define('ITEM_PER_PAGE_6', 4);
define('ITEM_PER_PAGE_10', 10);
define('ITEM_PER_PAGE_12', 12);
define('ITEM_PER_PAGE_18', 18);
define('ITEM_PER_PAGE_20', 20);
define('ITEM_PER_PAGE_30', 30);

// === Category Post ===

define('TUVI_POST_ID_CAT', 2);
define('FAMOUS_PEOPLE_ID_CAT', 3);
define('SAO_ID_CAT', 4);
define('CACH_CUC_ID_CAT', 5);

define('TUVI_CAT', 'tu-vi');
define('FAMOUS_PEOPLE_CAT', 'nguoi-noi-tieng');
define('SAO_CAT', 'sao');
define('CACH_CUC_CAT', 'cach-cuc');
define('TAM_LY_CAT', 'tam-ly');

// === Category Download ===

define('TUVI_DOWNLOAD_ID_CAT', 1);










// ========== CUNG ==========

define('BAN_MENH_URL', 'ban-menh');
define('PHU_MAU_URL', 'phu-mau');
define('HUYNH_DE_URL', 'huynh-de');
define('PHUC_DUC_URL', 'phuc-duc');
define('NO_BOC_URL', 'no-boc');
define('QUAN_LOC_URL', 'quan-loc');
define('THIEN_DI_URL', 'thien-di');
define('TAI_BACH_URL', 'tai-bach');
define('PHU_THE_URL', 'phu-the');
define('TU_TUC_URL', 'tu-tuc');
define('DIEN_TRACH_URL', 'dien-trach');
define('TAT_ACH_URL', 'tat-ach');

// ========== CHINH TINH ==========

define('THAI_DUONG_URL', 'thai-duong');
define('THAI_AM_URL', 'thai-am');
define('THIEN_CO_URL', 'thien-co');
define('THIEN_TUONG_URL', 'thien-tuong');
define('THIEN_LUONG_URL', 'thien-luong');
define('TU_VI_URL', 'tu-vi');
define('CU_MON_URL', 'cu-mon');
define('THIEN_DONG_URL', 'thien-dong');
define('VU_KHUC_URL', 'vu-khuc');
define('THIEN_PHU_URL', 'thien-phu');
define('LIEM_TRINH_URL', 'liem-trinh');
define('THAM_LANG_URL', 'tham-lang');
define('PHA_QUAN_URL', 'pha-quan');
define('THAT_SAT_URL', 'that-sat');

// ========== VONG THAI TUE ==========

define('THAI_TUE_URL', 'thai-tue');
define('BACH_HO_URL', 'bach-ho');
define('QUAN_PHU_URL', 'quan-phu'); // Quan Phù









define('QUAN_TRI_PHU_URL', 'quan-tri-phu'); // Quan Tri Phủ



// ========== 10 CAN ==========

define('CAN_GIAP_URL', 'can-giap');
define('CAN_AT_URL', 'can-at');

define('CAN_BINH_URL', 'can-binh');
define('CAN_DINH_URL', 'can-dinh');
define('CAN_MAU_URL', 'can-mau');
define('CAN_KY_URL', 'can-ky');

define('CAN_CANH_URL', 'can-canh');
define('CAN_TAN_URL', 'can-tan');
define('CAN_NHAM_URL', 'can-nham');
define('CAN_QUY_URL', 'can-quy');

// ========== 12 CON GIAP ==========

define('TI_URL', 'con-giap-ti');
define('SUU_URL', 'con-giap-suu');
define('DAN_URL', 'con-giap-dan');
define('MAO_URL', 'con-giap-mao');

define('THIN_URL', 'con-giap-thin');
define('TY_URL', 'con-giap-ty');
define('NGO_URL', 'con-giap-ngo');
define('MUI_URL', 'con-giap-mui');

define('THAM_URL', 'con-giap-than');
define('DAU_URL', 'con-giap-dau');
define('TUAT_URL', 'con-giap-tuat');
define('HOI_URL', 'con-giap-hoi');
