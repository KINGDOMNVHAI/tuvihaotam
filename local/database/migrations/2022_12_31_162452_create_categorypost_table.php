<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategorypostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->down();
        Schema::create('categorypost', function (Blueprint $table) {
            $table->increments('id_cat_post');
            $table->string('name_vi_cat_post');
            $table->string('url_cat_post');
            $table->boolean('enable_cat_post')->default(true);
            $table->integer('parent_cat')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categorypost');
    }
}
