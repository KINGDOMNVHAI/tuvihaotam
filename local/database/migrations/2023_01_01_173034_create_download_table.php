<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDownloadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->down();
        Schema::create('download', function (Blueprint $table) {
            $table->increments('id_download');
            $table->string('name_vi_download');
            $table->string('url_download');
            $table->string('description_vi_download')->nullable();
            $table->string('thumbnail_download')->nullable();
            $table->string('link_download')->nullable();
            $table->integer('id_cat_download');
            $table->boolean('enable_download')->default(true);
            $table->boolean('alert')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('download');
    }
}
