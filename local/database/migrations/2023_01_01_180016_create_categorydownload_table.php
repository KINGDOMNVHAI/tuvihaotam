<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategorydownloadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->down();
        Schema::create('categorydownload', function (Blueprint $table) {
            $table->increments('id_cat_download');
            $table->string('name_vi_cat_download');
            $table->string('url_cat_download');
            $table->boolean('enable_cat_download')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categorydownload');
    }
}
