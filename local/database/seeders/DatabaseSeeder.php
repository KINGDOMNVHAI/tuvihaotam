<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(CategoryDownloadTableSeeder::class);
        $this->call(CategoryPostTableSeeder::class);
        $this->call(DownloadTableSeeder::class);
        $this->call(PostsTableSeeder::class);
    }
}
