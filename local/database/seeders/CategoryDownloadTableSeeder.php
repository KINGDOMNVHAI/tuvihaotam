<?php
namespace Database\Seeders;

use App\Models\categorydownload;
use Illuminate\Database\Seeder;

class CategoryDownloadTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        categorydownload::create([
            'name_vi_cat_download'  => 'Tử vi',
            'url_cat_download'      => TUVI_CAT,
        ]);
    }
}
