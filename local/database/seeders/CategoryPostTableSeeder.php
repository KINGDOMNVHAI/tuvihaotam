<?php
namespace Database\Seeders;

use App\Models\categorypost;
use Illuminate\Database\Seeder;

class CategoryPostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        categorypost::create([
            'name_vi_cat_post'  => 'Không có Category',
            'url_cat_post'      => 'no-category',
            'enable_cat_post'   => false,
        ]);

        categorypost::create([
            'name_vi_cat_post'  => 'Tử vi',
            'url_cat_post'      => TUVI_CAT,
        ]);

        categorypost::create([
            'name_vi_cat_post'  => 'Người nổi tiếng',
            'url_cat_post'      => FAMOUS_PEOPLE_CAT,
        ]);

        categorypost::create([
            'name_vi_cat_post'  => 'Bộ sao',
            'url_cat_post'      => SAO_CAT,
            'parent_cat'        => TUVI_POST_ID_CAT,
        ]);

        categorypost::create([
            'name_vi_cat_post'  => 'Cách cục',
            'url_cat_post'      => CACH_CUC_CAT,
            'parent_cat'        => TUVI_POST_ID_CAT,
        ]);

        categorypost::create([
            'name_vi_cat_post'  => 'Tâm lý',
            'url_cat_post'      => TAM_LY_CAT,
        ]);
    }
}
