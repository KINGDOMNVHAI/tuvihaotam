<?php
namespace Database\Seeders;

use App\Models\download;
use Illuminate\Database\Seeder;

class DownloadTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        download::create([
            'name_vi_download'          => 'Tử Vi Nghiệm Lý Toàn Thư',
            'url_download'              => 'tu-vi-nghiem-ly-toan-thu',
            'description_vi_download'   => '<p>Với quyển sách Tử Vi Nghiệm Lý Toàn Thư của Cụ Thiên Lương, quý vị sẽ lĩnh hội những lý giải thâm sâu về Tử Vi, về tài thọ, về tam hóa liên châu...</p>

',
            'thumbnail_download'        => 'tu-vi-nghiem-ly-toan-thu.jpg',
            'link_download'             => DOMAIN_TU_VI_HAO_TAM . '/download/document/tu-vi-nghiem-ly-toan-thu.pdf',
            'id_cat_download'           => TUVI_DOWNLOAD_ID_CAT,
            'alert'                     => false,
        ]);

        download::create([
            'name_vi_download'          => 'Tử Vi Đẩu Số Toàn Thư',
            'url_download'              => 'tu-vi-dau-so-toan-thu',
            'description_vi_download'   => '<p>Với quyển sách Tử Vi Đẩu Số Toàn Thư của Cụ Thiên Lương, quý vị sẽ lĩnh hội những lý giải thâm sâu về Tử Vi, về tài thọ, về tam hóa liên châu...</p>

',
            'thumbnail_download'        => 'tu-vi-dau-so-toan-thu.jpg',
            'link_download'             => DOMAIN_TU_VI_HAO_TAM . '/download/document/tu-vi-dau-so-toan-thu.pdf',
            'id_cat_download'           => TUVI_DOWNLOAD_ID_CAT,
            'alert'                     => false,
        ]);
    }
}
