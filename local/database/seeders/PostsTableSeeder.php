<?php
namespace Database\Seeders;

use App\Models\posts;
use App\Seeder\MenhSeeder;
use App\Seeder\FamousSeeder;
use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $famous = new MenhSeeder;
        $famous->index();

        $famous = new FamousSeeder;
        $famous->index();

        posts::create([
            'name_vi_post'    => 'Ý nghĩa bộ sao Tướng Quân - Phục Binh',
            'url_post'        => 'y-nghia-bo-sao-tuong-quan-phuc-binh',
            'present_vi_post' => 'Mẫu người Tướng Binh là mẫu người như thế nào?',
            'content_vi_post' => '<p>Tướng Quân Phục Binh luôn nằm đối diện nhau, gọi tắt là Tướng Binh.</p>

<center>

<p></p></center>

<h3>Sao Tướng Quân</h3>

<p>Tướng Quân hành Mộc.</p>
<ul>
<li>Vượng ở
<li>Miếu ở
<li>Đắc ở
<li>Hãm ở
</ul>

<center><img src="../upload/img/post/tang-mon-1.jpg" width="100%" alt="Tướng Quân Phục Binh" /><br><br></center>

<p>Sao Tướng Quân có những đặc điểm tương tự như Thất Sát, Phá Quân. Người có sao Tướng Quân là người mạnh mẽ, can đảm, xông pha trận mạc, có tài điều binh khiển tướng.</p>

<p></p>

<p></p>

<h3>Sao Phục Binh</h3>

<p>Phục Binh hành Hỏa.</p>
<ul>
<li>Vượng ở
<li>Miếu ở
<li>Đắc ở
<li>Hãm ở
</ul>

<center><img src="../upload/img/post/phuc-binh-1.jpg" width="100%" alt="Tướng Quân Phục Binh" /><br><br></center>

<p>Trái ngược với Tướng Quân xông pha, chiến đấu thẳng mặt, không sợ ai, Phục Binh là sát thủ, thích khách, thừa cơ đối phương không để ý liền ra tay ám sát.
Phục Binh ở Mệnh là người có tính đố kỵ, cạnh tranh. Nhưng cách cạnh tranh của họ là nói xấu, chơi xấu, đánh lén.</p>

<p>Ngược lại, Phục Binh cũng ám chỉ người dễ bị làm phản, bị chơi xấu, nói xấu. Phục Binh cung Nô thì bị bạn bè chơi xấu. Tướng Quân cung Nô thì nếu có mâu thuẫn, kẻ xấu sẽ đánh trước mặt.
Đặc biệt, Phục Binh khác Tướng Quân là biết nhẫn nhịn, dồn nén cơn tức giận để rồi tung ra một cú đánh bất ngờ, ám sát đối thủ. Tướng Quân lại là loại người tức lên là nói thẳng, xả giận ngay lập tức.
Vì vậy, Phục Binh chính là loại người "ở nhà ngoan lắm" nhưng đột nhiên lại làm chuyện động trời.</p>

<p>Phục Binh có một tính chất đặc biệt: <b>ám chỉ yếu tố bất ngờ.</b> Phục Binh cung Tử Tức, dễ có con bất ngờ, ngoài dự kiến (lỡ mang thai, vỡ kế hoạch). Từ đó dẫn đến những câu chuyện bỏ con, gửi con lên chùa như báo đài vẫn thường đăng.</p>

<center><img src="../upload/img/post/phuc-binh-2.jpg" width="100%" alt="Tướng Quân Phục Binh" /><br><br></center>

<p>Về bệnh tật:</p>
<ul>
<li>Bệnh tật bất ngờ.
</ul>

<p>Người có sao Phục Binh nên theo những nghề phò tá, thư ký, những nghề không nổi tiếng, ẩn danh, giúp đỡ sếp, lãnh đạo.</p>

<p></p>



<h3>Thai Phục Vượng Tướng</h3>

<p>Bộ sao liên quan đến tình cảm, bao gồm sao Thai, Phục Binh, Đế Vượng, Tướng Quân.</p>

<p>Bộ sao này nếu rơi vào Mệnh hay cung Phu Thê, đương số sẽ lấy vợ/chồng có con riêng, có con trước hôn nhân.</p>


',
            'date_post'         => '2022-12-30',
            'thumbnail_post'    => 'y-nghia-bo-sao-tuong-quan-phuc-binh-thumbnail.jpg',
            'id_cat_post'       => SAO_ID_CAT,
            'views'             => random_int(50,500),
            'enable_post'       => ENABLE,
        ]);

        posts::create([
            'name_vi_post'    => 'Ý nghĩa bộ sao Tang Môn - Bạch Hổ',
            'url_post'        => 'y-nghia-bo-sao-tang-mon-bach-ho',
            'present_vi_post' => 'Mẫu người Tang Hổ là mẫu người như thế nào?',
            'content_vi_post' => '<p>Tang Môn Bạch Hổ luôn nằm đối diện nhau, gọi tắt là Tang Hổ.</p>

<center>

<p></p></center>

<h3>Sao Tang Môn</h3>

<p>Tang Môn hành .</p>
<ul>
<li>Vượng ở
<li>Miếu ở
<li>Đắc ở
<li>Hãm ở
</ul>

<center><img src="../upload/img/post/tang-mon-1.jpg" width="100%" alt="Tang Môn Bạch Hổ" /><br><br></center>

<p>Tang Môn là sự buồn bã, lo lắng, luôn trong trạng thái lo nghĩ về vấn đề nào đó. Tang Môn đóng ở đâu, người đó không ngừng suy nghĩ, ưu tư về cung đó. Tang Môn cung Nô, luôn suy nghĩ vê bạn bè, mối quan hệ bên ngoài.</p>

<p>Tang Môn còn là duyên nợ, duyên tiền kiếp. Người có Tang Môn ở Mệnh, Phu Thê, chuyện tình cảm dù có cãi nhau, thậm chí đi đến ly hôn, nhưng vợ chồng vẫn có lý do để ràng buộc với nhau (con cái, tiền bạc...).
Những người không đến được với nhau, họ vẫn tiếp tục theo đuổi. Nếu đương số không quyết tâm, tỉnh táo, có thể xảy ra ngoại tình.</p>

<p>Về yếu tố tình cảm, Tang Môn và Đà La giống nhau. Đó là sự dây dưa, ràng buộc với nhau. Tang Môn cung Phu còn là sự lo lắng về vợ/chồng của mình. Trước khi kết hôn, bạn lo lắng gì về người phối ngẫu? Sau khi kết hôn, bạn lo lắng gì về người phối ngẫu?
Đó có thể là lo về sự thủy chung, lo về sự nghiệp, tiền bạc, sức khỏe của người phối ngẫu.</p>

<h3>Sao Bạch Hổ</h3>

<p>Bạch Hổ hành Kim.</p>
<ul>
<li>Vượng ở
<li>Miếu ở
<li>Đắc ở
<li>Hãm ở
</ul>

<center><img src="../upload/img/post/bach-ho-1.jpg" width="100%" alt="Tang Môn Bạch Hổ" /><br><br></center>

<p>Khi Tang Môn là sự buồn rầu, lo lắng thì Bạch Hổ là sự chiến đấu, vượt qua nghịch cảnh. Tính chất mạnh mẽ, quyết đoán như hổ. Cũng vì vậy mà Bạch Hổ luôn thu hút sự chú ý của mọi người, gặp cả tích cực lẫn thị phi.</p>

<p>Bạch Hổ hành Kim, giống như Thất Sát, tính thẳng thắn, tập trung, kỷ luật, dùng lý trí hơn tình cảm. Cũng có thể sự mạnh mẽ này chỉ biểu hiện ở bên ngoài, bên trong họ sống tình cảm hơn.</p>

<p></p>

<p>Về bệnh tật:</p>
<ul>
<li>Bạch Hổ là họa về thú vật: chó cắn, côn trùng cắn. Bạch Hổ ở Điền Trạch, khu vực nơi ở, nơi làm việc nuôi động vật.
<li>Bạch Hổ dễ bị thương tich trong lúc làm việc, giống như hổ bị thương khi săn mồi.
</ul>

<p></p>

<center><img src="../upload/img/post/phi-liem-bach-ho-1.jpg" width="100%" alt="Tang Môn Bạch Hổ" /><br><br></center>

<p></p>

<p></p>
',
            'date_post'         => '2022-12-31',
            'thumbnail_post'    => 'y-nghia-bo-sao-tang-mon-bach-ho-thumbnail.jpg',
            'id_cat_post'       => SAO_ID_CAT,
            'views'             => random_int(50,500),
            'enable_post'       => ENABLE,
        ]);

        posts::create([
            'name_vi_post'    => 'Ý nghĩa bộ sao Thiên La - Địa Võng',
            'url_post'        => 'y-nghia-bo-sao-thien-la-dia-vong',
            'present_vi_post' => 'Mẫu người Tang Hổ là mẫu người như thế nào?',
            'content_vi_post' => '<p>Thiên La Địa Võng luôn nằm đối diện nhau, gọi tắt là La Võng.</p>

<p>sao Thiên La luôn được an ở cung Thìn, Địa võng luôn được an ở cung Tuất, 2 cung của Tứ Mộ.</p>

<center>

<p></p></center>

<center><img src="../upload/img/post/thien-la-dia-vong-1.jpg" width="100%" alt="Thiên La Địa Võng" /><br><br></center>

<p>Thành ngữ Thiên La Địa Võng ám chỉ tình thế "bị bao vây, không lối thoát". Thiên La Địa Võng, theo như phim Trung Quốc thường nói, là kế sách bao vây, không cho đối phương chạy thoát. Mặc dù chưa tấn công hay gây tổn thất, Thiên La Địa Võng khiến đối thủ phải khó khăn tìm cách thoát khỏi cảnh bị kiềm tỏa.</p>

<p>Thiên La Địa Võng còn ám chỉ lưới của trời đất, luật lệ, khuôn khổ không được phép vượt qua.</p>

<p>Vì vậy, Thiên La Địa Võng đóng ở đâu, cung đó sẽ bị cản trở, sự nghiệp khó phát triển, đi lại khó khăn, khó xuất ngoại, đi xa.
Nơi ở dễ bị bao vây bởi những kẻ khó ưa. Tình duyên hay công việc dễ bị người khác ngăn cấm.</p>

<p>Về mặt tích cực, Thiên La Địa Võng là sự kỷ luật. La Võng tại Điền Trạch, nơi ở, nơi làm việc là những nơi đặt khuôn khổ, phép tắc lên hàng đầu.
Người có La Võng tại Điền, Quan dễ làm nghề liên quan đến luật pháp, luật lệ như luật sư, cảnh sát, bộ đội. Nhưng điều đó không có nghĩa là bản thân đương số là người kỷ luật.
Có thể đương số sống trong môi trường kỷ luật, nhưng bản thân lại thích tự do, không ràng buộc.</p>

<p>Nếu La Võng ở Mệnh, sự kỷ luật, cẩn thận, khuôn khổ, khuôn phép được đặt lên cao hơn. Đương số là người cẩn thận, làm việc kỹ càng hơn. Nếu thêm nhiều phúc tinh, đương số sẽ dễ thăng tiến nhờ sự kỷ luật của bản thân.</p>

<p>Thiên La Địa Võng cần đồng cung với những sao đột phá mạnh như Sát Phá Tham, giống như hình tượng quân đoàn đột phá vòng vây kẻ địch, mở đường máu thoát thân vậy.</p>

<p>Nhưng không cần quá lo lắng. Thiên La Địa Võng chỉ là 2 ngôi sao nhỏ, tính chất không mạnh như bộ Lục Sát Tinh. Thiên La Địa Võng mang tính bao vây, vây hãm,
nghĩa là chúng chỉ có tác dụng lớn với những sao cần di chuyển, đi xa. Nếu gặp những sao mang tính thụ động như Thái Âm thì chẳng có ý nghĩa gì nhiều. Bản thân Thái Âm không thích di chuyển, chỉ muốn ở nhà. Nên bị bao vây cũng không sao.</p>

<h3>Thiên La Địa Võng đóng tại các cung</h3>

<p><b>Thiên La Địa Võng tại Mệnh: </b>dù có khả năng, tài giỏi thì làm gì cũng bị cản trở, khó thành công. Đương số làm bất kỳ điều gì, dù học hay chơi, công việc hay tình cảm chỉ ở mức trung bình khá.
Nếu thêm Lục Sát Tinh (Không Kiếp, Kình Đà, Linh Hỏa) thì cẩn thận có thể bị tù tội.</p>



<p><b>Thiên La Địa Võng tại Điền: </b>nơi ở, nơi làm việc là những nơi đặt khuôn khổ, phép tắc lên hàng đầu như công an, bộ đội, luật pháp. Đương số khó lòng đi xa khỏi nơi ở, có đi cũng phải quay về.</p>
',
            'date_post'         => '2023-01-02',
            'thumbnail_post'    => 'y-nghia-bo-sao-thien-la-dia-vong-thumbnail.jpg',
            'id_cat_post'       => SAO_ID_CAT,
            'views'             => random_int(50,500),
            'enable_post'       => ENABLE,
        ]);

        posts::create([
            'name_vi_post'    => 'Những sao giữ tài sản tốt nhất',
            'url_post'        => 'nhung-sao-giu-tai-san-tot-nhat',
            'present_vi_post' => 'Những sao có khả năng tích lũy, ngưng tụ.',
            'content_vi_post' => '<p>Trong tử vi có 2 loại sao: tán và tụ. Sao tán là sao gây hao tán, phân tán. Sao tụ là sao tích tụ, ngưng tụ lại.</p>

<h3>Cô Thần, Quả Tú</h3>

<center><img src="../upload/img/post/co-than-1.jpg" width="100%" alt="Cô Thần Quả Tú" /><br><br></center>

<p></p>

<p></p>

<p></p>

<h3>Hóa Kỵ</h3>

<p>Sao Hóa Kỵ hành Thủy</p>
<ul>
<li>Vượng ở Thìn, Tuất, Dậu, Mão.
<li>Hãm ở Tị, Ngọ, Tí, Sửu, Mùi, Hợi.
</ul>

<center><img src="../upload/img/post/co-than-1.jpg" width="100%" alt="Những sao giữ tài sản tốt nhất Hóa Kỵ" /><br><br></center>

<p>Hóa Kỵ có tính chất tụ. Về tinh thần, Hóa Kỵ tụ hợp những sự thị phi của thiên hạ vào mình. Về tài chính, Hóa Kỵ tích tụ tài sản, tiền bạc.</p>

<p>Đúng như tên gọi, Hóa Kỵ là sự ganh ghét, đố kỵ, hiểu lầm. Tuy nhiên, Lý do gì khiến người khác ghen ghét, đố kỵ với mình?
Hóa Kỵ chứng tỏ bạn phải có điểm gì đó hơn người, thậm chí là hơn vượt trội, đến mức mọi người xung quanh phải đổ kỵ với bạn.
Hóa Kỵ ở cung Tài giúp bạn dễ giàu có, khá giả hơn. Nhưng cái giá là Hóa Kỵ cũng tích tụ sự đố kỵ của mọi người vì bạn giàu có hơn họ.</p>

<h3>Thiên Phủ</h3>

<p>Thiên Phủ hành Thổ</p>
<ul>
<li>Vượng ở Thìn, Tuất.
<li>Miếu ở Dần, Thân, Tý, Ngọ.
<li>Đắc ở Tỵ, Hợi, Mùi.
<li>Bình ở Mão, Dậu, Sửu
</ul>

<center><img src="../upload/img/post/thien-phu-1.jpg" width="100%" alt="" /><br><br></center>

<p>Bộ sao Phủ Tướng là bộ sao dành cho các nhà quản lý. Lấy hình tượng Thiên Phủ là biệt thự, biệt phủ, kho chứa thóc gạo ngày xưa. Thiên Tướng là ông quan quản lý, canh giữ.
Vậy nên Thiên Phủ là nơi cất giữ, tích tụ tài sản.</p>

<p>Thiên Phủ rất tốt khi nằm ở cung Điền và Tài. Ai có Thiên Phủ miếu vượng ở Điền, người đó sở hữu rất nhiều điền sản.</p>

<p>Nhưng điền sản khác với tiền tài. Có những người được ở nhà cao cửa rộng, nhưng vì lý do nào đó, họ rất ít tiền trong túi. Trong công việc, đó có thể là lao công, người giúp việc hay những người quản lý cửa hàng, sale bất động sản.</p>


THIÊN PHỦ: Phú ông.
♦️Tôi là THIÊN PHỦ ở cái nhà to,
vì thế người ta gọi tôi là phú ông, phú hộ.
Thật tế cần kiểm tra mới biết.
Giàu thì phải có tài sản VŨ KHÚC chứ, phải có cái kho LỘC TỒN.

♦️Nếu có còn phải lo phản tác dụng bởi Không Vong.
Tôi ngại các ông KHÔNG này lắm. Vì có thể là cái nhà to ấy không phải là của tui.
🎩Các "Không Vong" gồm 3 cụ sau:
1. THIÊN KHÔNG KHÔNG VONG  (sao Địa Không đấy !.)
2. TUẦN TRUNG KHÔNG VONG. (Tuần)
3. TRIỆT LỘ KHÔNG VONG. (Triệt)

Là chú THẤT SÁT chứ ai !
(99% các 7 SÁT là lơ tơ mơ... gà mờ.)
♦️THIÊN PHỦ luôn luôn có kẻ bên ngoài quan sát, soi mói, nhòm ngó vào nhà, nghe tên nó khiến cũng lo lo. Vì nó mang tên là THẤT SÁT.
🎩THẤT SÁT có thể là tên đáng thương, thất nghiệp, mất nhà, mất vợ không con… càng mất nhiều thì Cụ THIÊN PHỦ lại càng lo nhiều. Cho nên Cụ THIÊN PHỦ thường có tính cẩn thận, thận trọng.
😎Cảm giác khi đọc trang Web nào họ cũng biết. Vì thế Cụ THIÊN PHỦ luôn luôn che đậy, che phủ, che chắn đến cả che chở. Vì sợ THẤT SÁT bên ngoài đoạt mất.
( THIÊN PHỦ .vs. THẤT SÁT )



<h3>Thái Âm</h3>

<p>Thái Âm hành Thủy</p>
<ul>
<li>Vượng ở Thân, Tý.
<li>Miếu ở Dậu, Tuất, Hợi.
<li>Đắc ở Sửu, Mùi.
<li>Hãm ở Dần, Mão, Thìn, Tỵ, Ngọ.
</ul>

<p></p>

<h3>Lộc Tồn</h3>

<ul>
<li>Vượng ở Tí, Ngọ, Thân, Dần, Mão, Dậu.
<li>Hãm ở .
</ul>

<p>Lộc Tồn hành</p>

<p>Lộc Tồn hiểu đơn giản là một kho tài sản đã được tích lũy từ trước. Vì vậy sẽ mất thời gian để bồi đắp, tích lũy. Lộc Tồn đóng cung Phúc là tốt nhất vì được thừa hưởng tài của ông bà tích lũy sẵn có.</p>

<p>Chính vì phải mất thời gian tích lũy nên Lộc Tồn là sự chậm chạp, chờ đợi, vun vén. Lộc Tồn cung Phu Thê, các mối tình phải kéo dài, chậm chạp, vun đắp, không thể chớp nhoáng như tình một đêm.</p>
',
            'date_post'         => '2023-01-03',
            'thumbnail_post'    => 'nhung-sao-giu-tai-san-tot-nhat-thumbnail.jpg',
            'id_cat_post'       => SAO_ID_CAT,
            'views'             => random_int(50,500),
            'enable_post'       => ENABLE,
        ]);

        posts::create([
            'name_vi_post'    => 'Các sao thể hiện vị trí nào trên cơ thể?',
            'url_post'        => 'cac-sao-the-hien-vi-tri-nao-tren-co-the',
            'present_vi_post' => 'Tác dụng của các sao ở cung Tật Ách.',
            'content_vi_post' => '<p></p>

<h3>Thái Dương</h3>

<p>Thái Dương hành Hỏa, tượng trưng cho mắt và đầu, phần thái dương.</p>

<p></p>

<p></p>

<h3>Thái Âm</h3>

<p>Thái Âm hành Thủy, tượng trưng cho mắt và vùng dạ dày.</p>

<p>Người có Thái Âm dễ bị các bệnh về mắt và dạ dày.</p>

<p></p>

<h3>Cự Môn</h3>

<p>Cự Môn hành Thủy, tượng trưng cho miệng đúng như tên gọi.</p>

<p>Người có Cự Môn dễ nhận thấy nhất là miệng rộng cằm nhọn. Ở cung Tật Ách sẽ gặp các bệnh liên quan đến miệng..</p>

<h3>Thiên Cơ</h3>

<p>Thiên Cơ hành </p>

<p></p>

<p></p>

<h3>Thiên Phủ</h3>

<p>Thiên Phủ hành Thổ</p>

<center><img src="../upload/img/post/thien-phu-1.jpg" width="100%" alt="" /><br><br></center>

<p></p>

<h3>Vũ Khúc</h3>

<p>Vũ Khúc hành Kim, tượng trưng cho cổ họng.</p>

<p></p>

<p></p>
',
            'date_post'         => '2023-01-04',
            'thumbnail_post'    => 'cac-sao-the-hien-vi-tri-nao-tren-co-the-thumbnail.jpg',
            'id_cat_post'       => SAO_ID_CAT,
            'views'             => random_int(50,500),
            'enable_post'       => UNENABLE,
        ]);

        posts::create([
            'name_vi_post'    => 'Lai Nhân Cung',
            'url_post'        => 'lai-nhan-cung',
            'present_vi_post' => 'Cung nào thay đổi cuộc đời bạn?',
            'content_vi_post' => '<p>Lai Nhân Cung là cung sẽ khiến bạn thay đổi cuộc đời, hoặc quyết định cuộc đời bạn sau này.</p>

<p>Lai Nhân Cung không xảy ra ở Huynh Đệ (anh em rồi mỗi người một ngả), Tật Ách (bệnh tật, nghiệp chướng), Nô Bộc (bạn bè, đối tác luôn thay đổi), Điền Trạch (nơi ở, nơi làm việc, tài sản có thể thay đổi).</p>

<h3>Lai Nhân Cung Bản Mệnh</h3>

<p>Đương số có tính cách độc lập, phụ thuộc vào năng lực bản thân. Người có Lai Nhân Cung Bản Mệnh, nếu Mệnh tốt thì cả đời tốt đẹp, ngược lại thì cả đời khó khăn.
Lý do vì Lai Nhân Cung Bản Mệnh, cuộc đời được quyết định bởi bản thân nên không có gì thay đổi.</p>

<p>Nếu cuộc sống quá khó khăn, người Lai Nhân Cung Bản Mệnh đầu tiên nên tự thay đổi bản thân mình để tốt lên.</p>

<h3>Lai Nhân Cung Quan Lộc</h3>

<h3>Lai Nhân Cung Tài Bạch</h3>

<h3>Lai Nhân Cung Thiên Di</h3>

<h3>Lai Nhân Cung Phụ Mẫu</h3>

<p>Ba mẹ tác động lớn đến cuộc đời đương số. Những lời dạy, những hành động của ba mẹ sẽ dễ ghi nhớ vào đầu con trẻ, khiến chúng thay đổi và làm theo.</p>

<h3>Lai Nhân Cung Phúc Đức</h3>

<h3>Lai Nhân Cung Phu Thê</h3>

<center><img src="../upload/img/post/co-than-1.jpg" width="100%" alt="Cô Thần Quả Tú" /><br><br></center>

<p>Vợ/chồng sẽ ảnh hưởng đến cuộc đời của đương số. Nếu đương số sống độc thân thì chính đương số là nhân tố chính ảnh hưởng đến cuộc đời của chính mình.
Lai Nhân Chung ở Tam Hợp của cung Phúc Đức (Phúc Di Phối) là thuộc cách tự lập cánh sinh.</p>

<p>Nhưng cũng có ý kiến cho rằng, nếu đương số độc thân thì những mối tình trước đây (đã có quan hệ thể xác) cũng có thể ảnh hưởng đến cuộc đời mình.
Lý do vì theo một số người, việc quan hệ thể xác cũng đã được tính vào cung Phu Thê.
Vậy nên nếu trường hợp không vợ chồng, không con cái mà đã từng quan hệ thể xác, thì mối tình đó cũng có thể ảnh hưởng đến cuộc đời đương số.</p>
',
            'date_post'         => '2023-01-05',
            'thumbnail_post'    => 'lai-nhan-cung-thumbnail.jpg',
            'id_cat_post'       => CACH_CUC_ID_CAT,
            'views'             => random_int(50,500),
            'enable_post'       => UNENABLE,
        ]);

        posts::create([
            'name_vi_post'    => 'Cung Thân Cư',
            'url_post'        => 'than-cu',
            'present_vi_post' => 'Cung Thân là cung gì?',
            'content_vi_post' => '<p>Cung Thân Cư, gọi tắt là cung Thân, là ...</p>

<p>Thân Cư không xảy ra ở Phụ Mẫu (ba mẹ không thể ở bên mình mãi), Huynh Đệ (anh em rồi mỗi người một ngả), Nô Bộc (bạn bè, đối tác luôn thay đổi), Tật Ách (bệnh tật, nghiệp chướng), Điền Trạch (nơi ở, nơi làm việc, tài sản có thể thay đổi).</p>

<h3>Thân Cư Bản Mệnh</h3>

<p></p>

<p>Người Thân Cư Bản Mệnh, mọi quyết định đều rất quyết đoán.</p>

<h3>Thân Cư Quan Lộc</h3>

<p>Sớm có định hướng nghề nghiệp, sự nghiệp rõ ràng. Không chắc đương số có giữ một nghề hay sẽ thay đổi nghề, nhưng sự nghiệp chung được vạch ra rõ ràng từ còn trẻ.</p>

<p>Thậm chí, cuộc sống đương số sẽ chịu ảnh hưởng từ quyết định công việc. Ví dụ như người khác sẽ ưu tiên gia đình, bản thân hơn công việc. Người Thân Cư Quan Lộc sẽ ưu tiên công việc hơn.</p>

<h3>Thân Cư Tài Bạch</h3>

<p>Sớm có ý thức tự lập, quan tâm đến việc kiếm tiền. Đương số quyết định mọi việc dựa vào tiền bạc, lời lỗ, tiền vào tiền ra.</p>

<h3>Thân Cư Thiên Di</h3>

<p>Đương số chắc chắn phải làm ăn xa quê, đi lại nhiều, số xuất ngoại. Thân Cư Thiên Di là người hướng ngoại, luôn thích tụ tập bạn bè, hội nhóm, là người của cộng đồng.
Một số thích gây sự chú ý. Số khác lại thích lăn lộn vì tiền.</p>

<p>Người thân cư Di dễ bị hoàn cảnh xã hội xung quanh tác động lên số mệnh. Ví dụ: nếu người thân cư Di không có được sự chú ý của mọi người, họ sẽ bị ảnh hưởng đến tâm lý.
Hoặc nếu bắt họ ở nhà, họ sẽ cảm thấy bị trói buộc, muốn thoát ra ngoài.</p>

<h3>Thân Cư Phúc Đức</h3>

<p>Những người sinh vào giờ Sửu và giờ Mùi đều có thân cư Phúc Đức.</p>

<p>Phúc Đức vừa mang ý nghĩa dòng họ, vừa mang ý nghĩa tinh thần. Đương số sẽ dựa vào tinh thần để quyết định mọi chuyện. Vì vậy, một tinh thần vững vàng và một cái đầu tỉnh táo là rất quan trọng.</p>

<p>Đương số cũng sẽ là người chăm nom phần mộ ông bà cha mẹ. Thậm chí, nếu ông bà cha mẹ có để lại nghiệp chướng, nợ nần, đương số cũng phải là người gánh vác.</p>

<p>Người có Thân Cư Phúc Đức cần tích nhiều phúc, làm nhiều việc thiện. Vì Phúc Đức, đúng như tên gọi, phụ thuộc vào việc đương số sống có tốt đời đẹp đạo hay không.</p>

<h3>Thân Cư Phu Thê</h3>

<center><img src="../upload/img/post/co-than-1.jpg" width="100%" alt="Cô Thần Quả Tú" /><br><br></center>

<p>Những hành động, quyết định của người thân cư Phu Thê sẽ phụ thuộc vào ý kiến, quyết định của vợ/chồng. Ví dụ: người phối ngẫu bảo ở nhà là ở nhà, đi là đi.
Nếu cung Phu Thê tốt, quyết định của người phối ngẫu sẽ giúp ích rất nhiều. Ngược lại, họ sẽ gây ảnh hưởng xấu đến đương số.</p>

<p>Khác với Lai Nhân Cung Phu Thê, cuộc đời đương số chưa chắc thay đổi hoàn toàn vì vợ/chồng. Thân Cư Phu Thê mang tính duyên nợ, trả nợ kiếp trước nhiều hơn.</p>
',
            'date_post'         => '2023-01-06',
            'thumbnail_post'    => 'than-cu-thumbnail.jpg',
            'id_cat_post'       => CACH_CUC_ID_CAT,
            'views'             => random_int(50,500),
            'enable_post'       => ENABLE,
        ]);

        posts::create([
            'name_vi_post'    => 'Sao nhanh và sao chậm',
            'url_post'        => 'sao-nhanh-va-sao-cham',
            'present_vi_post' => 'Những sao có tính chất nhanh nhẹn và chậm chạp.',
            'content_vi_post' => '<p></p>

<h3>Những sao nhanh nhẹn</h3>

<p><b>Phi Liêm</b></p>

<center><img src="../upload/img/post/sao-phi-liem-world-cup.jpg" width="100%" alt="Phi Liêm World Cup 2022" /><br><br></center>

<p>Phi Liêm giống như mũi tên, được bắn đi với vận tốc nhanh. Người có Phi Liêm luôn phải di chuyển liên tục, di chuyển rất nhanh.
Nô Bộc có Phi Liêm thì thay đổi bạn bè. Quan Lộc có Phi Liêm thì công việc phải di chuyển. Nếu công việc ngồi một chỗ cũng phải thay đổi công ty, cơ quan liên tục.
Chính vì phải di chuyển nên số lượng bạn cũng rất hạn chế.</p>

<p>Phi Liêm cũng ám chỉ mọi thứ xuất hiện bất ngờ. Nếu Phi Liêm đi cùng các sao tốt thì vận tốt đến bất ngờ như trúng số, nhặt tiền, tình yêu sét đánh...
Ngược lại, Phi Liêm đi cùng các sao xấu thì tai họa tới bất ngờ như tai nạn giao thông.</p>

<p><b>Địa Không Địa Kiếp</b></p>

<p>Thuộc hành Hỏa, Không Kiếp giống như những ngọn lửa, bùng cháy rồi lại tắt, thể hiện sự nhanh tới, nhanh đi. Cung Phu Thê có Không Kiếp thì </p>

<p>Địa Kiếp </p>

<p></p>

<p></p>

<p></p>

<p><b>Thiên Mã</b></p>

<p>Sao Thiên Mã là sao di chuyển, ngựa phi, bôn ba khắp nơi. Tùy vào Thiên Mã đắc hay hãm, cộng thêm các sao khác mà suy xét đương số có xuất ngoại được hay không.</p>

<p>Thiên Mã chỉ nằm ở 4 cung: Dần, Thân, Tỵ, Hợi (4 góc lá số).</p>

<p>Nếu Thiên Mã có thêm các sao di chuyển khác như Thiên Đồng, Lưu Hà thì đi nhiều và nhanh hơn nữa. Nếu Mệnh Cự Môn giỏi ngoại ngữ, khả năng xuất ngoại tăng cao.</p>

<p><b>Sát Phá Liêm Tham</b></p>

<p></p>

<p></p>

<p></p>

<p><b>Hỏa tinh Linh tinh</b></p>

<p>Có vẻ các sao có tính nhanh nhẹn thường là các sao hành Hỏa. Hỏa tinh Linh tinh mang tính chất quyền quyền uy cao.
Có lẽ do Linh Hỏa mang tác phong nhanh mạnh và khẩn cấp, đúng tác phong quân lệnh của con nhà lính. </p>

<p></p>

<p></p>


<h3>Những sao chậm chạp</h3>

<p><b>Phi Liêm</b></p>

<p>Đáng ngạc nhiên khi Phi Liêm cũng là sao chậm chạp. Phi Liêm giống như mũi tên, được bắn đi với vận tốc nhanh. Nhưng ngược lại, mũi tên cần được rút ra, giương cung và bắn với số lượng nhất định nên rất chậm chạp khi chuẩn bị.</p>

<p>Phi Liêm thiên về phân hóa và chắt lọc. Ý nghĩa tích cực là chọn lọc cẩn trọng.
Ý nghĩa tiêu cực là chủ về chia rẽ bè phái trong tập thể, chỉ chơi với bạn hợp tính mình, chọn người yêu cẩn thận, dẫn đến chậm gia đình, ít bạn bè.
Giống như số lượng mũi tên trong ống vậy.</p>

<center><img src="../upload/img/post/sao-phi-liem-2.jpg" width="100%" alt="Phi Liêm" /><br><br></center>

<p>Nếu phi liêm đóng ở Di hay Nô chủ về các mối quan hệ bạn bè, đồng nghiệp hay làm ăn sẽ có sự phân hóa và lựa chọn cẩn trọng.
Nếu đóng ở Tài Bạch và Quan Lộc thì thiên hướng chuyên môn hóa cao trong công việc, quản lý thu chi chặt.
Nếu Phi Liêm chọn lọc kỹ gặp Đào Hoa quen nhiều người khác giới thì đây là người quen rất nhiều bạn khác giới, làm rất nhiều ngành nghề nhưng chỉ có vài người, vài nghề chuyên sâu. Còn lại chỉ là tài lẻ, người qua đường.</p>

<p></p>

<p></p>

<p><b>Cô Thần Quả Tú</b></p>


',
            'date_post'         => '2023-01-07',
            'thumbnail_post'    => 'sao-nhanh-va-sao-cham-thumbnail.jpg',
            'id_cat_post'       => SAO_ID_CAT,
            'views'             => random_int(50,500),
            'enable_post'       => UNENABLE,
        ]);

        posts::create([
            'name_vi_post'    => 'Các cách cục trong tử vi',
            'url_post'        => 'cac-cach-cuc-trong-tu-vi',
            'present_vi_post' => 'Chính tinh và phụ tinh có thể tạo thành các cách cục nào?',
            'content_vi_post' => '<p></p>

<p></p>

<p></p>

<p></p>

<h3>Linh Xương Đà Vũ</h3>

<center><img src="../upload/img/post/linh-tinh-1.jpg" width="100%" alt="Linh Xương Đà Vũ" /><br><br></center>

<p>Linh Xương Đà Vũ biểu hiện cái chết của đương số. Khác với Địa Kiếp, tai họa bất ngờ, chết nhanh gọn lẹ. Cái chết do bộ Linh Xương Đà Vũ gây ra là cái chết đớn đau, dai dẳng cả về tinh thần lẫn thể xác.</p>

<p></p>

<ul>
<li>Linh Tinh: tượng trưng cho kim loại nóng chảy, nóng tính, nói xấu, nghĩ xấu người khác.
<li>Văn Xương:
<li>Đà La: sự dây dưa, kéo dài về tình cảm.
<li>Vũ Khúc: cô đơn, cô độc do quá thẳng tính.
</ul>

<p>Thật ra, tuy sách vở đều đề cập 4 sao: Linh Tinh, Văn Xương, Đà La, Vũ Khúc. Nhưng thật ra, yếu tố chính là Linh Xương Đà, và 2 hạt nhân quan trọng nhất là Xương Đà.</p>

<p>Vũ Khúc hành kim có thể thay thế bằng Kiếp Sát cũng là kim loại, dao kéo, mổ xẻ. Thiên Cơ hay Hóa Lộc cũng dễ cô đơn, thẳng tính, máu cạnh tranh, lý trí mà dẫn đến thiệt tình cảm.
Chỉ khác là cách dai dẳng, hành hạ, thậm chí là dẫn đến cái chết sẽ khác nhau.</p>

<ul>
<li>Vũ Khúc: hành hạ đương số bằng sự cô độc, thẳng tính, thậm chí vì thẳng tính quá mà dẫn đến hậu quả.
<li>Hóa Lộc: ham mê kiếm tiền, thậm chí là kiếm tiền bằng mọi giá cũng sẽ hành hạ đương số.
<li>Thiên Cơ: đi với Hỏa Linh, Kình Đà luôn nghĩ xấu, nghĩ về những thứ tiêu cực, dẫn đến mất lòng mọi người, sống trong tư tưởng u ám.
</ul>



<h3>Diêu Đà Kỵ</h3>

<p>Tam ám Diêu Đà Kỵ là bộ sao nổi tiếng về hoang dâm, dâm dục.</p>

<ul>
<li>Thiên Diêu: dâm tinh, sinh lý cao, buồn sầu, đa cảm, lãng mạn.
<li>Đà La: sự dây dưa, kéo dài về tình cảm.
<li>Hóa Kỵ: tai tiếng, nói xấu.
</ul>

<p>Nếu bộ sao này hãm địa, dù trai hay gái đều trở thành những kẻ đam mê sắc dục, làm kỹ nữ, thường lui đến chốn lầu xanh.</p>

<p>Nếu bộ sao này đắc địa, nhu cầu sinh lý không giảm như sẽ được giấu đi, kín đáo hơn, ít thể hiện ra bên ngoài.
Nghĩa là người này bên ngoài trông như bao người khác, nhưng bên trong có thể ngoại tình, đi tìm những điều lạ bên ngoài.</p>

<p></p>

<h3>Lộ Thượng Mai Thi</h3>

<p>Thất Sát và Liêm Trinh gặp nhau tại Sửu Mùi. 2 sao này nằm trong bộ Sát Phá Liêm Tham, bộ sao dành cho binh lính, quan võ, người xông pha trận mạc, khai phá vùng đất mới.</p>

<p>Lộ Thượng Mai Thi (vùi thây trên đường) ám chỉ việc chết vì tai nạn giao thông, chết nơi đất khách quê người, ra chiến trận. Tuy nhiên, chưa xác định rõ họ chết vì điều gì.
Có thể họ đến nơi đất khách quê người rồi qua đời vì tuổi già. Hoặc cũng có thể xấu hơn là gặp tai nạn rồi qua đời.
Nhưng một điều chắc chắn: người này sẽ qua đời ở xa quê.</p>

<p>Nếu vào cung Nô Bộc, có thể bạn bè, người quen mất ở nơi xa quê. Nhưng cũng có thể chính đương số sẽ mất khi rơi vào Đại Vận này. Tất nhiên còn phải xem các sao cứu giải khác như Thiên Lương, Hóa Khoa...</p>

<p>Khả năng mất nơi đất khách quê người sẽ cao hơn nếu đương số có một lá số di chuyển nhiều. Có thể kể đến các sao dịch chuyển như Thiên Đồng, Thiên Mã, Phá Quân, Phi Liêm, Lưu Hà...</p>

<p></p>

<p></p>


https://tuvicaimenh.com/sao-thien-rieu.html


',
            'date_post'         => '2023-01-08',
            'thumbnail_post'    => 'cac-cach-cuc-trong-tu-vi-thumbnail.jpg',
            'id_cat_post'       => CACH_CUC_ID_CAT,
            'views'             => random_int(50,500),
            'enable_post'       => UNENABLE,
        ]);

        posts::create([
            'name_vi_post'    => 'Bạn lấy chồng lớn tuổi hay nhỏ tuổi?',
            'url_post'        => 'ban-lay-chong-lon-tuoi-hay-nho-tuoi',
            'present_vi_post' => 'Bạn sẽ lấy vợ/chồng lớn tuổi hơn hay nhỏ tuổi hơn?',
            'content_vi_post' => '<p></p>

<p></p>

<p></p>

<p></p>

<h3>Tính cách</h3>

<p>Nếu cung Phu Thê của bạn là những bộ sao có tính cách hưởng thụ, tính trẻ con như Thiên Đồng, Thái Âm, Nguyệt Đức... thì khả năng cao bạn sẽ lấy vợ/chồng là người nhỏ tuổi hơn.</p>

<p>Ngược lại, nếu cung Phu Thê của bạn thể hiện vợ/chồng là người từng trải, va chạm xã hội, chững chạc như </p>

<h3>Địa vị, tài sản</h3>

<p>Nếu cung Phu Thê có những sao thể hiện địa vị, tài sản như Hóa Quyền, Thiên Phủ, khả năng cao vợ/chồng là người lớn tuổi hơn bạn.
Vì thực tế, những người có chức quyền, địa vị, tài sản nhiều đều là những người đã va chạm xã hội, mất nhiều thời gian, công sức để có sự nghiệp.
Khả năng một người tính cách trẻ con, ít va chạm như Thái Âm mà có nhiều địa vị xã hội là rất thấp, mặc dù Thái Âm cũng có ý nghĩa là đất đai, điền sản.</p>

',
            'date_post'         => '2023-01-09',
            'thumbnail_post'    => 'ban-lay-chong-lon-tuoi-hay-nho-tuoi-thumbnail.jpg',
            'id_cat_post'       => CACH_CUC_ID_CAT,
            'views'             => random_int(50,500),
            'enable_post'       => UNENABLE,
        ]);








        posts::create([
            'name_vi_post'    => 'Các sao ám Tinh',
            'url_post'        => 'cac-sao-am-tinh',
            'present_vi_post' => 'Ám Tinh bao gồm những sao nào?',
            'content_vi_post' => '<p>Ám Tinh là những sao không gây hại về mặt cơ thể, thương tích cho đương số. Nhưng ám tinh sẽ khiến cuộc đời đương số chịu nhiều tai tiếng, cản trở sự nghiệp, thậm chí ly hôn, ly thân.</p>

<center><img src="../upload/img/post/ghost-pokemon.jpg" width="100%" alt="Ám Tinh" /><br><br></center>

<p>Ám Tinh gồm 4 sao: Cự Môn, Thiên Diêu, Đà La, Hóa Kỵ. Tính chất chủ yếu là sự che mờ, cản trở. Hãy cùng tìm hiểu tính chất và mức độ của những sao này.</p>

<h3>Cự Môn</h3>

<p>Cự Môn hành Thủy, miếu ở Mão Dậu, vượng ở Tý Ngọ Dần, đắc ở Thân Hợi. Cự Môn đồng cung với Thiên Cơ ở Mão Dậu.</p>

<p>Miệng rộng, đúng như tên gọi, là bộ sao mà khi ở Mệnh, đương số sẽ có đặc điểm đặc trưng là miệng rộng cằm nhọn. Và như ông bà ta có câu "Họa từ miệng mà ra", càng nói nhiều, càng dễ bị tai tiếng.</p>

<center><img src="../upload/img/post/smile-horror-1.jpg" width="100%" alt="Ám Tinh" /><br><br></center>

<p>Đặc tính tốt của Cự Môn là tài ăn nói. Khả năng ăn nói lanh lợi của Cự Môn có thể đem lại nhiều mối quan hệ tốt, mối làm ăn mới. Đặc biệt, Cự Môn học rất giỏi ngoại ngữ. Người có Cự Môn dễ xuất ngoại, làm cho các công ty lớn, công ty nước ngoài nhờ tài ngoại ngữ của họ.</p>

<p>Nhưng Cự Môn học nói 3 năm, cả đời tu khẩu. Người có Cự Môn ở cung nào, cung đó sẽ dễ xảy ra nói xấu, chế giễu, cãi vã, khó có sự thông cảm.
Vì vậy, Cự Môn sẽ đẹp nhất khi đi cùng với Thiên Cơ. Khả năng ăn nói cùng với cái đầu thông minh sẽ ứng với câu "Uốn lưỡi 7 lần trước khi nói", giúp giảm bớt những trường hợp nói hớ, nói không suy nghĩ.</p>

<h3>Hóa Kỵ</h3>

<p>Sao Hóa Kỵ hành Thủy</p>
<ul>
<li>Vượng ở Thìn, Tuất, Dậu, Mão.
<li>Hãm ở Tị, Ngọ, Tí, Sửu, Mùi, Hợi.
</ul>

<p>Đúng như tên gọi, <b>Hóa Kỵ là sự ganh ghét, đố kỵ.</b> Hóa Kỵ ở đâu, nơi đó có sự ganh ghét, tranh cãi như Cự Môn. Nhưng sự ganh ghét thì mang tính chất ngầm hơn Cự Môn thể hiện ra bên ngoài.</p>

<p>Hóa Kỵ có đặc điểm nổi bật là <b>sự dang dở, đầu voi đuôi chuột.</b> Hóa Kỵ đóng ở đâu, chỗ đó luôn trong tình trạng đầu voi đuôi chuột. Đóng ở Phu Thê thì chuyện tình dang dở, sớm muộn cũng chia tay (nhưng ly hôn biệt tích hay không còn phải xem duyên nợ). Đóng ở Quan thì công việc khó kéo dài.</p>

<p>Hóa Kỵ còn gây ra sự hiểu lầm, khiến người khác hiểu sai về bạn, đánh giá sai năng lực hay tâm lý của bạn. Ví dụ như bạn là một người học giỏi, năng lực tốt. Nhưng về bằng cấp, bạn lại không có trong tay tấm bằng nào.
Điều đó có thể khiến bạn bị nghi ngờ, đánh giá sai năng lực, mất nhiều công sức để chứng tỏ bản thân hơn.</p>

<p>Tuy nhiên, nói đi cũng phải nói lại. Lý do gì khiến người khác ghen ghét, đố kỵ với mình? Hóa Kỵ chứng tỏ bạn phải có điểm gì đó hơn người, thậm chí là hơn vượt trội, đến mức mọi người xung quanh phải đổ kỵ với bạn.
Hóa Kỵ ở cung Tài giúp bạn dễ giàu có, khá giả hơn. Nhưng cái giá là Hóa Kỵ cũng tích tụ sự đố kỵ của mọi người vì bạn giàu có hơn họ.</p>

<p>Hóa Kỵ cũng có đặc tính là <b>sự dơ dáy, bừa bộn.</b> Hóa Kỵ cung Điền thì nhà cửa bừa bộn, xập xệ.</p>

<p>Hóa Kỵ là ám tinh. <b>Khi đi cùng chính tinh nào, các bộ phận trên cơ thể đó sẽ gặp vấn đề.</b> Hóa Kỵ và Cự Môn thì miệng méo. Hóa Kỵ với Nhật Nguyệt thì mắt kém.</p>

<p>Đặc điểm cuối cùng là <b>sự thu mình, thu vào.</b> Hóa Kỵ tốt khi ở cung Tài, cung Điền. Khi đó, sự thu vào của Hóa Kỵ sẽ giúp tích tụ tài sản, giúp nhanh giàu có.
Hoặc có thể hiểu đơn giản: công việc, vợ chồng có thể đầu voi đuôi chuột, dang dở, nhưng việc kiếm tiền thì chắc chắn phải làm cả đời. Dù còn trẻ con hay về già, tiền bạc và tài sản là thứ bạn phải làm cả đời.</p>

<p>Tóm lại, Hóa Kỵ có hình dạng như cái phễu. Phần đầu vào lớn, phần đầu ra nhỏ, giúp tích lũy tài sản tốt, nhưng không phù hợp với các sự nghiệp hay tình cảm lâu dài.</p>

<center><img src="../upload/img/post/pheu-inox-1.jpg" width="50%" alt="Hóa Kỵ" /><br><br></center>

<h3>Thiên Diêu</h3>

<p>Sao Thiên Diêu hành Thủy, đắc ở Dần, Thân, Mão, Dậu. Hãm địa ở Tý, Ngọ, Sửu, Mùi, Thìn, Tuất, Tị, Hợi.</p>

<p>Thiên Diêu nổi tiếng với đặc điểm đam mê khoái lạc, phong lưu, tửu sắc, lãng mạn, bay bổng. Tính cách này chỉ được giảm bớt giải khi ở đắc địa. Lúc này, những tính cách ham khoái lạc sẽ được ẩn đi.</p>

<center><img src="../upload/img/post/ky-nu-1.jpg" width="100%" alt="Đà La" /><br><br>

<p>Kỹ nữ thời xưa (ảnh minh họa)</p></center>

<p>Thiên Diêu lãng mạn, nếu ở Quan thì có thể làm những công việc liên quan đến văn học nghệ thuật.</p>

<p>Thiên Diêu luôn đồng cung Thiên Y, nằm trong tam hợp với Thiên Hình, tạo thành bộ Diêu Y Hình. Lúc này, khả năng về y học được thể hiện rõ. Thiên Y chủ về y học. Thiên Diêu chủ về thảo dược. Châm cứu, bấm huyệt là Thiên Hình. Nếu có Hóa Kỵ thì mang ý nghĩ Đông y.</p>

<p>Một điểm cần lưu ý: Thiên Diêu nói về ham mê tửu sắc. Nhưng người ham mê tửu sắc lại luôn phải có sức khỏe tốt. Chắc chắn không ai sức khỏe yếu lại ham khoái lạc cả.
Vì vậy, Thiên Diêu cần có Thiên Y chủ về y học, ý nghĩa là giải được các bệnh lý gây hại. Bên cạnh đó, phải xem sức khỏe đương số có tốt không. Sức khỏe càng tốt thì càng có điều kiện tận hưởng khoái lạc.</p>

<h3>Đà La</h3>

<p>Sao Đà La hành Kim, vượng ở Thìn, Tuất, Sửu, Mùi. Đắc ở Tí, Hợi. Hãm ở Dần, Thân.</p>

<p>Đà La có đặc điểm nổi bật là <b>sự dây dưa, vương vấn, mắc nợ nhau.</b> Đặc điểm này giống với Tang Môn, cũng là duyên nợ tiền kiếp.
Điều này trái ngược với Hóa Kỵ gãy gánh giữa đường. Nhờ có đặc tính này nên mối quan hệ tình cảm được níu giữ lại. Vợ chồng gặp Hóa Kỵ có thể ly dị.
Nhưng nếu Mệnh hay Phu Thê có Đà La, Tang Môn thì mối quan hệ lại ở mức ly thân. Thậm chí ly dị rồi vẫn quay lại với nhau.</p>

<center><img src="../upload/img/post/your-name-1.jpg" width="100%" alt="Đà La" /><br><br>

<p>Đà La như một sợi chỉ đỏ, khiến mối tình dây dưa, mắc nợ nhau.</p></center>

<p>Nhưng sự dây dưa đó cũng là một dạng ám tinh. Cung Phu Thê có Đà La thì chậm lập gia đình. Nếu ly thân chưa chắc họ đã ly hôn.
Chính sự dây dưa đó khiến cả 2 khó bước thêm bước nữa, không thật sự thoát khỏi nhau. Có thể vợ chồng cũ vẫn qua lại với nhau vì con cái.
Người yêu cũ vẫn gặp nhau dù cả 2 đã có gia đình riêng. Đó là một dạng ám tinh, nhưng cảm giác này chưa hẳn đương số đã ghét nó.</p>

<p>Đà La ở Mệnh biểu hiện qua đôi mắt lớn, lồi, nếu hãm địa dễ bị cận thị.</p>

<p>Đà La ở Mệnh, hành động và suy nghĩ không nhất quán, nhiều khi rơi vào cảnh không ai hiểu, khóc thầm. Đà La với Cự Môn, thị phi ngầm sau lưng.</p>

<h3>Diêu Đà Kỵ</h3>

<p>Tam Ám Diêu Đà Kỵ là bộ sao nổi tiếng về hoang dâm, dâm dục, đường tình trắc trở, đầu voi đuôi chuột.</p>

<ul>
<li>Thiên Diêu: dâm tinh, sinh lý cao, buồn sầu, đa cảm, lãng mạn.
<li>Đà La: sự dây dưa, kéo dài về tình cảm.
<li>Hóa Kỵ: tai tiếng, nói xấu, gãy gánh giữa đường.
</ul>

<p>Chỉ cần 2 trong 3 Tam Ám cũng khiến cho chuyện tình cảm của đương số dễ rơi vào cảnh đa tình bên ngoài nhưng cô đơn đường gia đạo. Đương số sa vào nhiều cuộc tình, người quen kẻ lạ có đủ. Nhưng đều có chung kết cục là về sau đều gãy gánh và tìm mối tình mới.</p>

<p>Tuy nhiên, nếu có Đà La thì những mối tình gãy gánh đó vẫn sẽ quay lại như người yêu cũ, vợ chồng cũ. Dẫn đến cảnh ngoại tình lén lút. Nếu vỡ lở thì tai tiếng càng lộ rõ.</p>

<center><img src="../upload/img/post/material-look-the-girl-meme-1.jpg" width="100%" alt="Tam Ám Diêu Đà Kỵ" /><br><br></center>

<p>Diêu Đà Kỵ là cũng ám chỉ người mê tín, hay đi lễ chùa. Nhưng cũng chưa hẳn là người mê tín mà có thể là người có duyên với tâm linh.</p>
',
            'date_post'         => '2023-01-10',
            'thumbnail_post'    => 'cac-sao-am-tinh-thumbnail.jpg',
            'id_cat_post'       => CACH_CUC_ID_CAT,
            'views'             => random_int(50,500),
            'enable_post'       => ENABLE,
        ]);

        posts::create([
            'name_vi_post'    => 'Vô Chính Diệu 12 cung',
            'url_post'        => 'vo-chinh-dieu-12-cung',
            'present_vi_post' => 'Vô Chính Diệu của 12 cung sẽ như thế nào?',
            'content_vi_post' => '<p>Vô Chính Diệu (VCD) là trường hợp cung không có chính tinh. Một số người cho rằng: <b>nếu cung VCD thì hãy xem cung đối diện chiếu qua.</b> Nhưng một số khác thì không đồng ý. Họ xem cung VCD có ý nghĩa riêng.</p>

<p>Vậy với cách xem cung VCD có ý nghĩa riêng thì ý nghĩa của 12 cung Vô Chính Diệu là gì? Tử Vi Anh Liễu sẽ tổng hợp giúp mọi người.</p>

<p>Những trường hợp cần nhớ:</p>

<p><strong>Nếu cung VCD thì hãy xem cung xung chiếu.</strong> Mặc dù vẫn có nhiều ý kiến tranh luận, nhưng đây cũng là một cách nên học hỏi.</p>

<p><strong>Vô Chính Diệu đắc Tam Không, Tứ Không:</strong> cung VCD có các sao Tuần, Triệt và Thiên Không, Địa Không chiếu hoặc nằm trong cung đó. Lúc này, cung đó không đến nỗi quá xấu.</p>

<p>Thứ 2: khi gặp VCD, phụ tinh trong cung đó sẽ có vai trò quan trọng hơn. Chúng ta có thể giải thích đơn giản:</p>

<ul>
<li>Cung có chính tinh giống như cây có rễ cọc. Cứ theo chính tinh mà luận. rễ cọc cứ cắm thẳng xuống còn các phụ tinh rẽ nhánh xung quanh.
<li>Cung VCD giống như cây có rễ chùm. Cây không có cọc mà cứ đâm chồi ra khắp nơi. Các phụ tinh giống như những rễ chùm đâm chồi xung quanh.
</ul>

<h3>Mệnh Vô Chính Diệu</h3>

<p>Mệnh VCD được mô tả như nhà không nóc, rất vất vả, dễ bị lung lay bởi gió bão. Nên nhược điểm của Mệnh VCD là dễ bị lay chuyển, tâm trí không vững vàng, không nên làm những chức vụ đứng đầu, lãnh đạo.
Người Mệnh VCD giống như một cái bóng, hỗ trợ, núp sau những người cứng cáp, vững vàng hơn. Vì nếu Mệnh VCD phải dựa vào cung đối diện - tức là cung Thiên Di - để luận đoán thì chẳng phải người Mệnh VCD đang dựa vào người bên ngoài để đoán hay sao?
Cũng chính vì cần một chủ nhân để phò tá, cần xem cung Mệnh hay các cung khác như Thiên Di, Nô Bộc để luận đoán xem bản thân mình nên phò tá ai? Loại người như thế nào? Có hợp tính cách hay không?</p>

<p>Ngược lại, người Mệnh VCD lại có khả năng thích nghi nhiều hoàn cảnh, tính cách uyển chuyển. Và người Mệnh VCD cũng không cần quá lo về căn nhà không nóc này. Nếu các phụ tinh có nhiều phúc tinh thì nhà không nóc cũng khá vững chãi, ít nhất là không bị đổ. Còn nếu nhiều hung tinh thì rất nguy hiểm.</p>

<center><img src="../upload/img/post/menh-vo-chinh-dieu-nha-khong-noc-1.jpg" width="100%" alt="Mệnh Vô Chính Diệu" /><br><br>

<p>Nhà không nóc có nhiều phúc tính</p>

<img src="../upload/img/post/menh-vo-chinh-dieu-nha-khong-noc-2.jpg" width="100%" alt="Mệnh Vô Chính Diệu" /><br><br>

<p>Nhà không nóc có nhiều hung tính</p></center>

<p>Một số người cho rằng, Mệnh VCD yểu mệnh. Giống như căn nhà không có nóc thì dễ bị lung lay, có thể sập bất kỳ lúc nào.
Hay như người ta thường nói "Đa tài bạc mệnh", người tài thường mất sớm.
Vì vậy, để tránh tai họa này, cần phải có tai họa khác như ly dị, ly thân.</p>

<p><strong>Mệnh VCD được Thái Dương, Thái Âm miếu, vượng chiếu.</strong> Căn nhà không nóc sẽ qua khỏi tai họa nếu bầu trời bên trên là một bầu trời xanh tươi đẹp đẽ, không mây không nắng.
Mệnh VCD an tại Mùi có Thái Dương ở Mão (Mặt trời lúc bình minh) và Thái Âm ở Hợi (mặt trăng vằng vặc lúc nửa đêm, cả hai cùng hợp chiếu về Mệnh. Đây là cách "Nhật Nguyệt Tịnh Minh tá cửu trùng ư kim diện".
Hiểu đơn giản, người Mệnh VCD tại Mùi sẽ dễ được phò tá vua chúa, cùng hưởng vinh hiển.</p>

<center><img src="../upload/img/post/menh-vo-chinh-dieu-nha-khong-noc-3.jpg" width="100%" alt="Mệnh Vô Chính Diệu" /><br><br>

<p>Nhà không nóc giữa thời tiết đẹp.<br>
Đây là cách Mệnh VCD được Thái Dương, Thái Âm miếu, vượng chiếu.</p></center>

<h3>Phụ Mẫu Vô Chính Diệu</h3>

<p>Không nhờ cậy được ba mẹ, có thể do ly hôn, ly dị, xa cách ba mẹ... Nếu có Tuần Triệt thì có thể ở gần ba mẹ hơn. Khả năng cao phải xa quê lập nghiệp.</p>

<h3>Huynh Đệ Vô Chính Diệu</h3>

<p>Khó nhờ cậy anh em, anh em mỗi người một nơi, anh em ly tán. Nếu có Tuần Triệt thì tiền vận của anh em sẽ vất vả, hậu vận khá hơn.</p>

<h3>Phúc Đức Vô Chính Diệu</h3>

<p>Phúc Đức vừa là dòng họ, vừa là tinh thần. Phúc Đức VCD thì tinh thần dễ bất ổn, dễ bị lay chuyển. Cũng có thể đương số xa dòng họ, xa cách họ hàng người thân.</p>

<p>Vô Chính Diệu đắc Tam không thì được hưởng phúc lộc dồi dào hơn.</p>

<h3>Điền Trạch Vô Chính Diệu</h3>

<p>Điền Trạch là chỗ làm, chỗ ở, tài sản có được. Điền Trạch VCD thể hiện nơi ở không cố định, chỗ làm không ở một nơi, tài sản khó giữ.</p>

<p>Những người Điền Trạch VCD nên làm nghề buôn bán BĐS, nghề di chuyển, thay đổi nơi ở, cầm đồ.</p>

<p>Nếu có Tuần Triệt thì về sau mới thuận lợi, giữ được điền sản.</p>

<h3>Phu Thê Vô Chính Diệu</h3>

<p>Phu Thê VCD thường chậm lập gia đình, khó nhờ cậy vợ/chồng. Vợ/chồng có thể là bất kỳ ai, muôn hình vạn trạng, giống như những cô dâu được gả cho chồng nơi xứ người, không ai biết chồng mình sau này thế nào.</p>

<p>Nếu có Tuần Triệt thì hậu vận tốt hơn. Nếu nhiều phúc tinh thì vẫn có khả năng vợ/chồng sống hạnh phúc. Nhiều hung tinh thì dễ chia ly.</p>

<p>Trường hợp Phu Thê VCD có thể dùng cung Phúc, Di hay Quan Lộc để đánh giá. Hoặc đương số cũng có thể dựa vào những mối quan hệ trong thực tế để tìm hiểu.
Vì suy cho cùng, Phu Thê VCD, người hôn phối có thể là bất kỳ loại người nào, miễn là đương số tìm hiểu kỹ và cẩn thận trong tình duyên.</p>

<h3>Tử Tức Vô Chính Diệu</h3>

<p>Tử Tức VCD thì hiếm con, chậm con. Nếu có Tuần Triệt càng dễ chậm con, con khó nuôi.</p>

<p>Tuy nhiên, một số trường hợp Tử Tức VCD vẫn có 2 con trở lên. Có thể do các phụ tinh như Thiên Không, Đào Hoa giúp tăng số lượng.</p>

<h3>Quan Lộc Vô Chính Diệu</h3>

<p>Quan Lộc VCD thể hiện đương số nhiều khả năng sẽ phải thay đổi công việc, làm ít nhất 2 nghề trong đời. Đương số là người tháo vát, đa năng đa tài mới có thể học nhiều nghề như vậy.</p>

<p>Nhưng cũng chính vì phải thay đổi công việc nên sẽ không được người đời đánh giá cao. Ví dụ: một người ngoài 30 đổi sang một nghề mới. Chắc chắn người đó sẽ khó khăn khi xin việc, cũng như học kiến thức mới. Phải trải qua thời gian dài mới được công nhận.</p>

<p>Nếu Tài Bạch tốt, có Tả Hữu giúp đỡ cùng nhiều phúc tinh khác, những người Quan Lộc VCD dễ trở thành lãnh đạo giỏi vì họ đã từng trải rất nhiều khi làm nhiều nghề.</p>

<center><img src="../upload/img/post/quan-loc-vo-chinh-dieu-1.jpg" width="100%" alt="Quan Lộc Vô Chính Diệu" /><br><br></center>

<h3>Tài Bạch Vô Chính Diệu</h3>

<p>Tiền tài lúc có lúc không. Đương số sẽ có lúc phát đạt, cũng sẽ có lúc đi xuống, tụt dốc tùy giai đoạn.</p>

<p>Tài Bạch VCD thể hiện đương số có thể kiếm tiền bằng nhiều nghề, nhiều cách khác nhau. Một số người có thể giàu ngầm, nhờ người khác đứng tên hoặc chuyển hết tiền thành tài sản.</p>

<h3>Nô Bộc Vô Chính Diệu</h3>

<p>Bạn bè, mối quan hệ bên ngoài có thể là bất kỳ loại người nào. Giống như dân kinh doanh, sale những sản phẩm ai cũng sử dụng vậy.</p>

<p>Người có Nô Bộc VCD dễ làm những nghề giao tiếp xã hội, kinh doanh, có thể tự do chọn cho mình sản phẩm để mình bán. Trong khi những người Nô Bộc có chính tinh, họ thường xuyên gặp một loại khách hàng tương tự nhau.</p>

<p>Nhưng cũng vì gặp nhiều loại người nên khó nhờ cậy được bạn bè. Bạn bè dễ xảy ra mâu thuẫn với nhau. Ví dụ: người có học và người thất học, đương số có thể quen được cả đôi bên. Nhưng nếu họ gặp nhau thì chắc chắn không hợp. Vì vậy cần</p>

<h3>Thiên Di Vô Chính Diệu</h3>

<p>Thiên Di VCD thể hiện bạn là người di chuyển, nhưng điểm đến, đích đến của bạn lại không rõ ràng, do người khác quyết định.</p>

<p>Giống như tài xế taxi, họ đi rất nhiều. Nhưng địa điểm họ tới lại thuộc về quyết định của hành khách. Do đó, rất khó để người có Thiên Di VCD quyết định những việc liên quan đến đi xa, làm ăn xa xứ, đi nước ngoài.</p>

<center><img src="../upload/img/post/taxi-1.jpg" width="100%" alt="Thiên Di Vô Chính Diệu" /><br><br>

<p>Thiên Di VCD, đi nhiều nhưng điểm đến do người khác quyết định.</p></center>

<p>Bù lại, những người Thiên Di VCD sẽ hợp với nghề tài xế, vận chuyển. Nếu Mệnh có những sao thiên về máy móc như Hỏa Tinh, Linh Tinh thì càng chính xác.</p>

<p>Thiên Di VCD cũng ám chỉ mục tiêu thường dễ bị thay đổi, lung lay bởi ngoại cảnh. Bạn muốn đi Hà Nội, nhưng sau đó lại đổi sang Đà Nẵng. Ngoại cảnh sẽ lấn át đương số, khiến họ khó đạt mục tiêu đề ra.</p>

<h3>Tật Ách Vô Chính Diệu</h3>

<p>Tật Ách VCD là đẹp nhất, không có bệnh nặng, nhưng khi bệnh thì khó tránh. Đặc biệt, bệnh có thể xuất hiện ở nhiều nơi, không rõ ràng ở địa điểm nào cả. Hiểu đơn giản: số lượng nhiều, nhưng bệnh không nặng.</p>
',
            'date_post'         => '2023-01-11',
            'thumbnail_post'    => 'vo-chinh-dieu-12-cung-thumbnail.jpg',
            'id_cat_post'       => CACH_CUC_ID_CAT,
            'views'             => random_int(50,500),
            'enable_post'       => ENABLE,
        ]); // https://tuvisonlong.com/giai-ma-12-cung-vo-chinh-dieu-tren-la-so-tu-vi/#ftoc-heading-9
// https://hocvienlyso.org/luan-ve-menh-vo-chinh-dieu.html

        posts::create([
            'name_vi_post'    => 'Làm sao để trở thành thầy Tử Vi, Phong Thủy',
            'url_post'        => 'lam-sao-de-tro-thanh-thay-tu-vi-phong-thuy',
            'present_vi_post' => '',
            'content_vi_post' => '<p><strong>Lưu ý:</strong> bài viết này không bao gồm các thầy lừa đảo hay nhằm mục đích hướng dẫn làm việc phạm pháp. Mục đích của bài viết để giúp mọi người đánh giá xem thầy nào tốt, thầy nào không tốt để có cái nhìn chính xác hơn.</p>

<center><iframe width="100%" height="350" src="https://www.youtube.com/embed/FxL2mQSTx4w" title="thao túng tâm lý của thầy bói" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

<p>Phóng sự của VTV</p></center>

<p>Nếu đã xem phóng sự của VTV, ai cũng có thể thấy những chiêu thức gọi hồn, dự đoán được thực hiện rất có bài bản. Vậy phải chăng các thầy bói, Tử Vi, Phong Thủy đều sử dụng những chiêu thức đó, hay họ còn những cách nào khác?
Bài viết này sẽ giúp các bạn hiểu hơn về nghề làm thầy bói, Tử Vi, Phong Thủy. Tất nhiên, đây là những hướng dẫn để làm một người hành sự đúng mực, có đạo đức, không phải để đi lừa đảo.</p>

<h3>Có kiến thức Nhân Tướng Học, Tử Vi, Phong Thủy...</h3>

<p>Trong bất kỳ môn luận đoán, dự báo nào, đều cần phải có 2 thứ: dữ kiện và phương pháp.</p>

<p><b>Dự báo thời tiết:</b> dữ kiện là tình hình thời tiết hiện nay, quá khứ, quy luật... Phương pháp là đo đạc, tính toán.</p>

<p><b>Dự báo kinh tế:</b> dữ kiện là tình hình kinh tế hiện nay, lịch sử, quy luật... Phương pháp là tính toán các thông số, tình hình chính trị...</p>

<p><b>Cá độ bóng đá:</b> dữ kiện là tình hình các đội hiện nay, lịch sử đối đầu... Phương pháp là tra cứu thông tin...</p>

<p>Vậy làm thầy Tử Vi, Phong Thủy cũng thế. Đầu tiên, các thầy sẽ thu thập dữ kiện: ngày giờ sinh, cách ăn nói, trang phục... Phương pháp là những câu hỏi để người ta phải trả lời đúng yêu cầu, đúng ý mình.</p>

<p>Thậm chí, một số người chỉ cần nhìn mặt, biểu hiện tay chân, nghe giọng là có thể phán chính xác. Thật ra là họ đang sử dụng Nhân Tướng Học, và bộ môn này hoàn toàn không hề mê tín. Những người quản lý nhân sự giỏi sẽ có khả năng này qua sách vở, từng trải.</p>

<h3>Dự đoán quá khứ</h3>

<p>Từ những thông tin mà thầy Tử Vi, Phong Thủy thu thập được thông qua Nhân Tướng Học, lá số, quẻ dịch... Họ sẽ bắt đầu luận đoán về quá khứ, tính cách của bạn. Những luận đoán này được chia thành các loại sau:</p>

<p><strong>Những dự đoán tỷ lệ đúng 99%: giống như VTV đưa tin, các thầy sẽ đoán những câu hỏi có tỉ lệ trúng lên đến 99% như:</strong></p>

<ul>
<li>"Trong nhà đi ra có 1 cái ngã 3 đường"
<li>"Tiếp là có ngã 4 đường"
</ul>

<p><strong>Những dự đoán tỷ lệ đúng 80%: bắt đầu giảm tỉ lệ trúng xuống, thầy sẽ đoán những câu như:</strong></p>

<ul>
<li>Gần nhà có biển, sông, kênh, ao hồ
<li>Nhà mình, hoặc nhà hàng xóm, khu mình sống có nuôi động vật, thú cưng phải không?
</ul>

<p>Tỉ lệ trúng của những câu này chắc chắn thấp hơn, vì không phải ai cũng nuôi chó mèo, sống ở khu nuôi chó mèo hay nhà gần biển, sông, kênh, ao hồ. Vì vậy, trước khi hỏi những câu này, các thầy cần thu thập thông tin trước.</p>

<p>Các thầy sẽ hỏi địa chỉ, quê quán trước. Nếu địa chỉ, quê quán là nơi có sông ngòi lớn chảy qua (quận Hoàng Kiếm), hoặc sống ở các thành phố biển (Đà Nẵng, Cà Mau),
họ sẽ đoán gần nhà bạn có biển, sông, kênh, ao hồ. Tất nhiên, tỷ lệ trúng lúc này rất cao vì quận Hoàng Kiếm có hồ Hoàng Kiếm và sông Hồng chảy qua.
Đà Nẵng có cả vùng biển rộng lớn, sông ngòi với các cây cầu nổi tiếng. Khả năng trúng lên đến 80% hoặc hơn.</p>

<center><img src="../upload/img/post/quan-hoan-kiem-ha-noi.jpg" width="100%" alt="Thiên Di Vô Chính Diệu" /><br><br>

<p>Quận Hoàng Kiếm có hồ Hoàng Kiếm và sông Hồng chảy qua</p></center>

<p><strong>Những dự đoán tỷ lệ đúng 50%: tiếp tục giảm tỉ lệ trúng xuống, thầy sẽ đoán những câu như:</strong></p>

<ul>
<li>Bạn là người hay suy tư, lo âu.
<li>Bạn được sống trong môi trường gia đình yêu thương, chăm sóc tử tế.
</ul>

<p>Những câu hỏi này được dựa vào khuôn mặt, hành vi, cử chỉ, hay còn gọi là Nhân Tướng Học. Cũng có thể dựa vào sự từng trải của các thầy, do họ đã từng trải nên nhìn người là phán chính xác cao.</p>

<p></p>


<h3>Dự đoán tương lai</h3>

<p>Sau khi các thầy dự đoán quá khứ, họ sẽ dự đoán tương lai. Các thầy sẽ dự đoán tương lai dựa vào:</p>

<ul>
<li>Những thông tin họ đã thu thập từ bạn.
<li>Tính cách, trình độ của bạn.
<li>Kinh nghiệm cuộc sống của họ (những trường hợp tương tự như bạn).
</ul>

<p></p>


',
            'date_post'         => '2023-01-12',
            'thumbnail_post'    => 'lam-sao-de-tro-thanh-thay-tu-vi-phong-thuy-thumbnail.jpg',
            'id_cat_post'       => CACH_CUC_ID_CAT,
            'views'             => random_int(50,500),
            'enable_post'       => ENABLE,
        ]);

        posts::create([
            'name_vi_post'    => 'Tại sao gái đẹp lại thích đại gia?',
            'url_post'        => 'tai-sao-gai-dep-lai-thich-dai-gia',
            'present_vi_post' => 'Bài viết xét theo góc nhìn của Tử Vi.',
            'content_vi_post' => '<p>Gái đẹp và đại gia, cặp đôi hoàn hảo từ ngàn đời. Thuở xa xưa, đàn ông được phép cưới nhiều vợ. Nhưng chỉ có các phú ông, đại gia, vua chúa mới có năm thê bảy thiếp chứ chẳng bao giờ thấy anh nông dân nghèo có thê thiếp.</p>

<p></p>

<p></p>

<center>

<p></p></center>


<h3>Nữ Tham Lang là đẹp nhất</h3>

<p></p>

<p></p>

<h3>Tham Lang và Thiên Phủ</h3>

<p>Sao Tham Lang luôn cách sao Vũ Khúc 2 ô trên lá số Tử Vi.</p>

<p>Mệnh Tham Lang thì cung Phu Thê phải có Thiên Phủ. Tham Lang tại Dần Thân thì Phu Thê có Vũ Phủ, chỉ yêu những người thành công, nhiều tiền, dân kinh doanh. Đây là lý do chính khiến gái đẹp thích đại gia.</p>

<p>Ngược lại, cung Phu Thê có Tham Lang thì Tài Bạch sẽ có Thiên Phủ. Cung Tài Bạch có Thiên Phủ là người có thể tích tụ tài sản, đa số đều giàu có, sung túc, trừ khi gặp sát tinh quá nhiều. Nghĩa là đương số giàu có, kiếm được vợ/chồng xinh đẹp.</p>

<p>Mệnh Tham quan có Đào hồng có khiếu làm nghệ thuật. Đào hoa cư Quan, thì khả năng cao quen người yêu trong nghề nghiệp. Nếu quen người cùng nghề, thành công, nhiều tiền, dân kinh doanh thì khả năng nữ Tham Lang này làm những nghề được tiếp xúc nhiều đại gia, ông lớn như sale.</p>

<p>Tham Lang tại Dần hoặc Thân, cung Phu Thê sẽ là Vũ Khúc Thiên Phủ tại Tý và Ngọ, nhị Hợp với Thái Âm Thái Dương. Chồng vừa có tiền tài, vừa có đất đai, lại công danh sự nghiệp phát đạt, phong độ, khả năng cao là một đại gia giàu có.</p>

<h3>Đào Hoa tại cung nào?</h3>

<p>Đào Hoa luôn nằm tại cung Tý, Ngọ, Mão, Dậu.</p>

<p>Như đã nói ở trên, Tham Lang tại Dần hoặc Thân, cung Phu Thê sẽ là Vũ Khúc Thiên Phủ tại Tý và Ngọ. Hơn nữa, Tý và Ngọ lại là nơi Đào Hoa có thể đóng. Nghĩa là những cô gái đẹp này có thể quen nhiều anh đại gia, hoặc chồng cô ấy cũng là một người đào hoa, quen nhiều cô gái.</p>


',
            'date_post'         => '2023-01-13',
            'thumbnail_post'    => 'tai-sao-gai-dep-lai-thich-dai-gia-thumbnail.jpg',
            'id_cat_post'       => CACH_CUC_ID_CAT,
            'views'             => random_int(50,500),
            'enable_post'       => ENABLE,
        ]);
        // Tiếc là mệnh thân dính Hồng Không Cô Quả lắm mối tối năm không. Tham gặp Hồng không cô quả tại mệnh cũng biểu thị người có thể mộ đạo, dễ đi vào kiếp tu hành.
        // Nhưng bù lại Tham ở đây có Linh Hoả hội về. Quan có Lộc tồn thì làm gì cũng ra tiền, không thiếu thốn đc.

        posts::create([
            'name_vi_post'    => 'Bạn có được ở đúng môi trường?',
            'url_post'        => 'ban-co-duoc-o-dung-moi-truong',
            'present_vi_post' => 'Đã bao giờ bạn tự hỏi: môi trường này có phù hợp với mình hay không?',
            'content_vi_post' => '<p>Đã bao giờ bạn cảm giác rằng: mình không phù hợp với nơi mình sống, nơi mình học, nơi mình làm việc?
Đôi khi chúng ta khiến những người xung quanh mất đi thiện cảm và không muốn tiếp xúc nhiều với mình. Mặc dù chúng ta hành xử bình thường, thậm chí không làm gì, cũng có thể bị ghét.
Ví dụ: một đứa trẻ ngồi chơi game. Đứa trẻ đó không ảnh hưởng đến ai, nhưng ai nhìn vào cũng thấy không thiện cảm.</p>

<p></p>

<center><img src="../upload/img/post/khong-hoa-hop-voi-moi-truong-xung-quanh-1.jpg" width="100%" alt="không hòa hợp với môi trường xung quanh" /><br><br>

<p>Bạn không hòa hợp với môi trường xung quanh?</p></center>

<h3>Cách xem bản thân bạn.</h3>

<p>Bạn là người sôi nổi, hoạt bát hay ít nói?</p>
<p>Bạn là người có học thức cao hay thích làm điều mình muốn?</p>
<p>Bạn là một giang hồ, một đứa quậy phá hay là con ngoan trò giỏi?</p>

<h3>Cách xem môi trường sống của bạn.</h3>

<p>Bản Mệnh: đương số có Tả Phù Hữu Bật hỗ trợ hay có sát tinh bị kẻ xấu cản trở.</p>
<p>Điền Trạch: nơi bạn sống, nơi làm việc.</p>
<p>Nô Bộc: bạn bè, các mối quan hệ bạn gặp ngoài xã hội.</p>
<p>Quan Lộc: nghề nghiệp.</p>
<p>Thiên Di: nơi bạn đi tới, đi xa.</p>

<h3>Bản thân bạn có phù hợp với môi trường sống không?</h3>

<p>Bạn là một người ít học thức, nhưng lại sống trong môi trường xung quanh toàn những người học cao. Bạn và mọi người sẽ cảm thấy không phù hợp, khó nói chuyện, khó làm việc.</p>

<p>Bạn là một người tốt, hiền lành, nhưng lại bị buộc phải sống trong môi trường của những kẻ giang hồ, dân chợ búa.</p>

<p>Việc hòa hợp với môi trường xung quanh rất quan trọng. Những lá số có Tả Phù Hữu Bật, cung Nô Bộc, Quan Lộc tốt, chứng tỏ đương số rất thuận lợi khi làm việc, sự nghiệp thăng tiến.
Ngược lại, có những người bản thân rất tài năng, nhưng Nô Bộc, Quan Lộc luôn bị cản trở, khó khăn như Phục Binh, Không Kiếp, Linh Hỏa, Kình Đà.</p>

',
            'date_post'         => '2023-01-14',
            'thumbnail_post'    => 'ban-co-duoc-o-dung-moi-truong-thumbnail.jpg',
            'id_cat_post'       => CACH_CUC_ID_CAT,
            'views'             => random_int(50,500),
            'enable_post'       => ENABLE,
        ]);

        posts::create([
            'name_vi_post'    => 'Ý nghĩa sao Bệnh Phù',
            'url_post'        => 'y-nghia-sao-benh-phu',
            'present_vi_post' => 'Mẫu người có sao Bệnh Phù là mẫu người như thế nào?',
            'content_vi_post' => '


<h3>sao Bệnh Phù là gì?</h3>

<p>Hành: Thổ</p>

<p>Đắc:</p>

<p>Hãm:</p>

<center><img src="../upload/img/post/benh-phu-1.jpg" width="100%" alt="Bệnh Phù" /><br><br></center>

<p>Sao Bệnh Phù nói về tình trạng sức khỏe yếu của cơ thể. Cơ thể yếu ớt khi mới sinh, khó làm việc nặng. Tuy nhiên, một số trường hợp những người có sao Bệnh Phù vẫn làm những công việc nặng, đi lại nhiều, thậm chí kiếm được số tiền chục tỷ. Cần phải xem Tật Ách hay các cung khác.</p>

<p>Bệnh Phù còn có ý nghĩa là bảo thủ, cố chấp, không chịu lắng nghe.</p>

<p>Bệnh Phù là sự ít quan tâm, chăm sóc bản thân hay mọi người xung quanh. Bệnh Phù ở đâu, đương số sẽ ít quan tâm đến cung đó.</p>

<p></p>

<p><b>Bệnh Phù, Đại Hao/Tiểu Hao, Thất Sát, Thiên Hình, Hóa Kỵ:</b> Dễ nặng, cần phải mổ.</p>

<p><b>Bệnh Phù, Đào Hoa, Thiêu Diêu:</b> Dễ mắc bệnh liên quan đến sinh lý, tình dục.</p>

<h3>Bệnh Phù đóng tại các cung</h3>

<p><b>Bệnh Phù tại Mệnh: </b></p>

<p><b>Bệnh Phù tại Phụ Mẫu: </b> Ba mẹ hay bệnh tật. Ba mẹ ít quan tâm con cái, bảo thủ.</p>



<p><b>Bệnh Phù tại Điền: </b></p>
',
            'date_post'         => '2023-01-15',
            'thumbnail_post'    => 'y-nghia-sao-benh-phu-thumbnail.jpg',
            'id_cat_post'       => SAO_ID_CAT,
            'views'             => random_int(50,500),
            'enable_post'       => ENABLE,
        ]);

        posts::create([
            'name_vi_post'    => 'Lá số hình tù, tù tội',
            'url_post'        => 'la-so-hinh-tu-tu-toi',
            'present_vi_post' => 'Người như thế nào có khả năng chịu tù tội?',
            'content_vi_post' => '

<h3>Tù tội vì lý do gì?</h3>

<p>Có 2 loại tù tội: cố ý và vô ý.</p>

<p>Cố ý là </p>

<p>Vô ý là những vụ việc như gây tai nạn giao thông, gây tai nạn, gây thiệt hại về tài sản nhưng không cố ý tạo ra.</p>



<p><b>Thai - Phục - Vượng - Tướng + (Phá Quân - Thiên Diêu)</b></p>

https://www.youtube.com/watch?v=JyIY6W4J4UI
',
            'date_post'         => '2023-01-15',
            'thumbnail_post'    => 'la-so-hinh-tu-tu-toi-thumbnail.jpg',
            'id_cat_post'       => SAO_ID_CAT,
            'views'             => random_int(50,500),
            'enable_post'       => ENABLE,
        ]);

        posts::create([
            'name_vi_post'    => 'Thế nào mới là Phu Thê?',
            'url_post'        => 'the-nao-moi-la-phu-the',
            'present_vi_post' => 'Vợ chồng là phải như thế nào?',
            'content_vi_post' => '

<h3>Quan niệm ngày xưa</h3>

<p>Ngày xưa, "trai năm thê bảy thiếp, gái chính chuyên một chồng". Câu này thể hiện phần lớn mối quan hệ ngày xưa: đàn ông có thể lấy nhiều vợ, nhưng phụ nữ chỉ được phép lấy một chồng.</p>

<p></p>

<p></p>

<p></p>

<h3>Quan niệm ngày nay</h3>

<p>Ngày nay, trai gái đều có thể tìm nhiều đối tượng khác và quan hệ từ rất sớm.</p>

<p></p>

<p>Nhiều người cho rằng, chỉ có ngày nay phụ nữ mới quan hệ với nhiều người. Thật ra ngày xưa phụ nữ cũng đã quan hệ nhiều người, nhưng do bị trừng phạt khắc nghiệt hay bị giấu đi nên mọi người không phát hiện ra.
Mọi người hãy nhớ rằng: <b>khi bản năng đã trỗi dậy, chỉ trong một khoảnh khắc, tất cả đạo đức đều biến mất hết.</b> Vì vậy, đừng nghĩ chỉ có ngày nay mới quan hệ nhiều người.</p>

<p></p>

<p></p>



',
            'date_post'         => '2023-01-15',
            'thumbnail_post'    => 'the-nao-moi-la-phu-the-thumbnail.jpg',
            'id_cat_post'       => SAO_ID_CAT,
            'views'             => random_int(50,500),
            'enable_post'       => ENABLE,
        ]);

        posts::create([
            'name_vi_post'    => 'Sao Hóa Khoa',
            'url_post'        => 'hoa-khoa-12-cung',
            'present_vi_post' => 'Sao Hóa Khoa của 12 cung sẽ như thế nào?',
            'content_vi_post' => '<p></p>

<p></p>

<p></p>


// Mệnh có Hóa Khoa: có thể hóa giải rắc rối. Người Mệnh Hóa Khoa có thể giải quyết vấn đề. Chỉ cần giao một vấn đề nào đó, máy móc hư hỏng, người Hóa Khoa có thể giải quyết.
// Tử Phù biểu thị cho sự suy yếu và giảm sút.
// Mệnh có Tử Phù: hay gặp rắc rối. Người có trí tuệ, nhiều tài năng, nhanh nhẹn, hoạt bát, năng nổ, nhiệt tình nhưng thường gặp nhiều trắc trở, vấp ngã và hay rơi vào những trường hợp éo le.


',
            'date_post'         => '2023-01-16',
            'thumbnail_post'    => 'hoa-khoa-12-cung-thumbnail.jpg',
            'id_cat_post'       => SAO_ID_CAT,
            'views'             => random_int(50,500),
            'enable_post'       => ENABLE,
        ]);

        posts::create([
            'name_vi_post'    => 'Sao Tử Phù',
            'url_post'        => 'tu-phu-12-cung',
            'present_vi_post' => 'Sao Tử Phù của 12 cung sẽ như thế nào?',
            'content_vi_post' => '<p></p>

<p>Tử Phù biểu thị cho sự suy yếu và giảm sút.</p>

<p>Nguyệt Đức luôn đi cùng Tử Phù, mang ý nghĩa khi bạn liên tục gặp rắc rối, khó khăn, bạn sẽ học được cách điềm tĩnh hơn. Một số người có thể đi tu hoặc sống an phận để thoát khỏi rắc rối, gian khổ, sống bình lặng.</p>

<h3>Một số cách cục</h3>

<ul>
<li>Tử Phù đồng cung hoặc hội chiếu với Đào Hoa: nhiều mối tình nhưng mối tình nào cũng trắc trở. Phải vượt qua những trắc trở đó mới đến được với nhau.
<li>Tử Phủ đồng cung hoặc hội chiếu với Thiên Lương: là người lương thiện, nghiên cứu tâm linh, có duyên với tôn giáo, nơi tu hành.
</ul>



// Mệnh có Hóa Khoa: có thể hóa giải rắc rối. Người Mệnh Hóa Khoa có thể giải quyết vấn đề. Chỉ cần giao một vấn đề nào đó, máy móc hư hỏng, người Hóa Khoa có thể giải quyết.
// Mệnh có Tử Phù: hay gặp rắc rối. Người có trí tuệ, nhiều tài năng, nhanh nhẹn, hoạt bát, năng nổ, nhiệt tình nhưng thường gặp nhiều trắc trở, vấp ngã và hay rơi vào những trường hợp éo le.


',
            'date_post'         => '2023-01-17',
            'thumbnail_post'    => 'tu-phu-12-cung-thumbnail.jpg',
            'id_cat_post'       => SAO_ID_CAT,
            'views'             => random_int(50,500),
            'enable_post'       => ENABLE,
        ]);
    }
}








// cách hiếm muộn con cái trong lá số.(Không viết cách hoá giải)

//   Cung Tử Tức trong lá số biểu tượng cho con cái. Nó thể hiện tất cả sự liên quan giữa con cái và cha mẹ. Nhìn cung Tử Tức biết được có con hay không. Hiếm muộn hay không. Nguyên do,sự tương tác giữa mẹ - con & cha - con. Hiếm muộn ở đây gồm 2 ý chính. Có hoặc không. Thế nào là hiếm muộn. Định nghĩa đơn giản đó là tuổi đã nhiều mới có con hoặc già vẫn không có. Vậy những sao nào thể hiện điều đó ngay tại cung Tử Tức?

// 1. Vũ Khúc chủ về ít con(khó quá 2 con) cũng thể hiện sự khó nuôi,có có con.Bản chất Vũ Khúc là sự đơn độc,keo kiệt nên yếu tố thể hiện rõ nhất.

// 2. Phá Quân chủ hao tán,ý nghĩa rằng cha mẹ và con ít "duyên",  cũng thể hiện cho khó hoà hợp âm dương mẹ cha,khó thụ thai.

// 3. Thái âm (Dương) hãm địa ngộ Hoá Kỵ.Thể hiện âm dương khí cha mẹ kém (tinh trùng&trứng yếu).

// 4. Thiên Cơ Mộc Dục thể hiện sự muộn con,sinh con thiếu tháng,tràng rau quấn cổ,sinh non.

// 5. Bạch Hổ,Hoa Cái ngộ sát tinh. Bạch Hổ chủ máu huyết, Hoa cái chủ sinh nở nên dễ sảy, phá thai mà cơ địa "mẫn cảm" khó giữ bào thai.

// 6. Thai ngộ Không, Kiếp, Kỵ, Hình: Thai chủ sự nuôi dưỡng trong bụng, Không Kiếp chỉ sự dị dạng, bệnh lạ, Hoá Kỵ nảy sinh âm hư, Thiên Hình mổ sẻ...bào thai khó sống.

// 7. Kình Dương Song Hao sát tinh: Song Hao chủ hao tán, muộn mằn. Ngộ Kình Dương sát tinh ắt gây rất muộn & không con.

// 8. Thất Sát, Đầu Quân, Song Hao, Cô thần, quả tú: Thất Sát chủ nặng nghiệp, ít duyên thêm song hao càng làm tăng ý nghĩa hiếm muộn. Thêm đẩu quân do gánh nghiệp mà tạo "hiện kiếp" vô phương cứu chữa.

// 9.  Thiên Hình-Đẩu Quân chủ kiếp nghiệp chậm muộn con.

// ***Song hao vốn thuỷ,cơ thể chỉ hệ tiết niệu,biểu hiện cụ thể khi ngộ sát tinh thì đàn ông thận hư,đàn bà khí hư,chu kỳ không đều,buồng trứng kém.

//  Riêng "Đẩu Quân" tối kỵ cung Tử Tức.Vì sao????Kiến thức này sẽ rất rất lâu nữa không mấy ai biết.Và có lẽ không nên biết vẫn hơn.
//  Các yếu tố liên quan cung Tật Ách,Tử Tức....để luận đàm con cái có lẽ cũng không nên bàn đến.Sách vở có nhiều.

//  Cách thức và phương pháp chữa hiếm muộn luôn đặt Khoa Học máy móc,Y học hiện đại lên hàng đầu.Sau đó mới là tử vi,tâm linh vi diệu.
//  Lê Quang Lăng 5.2018





// Cung Phúc Hóa Kỵ: khó nhàn thân, lúc nào cũng day dứt, đắn đo khi chưa xong việc.
// Không cảm thấy an toàn trong vấn đề tài chính, không đầu tư chứng khoán được.
// Không vui hoàn toàn. Dù hp nhất thì trong đầu vẫn lo toan. Cảm xúc dễ bị tụt mood.
// Nghe nhiều hơn. Ngại nói vì nghĩ mọi người xung quanh không đủ kiến thức hay sự thấu hiểu để có thể hiểu những lời mình nói
// Đau bao tử do stress





// Vẻ đẹp của Chính Tinh (Đ) ko bị sát, bại tinh đánh phá:
// - Tử Vi : Đẹp Lạnh Lùng.
// - Thiên Phủ: Đẹp phúc hậu, thướt tha
// - Thái Âm: Đẹp quý phái, sang trọng
// - Tham Lang: Đẹp quyến rũ, thu hút, cuốn..
// - Cự Môn: Đẹp cân đối, nụ cười duyên dáng
// - Thiên Tướng: Đẹp uy nghiêm, dễ mến, chân thành
// - Liêm Trinh: Đẹp sắc bén, lộng lẫy, model
// - Thiên Lương: Đẹp Lão
// - Thất Sát: Đẹp nhỏ nhắn, xinh xắn, cute
// - Thiên Đồng: Đẹp tròn trịa, tinh anh
// - Vũ Khúc: Đẹp quý phái, thanh cao
// - Thái Dương: Đẹp cá tính, khôi ngô
// - Phá Quân: Đẹp tự nhiên
// - Thiên Cơ: Đẹp thon thả, duyên dáng
// * Nếu hội Tam Minh Đào - Hồng - Hỷ thì sẽ tăng vẻ đẹp và trẻ lâu.

// Nghiệm lý về Sao Hoá Khoa:
// Nói về tứ hoá, Hoá Lộc gieo duyên, Hoá Quyền củng cố, Hoá Khoa khai thác, Hoá Kị Đào thải
// - Hoá khoa chỉ việc học còn có rất nhiều thứ để học, học ở trường lớp sẽ là 1 phần, học hỏi từ bên ngoài là
// - Từ Hoá vốn là biến đổi đây dễ hiểu là biến đổi kiến thức người khác thành kiến thức của mình, phải dựa trên cơ sở lý thuyết nào đó
// - Hoá khoa đóng đâu thì học hỏi ở đó
// - Có xu hướng muốn học nhanh càng nhanh càng không chuyên sâu
// - Giỏi che đậy và có phong thái tri thức
// - Hoá khoa làm việc mang tính chất chuyên môn cao, thích tạo khuôn mẫu
// - Hoá khoa là sự sắp xếp hợp lý khoa học, dễ bị ám ảnh cưỡng chế liên quan đến sự đồng đều ngăn nắp
// - Hoá khoa hơi dài dòng văn tự loằng ngằng mì tôm, đủ trình tự mới thôi, hơi khắt khe
// - Hoá khoa là người khá chủ quan về kiến thức của mình, đóng đâu chủ quan ở đó khá sợ đóng tật ách
// - Hoá khoa ở mệnh thân số làm thầy, sẽ bị đặt trong cái thế phải hướng dẫn cho người khác
// - Hoá khoa làngười học có thể chậm hiểu nhưng cách họ truyền đạt lại cho người khác thì rất dễ hiểu
// - Hoá Khoa cứu giải bằng cách khai thác thông tin vấn đề cốt lõi từ đó đưa ra cách giải hợp lý nhất
// - Khoá không nên gặp Riêu Đà Kị giống như người học man thư vậy
// - Khoa gặp Không kiếp dễ đi vào con đường không chính đạo
// Tử Vi Nam Việt



// Sự tương tác của Thiên Không
// Tính sơ sơ 1 cuộc đời con người trung bình khoảng 70t, ít nhất kiểu gì cũng đối mặt với Thiên Không ít nhất là 1 lần. Người nào nhẹ thì ăn tam hợp, người nào nặng thì đi qua trực tiếp. Mình xin chia sẻ một số kiến thức ít ỏi về đại vận, tiểu hạn Thiên Không qua góc nhìn của Nam Phái:
// 1. Thiên không phá tài - Điều này thường xảy ra với bộ chính tinh Sát Phá Tham hoặc Sát Phá Liêm Tham. Những chính tinh mang tính chất bạo động cao, thành bại chỉ trong chớp mắt, tính liều lĩnh ăn thua lớn, khi vào vận gặp Thiên Không rất dễ rơi vào trạng thái tay trắng. Thiên Không làm họ giống con bạc khát nước, tất tay và tất tay, rồi đòm một phát là trắng tay. Đây là bộ sao cần nhất Thiên Không đến sớm, để còn có hi vọng đông sơn tái khởi. Thiên Không đến tầm khoảng 5 xịch thì cái nịt cũng không còn. Sau SPT là sự tiếp bước của Phủ Tướng- Phủ phùng không sứ tài suy. Mặc dù thận trọng hơn SPT nhưng cũng chạy trời không khỏi nắng, bao nhiêu dự tính đổ ụp trong phút chót, hoặc dở dang vì những lí do hết sức....trời ơi đất hỡi
// 2. Thiên Không thành công. Như chúng ta đều biết trong 4 vị trí quyền lực nhất của tử vi thì Nhật Nguyệt chiếm 1 nửa. Nhật Nguyệt  ngoài tượng trưng cho họ nội ngoại, cho cha mẹ, còn là thời thế, đôi mắt và trí tuệ. Nên Nhật  Nguyệt cực kì thích Thiên Không. Nếu dùng tượng của Nam Phái thì Thiên Không là bầu trời rộng lớn cho Nhật Nguyệt thỏa sức vẫy vùng. Nhật Nguyệt sáng gặp Thiên Không thì miễn bàn. Còn Nhật Nguyệt tối tăm gặp Thiên Không cũng giống như gặp được một cánh cửa mới cho cuộc đời hẩm hiu mà nếu họ tận dụng được, đủ lượng sẽ nhảy vọt về chất. Nhưng nhớ rằng Thiên Không chỉ tốt cho mặt trí tuệ của Nhật  Nguyệt, tức là được cho - nhưng cho trong phạm vi khuôn phép.
// 3. Thiên Không giác ngộ. Trường hợp này hơi khó gặp, vì nó yêu cầu sự đặc biệt từ mệnh hoặc đại vận của đương số. Nếu như mệnh cách hội họp những bộ sao tâm linh mang tính giác ngộ - Quang Quý, Quan Phúc, Tứ Đức, Thai Tọa,  Diêu Y, Long Phượng tứ linh,... cùng với những bộ chính tinh tiêu biểu như Tham Lang,  Thiên Đồng, Thiên Tướng, Thiên Lương,... thì trong giai đoạn của Thiên Không, họ sẽ hay có những cơ duyên để giác ngộ. Có thể chỉ là một vài câu nói đơn giản, nhưng lại giúp họ bỏ đi được chấp niệm vốn có trong lòng, đây cũng là lúc Thiên Không làm trọn vẹn nhất trách nhiệm của nó - Vạn Sự Giai Không...
// Thiên Không còn được biết đến với cái tên khác là thần Nhân Quả, gieo nhân nào, gặt quả ấy. Thiên Không lạnh lùng và tàn nhẫn không khác gì Tuần - Triệt, nhưng Thiên Không cũng bao la và đạo đức y hệt như Quang Quý. Thiên Không phá nát một cuộc hôn nhân, nhưng cũng là sự giải thoát cho hai mảnh đời nghiệt nợ. Thiên Không làm một người phá sản, nhưng đó là hệ quả của những năm tháng bất chấp. Người đau ốm nặng gặp Thiên Không thì y hệt quẻ lục xung, Thiên Không giúp họ chấm dứt hết những đau đớn khổ ải để sang một kiếp mới...
// Nói chung, Thiên Không đến như một người thầy kiểm tra đại vận, giải quyết hết tất cả những khúc mắc, bắt đương số trả sạch những nợ nần để chuyển sang một giai đoạn khác...
// Hạn đến Thiên Không thì nên đi học... Đừng mơ mộng những gì ngoài tầm với, mây của trời thì cứ để gió cuốn đi ^^



// https://www.facebook.com/photo/?fbid=174171371996276&set=gm.3507361919479206&idorvanity=1592102927671791
// cho em xin hỏi là có hạn liên quan tới chuyện giường chiếu ko ạ???
// đầu năm em rất đen mất 4tr tiền mặt
// bị thương nghỉ nửa tháng
// xong hôm qua em bị dính vào mqh mà em ko có tình cảm(e biết cái này là do em yếu đuối) nhưng giống như em bị vào thế ạ có phải cũng tính là hạn ko ?. tại hơn 3 năm nay e chưa từng yêu ai. rồi tự nhiên sảy ra chuyện này
// h chỉ thấy hối hận và sợ
// từ h đến cuối năm em còn bị hạn j nữa ko ạ???
// các thày mà phán em có hạn hỷ nữa là em mất ngủ luôn😔😔😔😔
// 30/7/1994 giờ Dần, Mệnh Thiên Phủ đắc tại Tỵ

// Đào Hoa
// Thân Tí Thìn: đh tại Dậu: thực dụng trong tình cảm
// Hợi Mão Mùi: đh tại Tí: hào hoa phong nhã
// Dần Ngọ Tuất: đh tại Mão: chân thành, đa tình, lụy tình
// Tỵ Dậu Sửu: đh tại Ngọ: sớm nở tối tàn, cả thèm chóng chán



// Workshop 18/11
// Những người trước tháng 5/75
// Gia đình cụ Thiên Lương
// https://www.tuvi8.com/

// Những người bên Hà Nội.
// https://tuvichanco.vn/

// Kình Dương phải đi với Lực Sĩ: tác động nhanh.
// Đà La phải đi với Quan Phủ: lâu dài
// Một số app an sao sai như app tuvichanco

// Can dương: Giáp, Mậu, Nhâm.....
// Can âm: Quý...

// Âm Kim hay Dương Kim khác nhau hoàn toàn.
// Thìn Tuất Sửu Mùi là Thổ.
// Theo mùa:
// Mộc: Thìn tiết khi xuân
// Hỏa: Mùi tiết khi hè
// Kim: Tuất tiết khi Thu
// Thủy: Sửu tiết khí Đông

// Nhị Hợp: cắt dọc lá số
// Nhị Hại: cắt ngang lá số

// Trong hợp có xung, trong hợp có khắc.
// Ví dụ: hợp nhưng có con Hỏa con Kim sẽ khắc, xung.

// Thủy: khó đoán nhất, uốn lượn theo thực tế.
// Tứ Trụ: trụ ngày, tháng, năm, giờ
// Mỗi trụ có hành. Hành nào vượng nhất.

// Thiên Thượng Hỏa: lửa Mặt Trời.
// Tích Lịch Hỏa: lửa từ điện.
// 2 Hỏa này Thủy không khắc được. Dội nước vào điện là hỏng. Lửa Mặt Trời không dập được.

// Mệnh ở Dần Ngọ Tuất: bộ Hỏa
// Môi trường cạnh tranh nhiều.
// Muốn ổn định phải nỗ lực rất nhiều.
// Mệnh ở Hợi Mão Mùi.
// Bộ nhân hòa. Xác định cuộc đời phải sống và làm việc phục vụ cộng đồng, có ích cho xã hội thì lợi ích mới đi theo.
// ====> H nè

// Mệnh ở Tỵ Dậu Sửu.
// Thiệt thòi nhất trong 4 bộ Tam Hợp.
// Không có gì tương sinh cho Kim, lại còn bị Hỏa khắc, sinh cho hành Thủy.
// Tự thân vận động mọi thứ, lo cho người khác. Ý chí khá cao.
// ====> MA nè

// Mệnh ở Tỵ Dậu Sửu.
// Thiệt thòi nhất trong 4 bộ Tam Hợp.
// Không có gì tương sinh cho Kim, lại còn bị Hỏa khắc, sinh cho hành Thủy.
// Tự thân vận động mọi thứ, lo cho người khác. Ý chí khá cao.

// Ý chí cao cỡ nào phải phụ thuộc vào Thiên Mã.
// Thiên Mã ở Dần, Tỵ: ý chí nghị lực cao. Thiên Mã là của mình.
// Thiên Mã ở Thân:
// Thiên Mã ở Hợi: làm vất vả hơn người khác rất nhiều.

// Mã gặp Triệt: ngựa què.
// Mã gặp Tuần: di chuyển từ cung nó đứng, đi ngược lại 1 cung.
// Mã ở Hợi -> di chuyển về cung Dần.

// Mã gặp Tang Môn: ngựa kéo xe tang
// Mã gặp Triệt, Tang Môn: vừa kéo xe tang, vừa què -> không có tác dụng.
// Mã Khốc Khách (Thiên Khốc, Điếu Khách): ngựa đeo nhạc. Khả năng ăn nói, giao tiếp, đứng trước đám đông.

// Ý chí cao cỡ nào phải phụ thuộc vào Thiên Mã.
// Thiên Mã ở Dần, Tỵ: ý chí nghị lực cao.
// Thiên Mã ở Thân:
// Thiên Mã ở Hợi: làm vất vả hơn người khác rất nhiều.

// Còn Mã ở đâu là của mình thì phải tùy vào Mệnh.
// Ví dụ: mạng Mộc thì Mã ở Dần (Hỏa) và Hợi (Thủy) mới là của mình.
// Mệnh gì thì xem nạp âm.

// Mệnh ở Thân Tí Thìn (Thủy):
// Không bị khắc, chỉ có sinh, Kim sinh.
// May mắn, nhưng ý chí không cao.

// Kiếm Phong Kim
// Ô của H ô Dần (Hỏa): địa lợi, được hỗ trợ.
// Ô của MA ô Sửu (Kim): bình thường.
// Mệnh Thổ, nằm ở cung Dần: bị khắc, môi trường không tốt.
// 1 mẹo: sao Tử Vi, Thái Âm, Thái Dương
// Thái Âm/Dương hãm
// Tử Vi tuần triệt
// Nên đi nước ngoài, đi xa khỏi nơi mình sống.
// MA Thái Âm/Dương hãm
// Tử Vi không tuần triệt.
// Ở nhà đi, không đi nước ngoài đâu.

// Bố cục lá số Tử Vi
// 3 thứ:
// Tam Hợp nào
// Vòng Tam Luân: tư cách con người. (Tang Tuế Điếu, Tuế Hổ Phù, ..., ...)
// Vòng Trường Sinh: Thai, Dưỡng, Mộ, Tuyệt...

// cung Thân là nơi dựa dẫm. Sau 30t mới mạnh.
// Là THân Cư Tài đó.

// Mệnh Thân đồng cung: nghĩ sao làm vậy.
// Khác cung: nghĩ nhưng tùy thuộc vào bên ngoài
// Tật Ách: suy nghĩ thâm sâu bên trong.
// Tài không phải cung tiền, mà là phương thức kiếm tiền.
// Két tiền là cung Tử Tức.
// Cách xài tiền nhìn cung Huynh Đệ.
// Quan Lộc cách kiếm ra tiền.
// Giàu hay không giàu nhìn Lộc Tồn. Thiên Di và Tử Tức mới giàu.
// Thiên Lương, Lộc Tồn cung Tài: giữ tiền nhiều hơn là kiếm.
// Lộc Tồn không tuần triệt thì không nghèo.
// Lộc Tồn Huynh Đệ: xài tiền không kỹ, kiếm tiền không dễ. MA nè
// Huynh Đệ: anh em đồng trang lứa
// Phụ Mẫu: ba mẹ, người đi trước.
// 2 cung này thân cận với mình. Xem họ khuyên thế nào.

// Lá số bình thường
// Lá số Phú Cách: giàu (chưa chắc sang)
// Lá số Quý Cách: sang (chưa chắc giàu)

// Quý Cách
// Tử Vi Thiên Phủ đồng cung tại Dần, Thân -> MA
// Thất Sát độc thủ Dần, Thân -> các sao khác Đắc Miếu Vượng toàn bộ.
// Mà nếu MA sang thì phải có nhà đầu tư để MA sang. Chứ MA không giàu thì tiền đâu mà sang.

// H nè:
// Lộc Tồn Phu Thê: cả nam lẫn nữ lập gd trễ thôi
// Nam Thiên Cơ -> Phu Thê Thái Dương -> phụ nữ quan trọng sự nghiệp hơn gia đình. Phải chấp nhận.

// Các bước
// Vòng Tam Luân
// Cung vị, cung chức, ảnh hưởng của các sao
// Vòng Tam Hợp.
// Vòng Tứ Hóa

// Bắc Phái
// Trung Châu Phái (Tam Hợp phái)
// Tứ Hóa Phái (khá chi tiết các sự việc)
// Về xem Cung Phi
// Tháng 12: Chiêu Tài Pháp, kích tài, tụ tài. Nói chung về tiền.
