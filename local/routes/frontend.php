<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Front-end Routes
|--------------------------------------------------------------------------
|
| Tổng hợp các route của trang Front-end.
|
*/

// ======================= Dashboard =======================

Route::get('/', 'frontend\MainController@index')->name('frontend-index');

Route::get('/about', 'frontend\MainController@about')->name('frontend-about-us');

Route::get('/post-list/{urlCat}', 'frontend\PostController@list')->name('frontend-post-list');

Route::get('/post/{urlPost}', 'frontend\PostController@detail')->name('frontend-post');

Route::get('/download-list/{urlCat}', 'frontend\DownloadController@list')->name('frontend-download-list');

Route::get('/download/{urlDownload}', 'frontend\DownloadController@detail')->name('frontend-download');





Route::get('/than-cu', 'frontend\ToolController@thancu')->name('than-cu');

Route::get('/bo-sao', 'frontend\ToolController@bosao')->name('bo-sao');

Route::post('/bo-sao-search', 'frontend\ToolController@bosaosearch')->name('bo-sao-search');



Route::get('/tool-luan-la-so', 'frontend\ToolController@index')->name('tool-luan-la-so');

Route::post('/tool-luan-la-so', 'frontend\ToolController@search')->name('tool-luan-la-so-search');

// https://www.brandcrowd.com/maker/logo/yin-yang-donut-171567?text=moon&isSearch=True
