Tham Lang Cung Điền Trạch

Tham Lang là ngôi sao nửa tốt nửa xấu.
Tham Lang tượng trưng cho ham muốn, lòng tham, dục vọng và số đào hoa, sắc đẹp.
Nhìn chung, khi nhắc đến Tham Lang, đó là sự quyết tâm có được thứ gì đó.

Tham Lang Cung Điền Trạch, đương số sẽ có quyết tâm muốn có tài sản, đất đai.
Đương số khả năng cao có nhiều điền sản, tài sản. Nhưng giữ được hay không còn phải xem các sao khác.

Tham Lang Cung Điền Trạch, nhà ở hoặc nơi làm việc xung quanh có nhiều nhà hàng, quán ăn, karaoke, những tụ điểm ăn chơi, làm nail, trang điểm, spa...

Tham Lang hành Âm Thủy, Thủy đới Mộc. Nhà cửa có thể bị thấm dột, nước ngầm.

Tham Lang Hóa Kỵ cung Điền Trạch, đương số lười dọn dẹp nhà cửa, nhà cũ, được xây từ rất lâu.
Nhà và nơi làm việc có sự tranh chấp, mâu thuẫn với hàng xóm, tranh đấu ngầm.