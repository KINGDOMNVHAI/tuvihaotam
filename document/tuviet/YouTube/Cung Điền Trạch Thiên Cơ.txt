Thiên Cơ Cung Điền Trạch Thiên Cơ

Thiên Cơ, tượng trưng cho cỗ máy, máy móc, luôn biến động, vận hành.
Trong khi Điền Trạch lại là bất động sản, chủ về tài sản, nhà đất nằm cố định, ít biến đổi.

Thiên Cơ Cung Điền Trạch, đương số khó thừa hưởng nhà. Nơi ở, nơi làm việc thường hay thay đổi.

Thiên Cơ chủ về trí óc, trí thông minh. Thiên Cơ Cung Điền Trạch, đương số sống và làm việc tại những nơi có nhiều tri thức như nhà sách, trường học, văn phòng... Đương số có nhiều cơ duyên gặp gỡ những người có tri thức.
